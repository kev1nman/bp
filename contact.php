<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include 'structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Contact</h1>
		</div>
	</div>
	<div class="text-center">
			<!-- FORMULARIO-->
			<form class="form-horizontal col-md-12">
				<!-- NOMBRE-->
				<div class="form-group">
					  <label class="col-md-4 control-label" for="name">Name</label>
					  <div class="col-md-6">
					  		<input id="name" name="name" type="text" placeholder="My name is?" class="form-control input-md">
					  </div>
				</div>
				<!-- EMAIL-->
				<div class="form-group">
					  <label class="col-md-4 control-label" for="email">Email</label>
					  <div class="col-md-6">
					  		<input id="email" name="email" type="text" placeholder="What is your email?" class="form-control input-md" required="">
					  <span class="help-block">Example: myemail@email.com</span>
					  </div>
				</div>
				<!-- MENSAJE -->
				<div class="form-group">
					  <label class="col-md-4 control-label" for="message"></label>
					  <div class="col-md-6">
					    	<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
					  </div>
				</div>
				<!-- BOTON ENVIAR -->
				<div class="form-group">
					  <label class="col-md-4 control-label" for="send"></label>
					  <div class="col-md-6">
					    	<button id="send" name="send" class="btn btn-primary">Send</button>
					  </div>
				</div>
			</form>
	</div>
	<?php
		pie();
	?>
	<script>
		$(document).ready(function () {
			$('#sect1').addClass('active');
		});
		$("#E-Banking").html('<span class="icon-user"></span> Personal E-Banking');
	</script>
</body>
</html>
