<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include ("structure.php");
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div class="content">
		<div class="container">
			<div class="main">
				<div class="slides">
					<a href="#"><img src="<?php host();?>/rs/img/1.jpg" alt=""></a>
					<a href="#"><img src="<?php host();?>/rs/img/2.jpg" alt=""></a>
					<a href="#"><img src="<?php host();?>/rs/img/3.jpg" alt=""></a>
					<a href="#"><img src="<?php host();?>/rs/img/4.jpg" alt=""></a>
				</div>
			</div>
			<div id="banner2">
				<img class="img-responsive block" src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
			</div>
			<div class="col-md-12">
				<div class="col-md-6 col-md-offset-3">
					<h1>¿What is BP?</h1>
					<p class="lead text-justify">BP is a financial institution recognized internationally.</p>
					<p class="lead text-justify">BP is to serve, because our priority is to meet the needs of our customers; with the highest standards for a financial institution.</p>
					<p class="lead text-justify">BP we specialize in asset management for select clients , and offer high quality advice focused on helping you improve your quality of life, we offer our full range of products and services.</p>
				</div>
			</div>
		</div>
		<div id="banner">
			<img class="img-responsive block" src="<?php host();?>/rs/img/bann2.jpg">
		</div>
	</div>

	<div class="articulos col-md-12">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="articuloCol12 col-md-12">
						<div class="articuloCol6 col-md-6">
							<div class="col-sm-8 col-md-8">
								<h3 class="title">Personal accounts
									<br><small>
										<a href="<?php host();?>/personal/accounts/savings.php">Savings</a> /
										<a href="<?php host();?>/personal/accounts/everyday.php">Everyday</a>
									</small>
								</h3>
								<p>
									Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile, Fast Savings.
								</p>
								<a href="<?php host();?>/personal/personal.php" class="btn btn-primary">More about</a>
							</div>
							<div class="col-sm-4 col-md-4 nopadding">
								<h3>
									<img class="img-responsive center-block img-thumbnail" width="300" src="<?php host();?>/rs/img/fastSavings.jpg">
								</h3>
							</div>
						</div>
						<div class="articuloCol6 col-md-6">
							<div class="col-sm-8 col-md-8">
								<h3 class="title">Business accounts
									<br><small>
										<a href="<?php host();?>/business/saving/saving.php">Savings</a> /
										<a href="<?php host();?>/business/everyday/everyday.php">Everyday</a>
									</small>
								</h3>
								<p>
									Our dedicated Business Banking specialists know what makes successful BP organizations tick...
								</p>
								<a href="<?php host();?>/business/business.php" class="btn btn-primary">More about</a>
							</div>
							<div class="col-sm-4 col-md-4 nopadding">
								<h3>
									<img class="img-responsive center-block img-thumbnail" width="300" src="<?php host();?>/rs/img/casa.jpg">
								</h3>
							</div>
						</div>
					</div>
					<div class="articuloCol col-md-12">
						<div class="articuloCol6 col-md-6">
							<div class="col-sm-8 col-md-8">
								<h3 class="title">Personal | Credit card
									<br><small>
										<a href="<?php host();?>/personal/creditCard/platinum.php">Platinum</a> /
										<a href="<?php host();?>/personal/creditCard/gold.php">Gold</a>
									</small>
								</h3>
								<p>
									Platinum Credit Card is a premium international payment card which will give you top-class privileges and security while traveling, as well as being a great companion in your everyday life.							</p>
								</P>
								<a href="<?php host();?>/personal/creditCard/creditCard.php" class="btn btn-primary">More about</a>
							</div>
							<div class="col-sm-4 col-md-4 nopadding">
								<h3>
									<img class="img-responsive center-block img-thumbnail" width="300" src="<?php host();?>/rs/img/creditCard.jpg">
								</h3>
							</div>
						</div>
						<div class="articuloCol6 col-md-6">
							<div class="col-sm-8 col-md-8">
								<h3 class="title">Home loans
									<br><small>
										<a href="<?php host();?>/personal/homeLoans/buyingYourFirstHome.php">Buying Your First home</a>
									</small>
								</h3>
								<p>
									Starting with the basics for first home buyers, we’ll talk you through home loans and every step of the process, so you can move forward with confidence.
								</p>
								<a href="<?php host();?>/personal/homeLoans/homeLoans.php" class="btn btn-primary">More about</a>
							</div>
							<div class="col-sm-4 col-md-4 nopadding">
								<h3>
									<img class="img-responsive center-block img-thumbnail" width="300" src="<?php host();?>/rs/img/Rewards.jpg">
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
			</div>

		</div>
	</div>
	<div class="content2">
		<div class="container">
			<div class="pieArticulos text-center col-md-12">
				<div class="row">
					<div class="pieArticuloCol4 col-md-3">
						<h3><span class="icon-facebook"></span> Facebook</h3>
					</div>
					<div class="pieArticuloCol4 col-md-3">
						<h3><span class="icon-twitter"></span> Twitter</h3>
					</div>
					<div class="pieArticuloCol4 col-md-3">
						<h3><span class="icon-youtube2"></span> Youtube</h3>
					</div>
					<div class="pieArticuloCol4 col-md-3">
						<h3><span class="icon-linkedin"></span> LinkedIn</h3>
					</div>
				</div>
			</div>
		</div>
		<!--
		<div class="mapSite">
			<div class="container">
				<div class="col-md-3 col-md-offset-1">
					<ul>
						<li>
						<h5><a href="<?php host();?>/personal/personal.php">
						Personal</a></h5></li>
							<li><a class='text-muted' href='<?php host();?>/personal/accounts/accounts.php'>
			                    Accounts</li>
							<li><a class='text-muted' href='<?php host();?>/personal/creditCard/creditCard.php'>
			                    Credit Card</li>
							<li><a class='text-muted' href='<?php host();?>/personal/homeLoans/homeLoans.php'>
			                    Home loans</li>
							<li><a class='text-muted' href='<?php host();?>/personal/personalLoans/personalLoans.php'>
			                    Personal loans</li>
							<li><a class='text-muted' href='<?php host();?>/personal/investment/investment.php'>
			                    Investment</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul>
						<li>
						<h5><a href="<?php host();?>/business/business.php">
						Business</a></h5></li>
							<li><a class='text-muted' href='<?php host();?>/business/saving/saving.php'>
			                    Savings</li>
							<li><a class='text-muted' href='<?php host();?>/business/everyday/everyday.php'>
			                    Everyday</li>
							<li><a class='text-muted' href='<?php host();?>/business/creditcard/creditcard.php'>
			                    Credit card</li>
							<li><a class='text-muted' href='<?php host();?>/business/loans/loans.php'>
			                    Loans</li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul>
						<li>
						<h5><a href="<?php host();?>/private/private.php">
						Private wealth management</a></h5></li>
							<li><a class='text-muted' href='<?php host();?>/private/investmentManagement.php'>
			                    Investment management</li>
							<li><a class='text-muted' href='<?php host();?>/private/privateBanking.php'>
			                    Private banking</li>
							<li><a class='text-muted' href='<?php host();?>/private/investmentManagement.php'>
			                    Investment management</li>
					</ul>
				</div>
			</div>
		</div>-->
	</div>
	<?php
		pie();
	?>
	<script>
	$(function(){
		$(".slides").slidesjs({
			play: {
				active: false,
				// [boolean] Generate the play and stop buttons.
				// You cannot use your own buttons. Sorry.
				effect: "fade",
				// [string] Can be either "slide" or "fade".
				interval: 4000,
				// [number] Time spent on each slide in milliseconds.
				auto: true,
				// [boolean] Start playing the slideshow on load.
				swap: false,
				// [boolean] show/hide stop and play buttons
				pauseOnHover: true,
				// [boolean] pause a playing slideshow on hover
				restartDelay: 3000,
				// [number] restart delay on inactive slideshow
			},
			pagination: {
		      active: true,
		        // [boolean] Create pagination items.
		        // You cannot use your own pagination. Sorry.
		      effect: "slide"
		        // [string] Can be either "slide" or "fade".
		    },
			navigation: false,
			height:380,
		});
	});


	    $(document).ready(function () {
	        $('#home').addClass('active');
	    });
		$("#E-Banking").html('<span class="icon-user"></span> Personal E-Banking');
		$("#div1").animate({ scrollTop: $('#div1')[0].scrollHeight}, 500);
	</script>
	<style media="screen">
		.slidesjs-pagination{
			background: #181818;
		}
		.slidesjs-pagination-item a{
			padding: 0;
			margin: 0;
			border-radius: 100px;
		}
		.slidesjs-pagination-item a.active{
			border-radius: 100px;
		}
	</style>
</body>
</html>
