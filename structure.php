<?php
    error_reporting(E_ALL ^ E_NOTICE);
    function host(){echo"http://localhost/bp";}
    function cabecera(){echo "
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      	<meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>
      	<link rel='stylesheet' href='http://localhost/bp/rs/css/bootstrap.min.css'>
        <link rel='stylesheet' href='http://localhost/bp/rs/css/jquery.smartmenus.bootstrap.css'>
      	<link rel='stylesheet' href='http://localhost/bp/rs/css/style.css'>
        <link rel='stylesheet' href='http://localhost/bp/rs/css/iconStyle.css'>

        <link rel='shortcut icon' href='http://localhost/bp/rs/img/favicon.ico' type='image/x-icon'>
        <link rel='icon' href='http://localhost/bp/rs/img/favicon.ico' type='image/x-icon'>

        <link rel='icon' href=''>
    	  <title>BANK | BP</title>
    ";}
    function menu(){echo "
        <!-- Navbar static top -->
        <div class='cabecera'>
            <div class='container'>
                <div class='cabeceraIzquierda col-md-6 col-sm-6'>
                    <a href='http://localhost/bp/' class='logoCabecera'>
                        <img class='logo' src='http://localhost/bp/rs/img/logoN.png' alt='BP' />
                        <p>BANK | BP</p>
                    </a>
                </div>
                <div class='ingreso col-md-6 col-sm-6'>
                    <button type='button' name='button' class='btn btn-primary' id='E-Banking' onClick=\"window.open('http://188.166.241.178/bank/public/login','mywin','width=1000,height=750,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1')\" ></button>
                </div>
                <div class='datos'>
                    <a class='iconCabecera' href='http://localhost/bp/atms.php'><span class='icon-map2'></span> Branch / ATM</a>
                    <a class='iconCabecera' href='http://localhost/bp/contact.php'><span class='icon-phone-hang-up'></span> Line BP Bank (9) 28-06230 </a>
                </div>
            </div>
        </div>
        <div class='navbar navbar-inverse navbar-static-top' role='navigation'>
          <div class='container'>
            <div class='navbar-header'>
              <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                <span class='sr-only'>Toggle navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
              </button>
            </div>
            <div class='navbar-collapse collapse'>

              <!-- Left nav -->
              <ul class='nav navbar-nav'>
            <!--HOME-->
                <li><a href='http://localhost/bp/' id='home'><span class='icon home icon-home'></span></a></li>
            <!--PERSONAL-->
                <li><a href='http://localhost/bp/personal/personal.php' id='sect1'><span class='icon icon-user'></span>
                Personal <span class='caret'></span></a>
                      <ul class='dropdown-menu'>
                    <!--Submenu 2-->
                <!----><li><a href='http://localhost/bp/personal/accounts/accounts.php'>
                        Accounts<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/personal/accounts/savings.php'>Savings</a></li>
                                <li><a href='http://localhost/bp/personal/accounts/everyday.php'>Everyday</a></li>
                            </ul>
                        </li>
                <!----><li><a href='http://localhost/bp/personal/creditCard/creditCard.php'>
                        Credit Card<span class='caret'></span></a>
                          <ul class='dropdown-menu'>
                            <li><a href='http://localhost/bp/personal/creditCard/classic.php'>Classic</a></li>
                            <li><a href='http://localhost/bp/personal/creditCard/gold.php'>Gold</a></li>
                            <li><a href='http://localhost/bp/personal/creditCard/platinum.php'>Platinum</a></li>
                            <li><a href='http://localhost/bp/personal/creditCard/black.php'>Black</a></li>
                          </ul>
                        </li>
                <!----><li><a href='http://localhost/bp/personal/homeLoans/homeLoans.php'>
                        Home loans<span class='caret'></span></a>
                          <ul class='dropdown-menu'>
                            <li><a href='http://localhost/bp/personal/homeLoans/buyingYourFirstHome.php'>Buying your first home</a></li>
                            <li><a href='http://localhost/bp/personal/homeLoans/buyingYourNextHome.php'>Buying your next home</a></li>
                            <li><a href='http://localhost/bp/personal/homeLoans/renovatingYourHome.php'>Renovating your home</a></li>
                          </ul>
                        </li>
                <!----><li><a href='http://localhost/bp/personal/personalLoans/personalLoans.php'>
                        Personal loans<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                              <li><a href='http://localhost/bp/personal/personalLoans/carLoans.php'>Car loans</a></li>
                              <li><a href='http://localhost/bp/personal/personalLoans/debtConsolidation.php'>Debt consolidation</a></li>
                            </ul>
                        </li>
                <!----><li><a href='http://localhost/bp/personal/investment/investment.php'>
                        Investment<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/personal/investment/shareTrading.php'>Share trading</a></li>
                                <li><a href='http://localhost/bp/personal/investment/superannuation.php'>Superannuation</a></li>
                                <li><a href='http://localhost/bp/personal/investment/termInvestments.php'>Term investments</a></li>
                            </ul>
                        </li>
                    </ul><!--FIN Submenu 2-->
                </li><!--FIN PERSONAL-->
                <!--BUSINESS-->
                <li><a href='http://localhost/bp/business/business.php'  id='sect2'><span class='icon icon-user-tie'></span>
                Business <span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li><a href='http://localhost/bp/business/saving/saving.php'>
                        Savings<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/business/saving/smart.php'>Smart saving</a></li>
                                <li><a href='http://localhost/bp/business/saving/master.php'>Master saving</a></li>
                                <li><a href='http://localhost/bp/business/saving/select.php'>Select saving</a></li>
                            </ul>
                        </li>
                        <li><a href='http://localhost/bp/business/everyday/everyday.php'>
                        Everyday<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/business/everyday/smart.php'>Smart saving</a></li>
                                <li><a href='http://localhost/bp/business/everyday/master.php'>Master saving</a></li>
                            </ul>
                        </li>
                        <li><a href='http://localhost/bp/business/creditcard/creditcard.php'>
                        Credit card<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/business/creditcard/classic.php'>Classic</a></li>
                                <li><a href='http://localhost/bp/business/creditcard/gold.php'>Gold</a></li>
                                <li><a href='http://localhost/bp/business/creditcard/platinum.php'>Platinum</a></li>
                                <li><a href='http://localhost/bp/business/creditcard/black.php'>Black</a></li>
                            </ul>
                        </li>
                        <li><a href='http://localhost/bp/business/loans/loans.php'>
                        Loans<span class='caret'></span></a>
                            <ul class='dropdown-menu'>
                                <li><a href='http://localhost/bp/business/loans/businessLending.php'>Business lending</a></li>
                                <li><a href='http://localhost/bp/business/loans/assetFinance.php'>Asset finance</a></li>
                                <li><a href='http://localhost/bp/business/loans/commercialBankingPropertyFinance.php'>Commercial banking and property finance</a></li>
                            </ul>
                        </li>

                    </ul><!--FIN Submenu 2-->
                </li><!--FIN BUSINESS-->

                <!--PRIVATE-->
                <li><a href='http://localhost/bp/private/private.php'  id='sect3'><span class='icon icon-pen'></span>
                Private wealth management <span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li><a href='http://localhost/bp/private/investmentManagement.php'>
                        Investment management</a></li>
                        <li><a href='http://localhost/bp/private/privateBanking.php'>
                        Private banking</a></li>
                        <li><a href='http://localhost/bp/private/wealthStructuringAndTrust.php'>
                        Wealth structuring and trust</a></li>
                    </ul>
                </li><!--FIN PRIVATE-->
            <!--SEARCH-->
                <li>
                    <div class='searchForm'>
                        <div class='input-group'>
                            <input type='text' class='form-control' placeholder='Search'>
                            <span class='input-group-btn'>
                                <button class='btn btn-info' type='button'><span class='icon-search'></span></button>
                            </span>
                        </div>
                    </div>
                </li>
            <!--SEARCH
                <li><a href='#'  id='sect5'><span class='icon-search'></span> <span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li>
                            <div class='searchForm'>
                                <div class='input-group'>
                                    <input type='text' class='form-control' placeholder='Search'>
                                    <span class='input-group-btn'>
                                        <button class='btn btn-info' type='button'><span class='icon-search'></span></button>
                                    </span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>-->
              </ul>
            </div><!--/.nav-collapse -->
          </div><!--/.container -->
        </div>
    ";}
    function pie(){echo "
        <footer class='col-sm-12 footer'>
            <div class='container'>
                <div class='col-xs-12 col-sm-5 footerLeft'>
                    <p class='text-muted'>
              			Legal Information © Copyright BP 2009. All rights reserved.
              		</p>
                </div>
          		<div class='col-xs-12 col-sm-7 footerRigth'>
                    <a id='pie1' class='text-muted' href='http://localhost/bp/info/privacyAndSecurity.php'>
                        Your privacy and security</a>
                    <a id='pie2' class='text-muted' href='#'>
                        Website Terms of Use</a>
                    <a id='pie3' class='text-muted' href='#'>
                        Site Map</a>
                    <a id='pie4' class='text-muted' href='#'>
                        Help</a>
                    <a id='pie1' class='text-muted' href='#'>
                        Jobs at BP</a>
                    <a id='pie5' class='text-muted' href='#'>
                        Contact us</a>
          		</div>
            </div>
  	    </footer>
        <script src='http://localhost/bp/rs/js/jquery.js'></script>
        <script src='http://localhost/bp/rs/js/actions.js'></script>
    	<script src='http://localhost/bp/rs/js/bootstrap.min.js'></script>
        <script src='http://localhost/bp/rs/js/jquery.smartmenus.min.js'></script>
        <script src='http://localhost/bp/rs/js/jquery.smartmenus.bootstrap.min.js'></script>
        <script src='http://localhost/bp/rs/js/jquery.slides.min.js'></script>
    ";}
?>
