<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include 'structure.php';
		include 'conect.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();

		$city = $_POST['city'];
		$text = $_POST['text'];
		$atms = $_POST['atms'];
		$branch = $_POST['branch'];
		$atmsbranch = $_POST['atmsbranch'];
	?>
	<div class="bgsystem">
		<div class="container">
			<div class="contentAtms col-md-12">
				<div class="searchAtms">
					<div class="col-md-12">
						<h1>ATMS and Branch</h1>
					</div>
					<div class="col-md-12">
						<form class="form-inline" method="post" action="atms.php">
							<div class="col-md-12">
								<!-- PAISES
								<div class="form-group">
									<select type="email" class="form-control input-lg" id="pais" placeholder="">
										<option>New Zeland</option>
									</select>
								</div>
								-->
								<div class="input-group">
									<span class="input-group-addon">
										<label class="checkbox-inline">
											<input type="checkbox" value="ATMS" <?php if ( isset($atms)) {echo " checked='checked' ";} ?> name="atms"> ATMS
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="BRANCH" <?php if ( isset($branch)) {echo " checked='checked' ";} ?> name="branch"> BRANCH
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="BRANCH & ATMS" <?php if ( isset($atmsbranch)) {echo " checked='checked' ";} ?> name="atmsbranch"> BRANCH & ATMS
										</label>
									</span>
									<input type="text" class="form-control input-lg" id="search" name="text" <?php if ($text=="") {echo "placeholder='Address...'";}else{ echo "value='".$text."' placeholder='Address...'"; } ?> autofocus>
								</div>
								<div class="form-group">
									<select class='form-control input-lg' id='city' name='city'>
										<option value='s'>Select city</option>
										<?php
										$consulta1 = "SELECT * FROM atms GROUP BY city";
										$resultado1 = mysql_query($consulta1,$conexion);
										while ($r1 = mysql_fetch_array($resultado1)) {
											$city2 = $r1['city'];
											echo "<option value='$city2'"; if ($city2==$city) {
												echo "selected";
											} echo">$city2</option>";
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-info input-lg">Search</button>
								</div>
							</div>

						</form>
					</div>
				</div>
				<?php
				if ($city!='s') {
					//echo "entra con city";
					if ( isset($atms) and !isset($branch) and !isset($atmsbranch)){
						//echo "consulta 1 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE city='$city'
						AND address LIKE '%$text%'
						AND type='$atms'
						";
					}else if( !isset($atms) and isset($branch) and !isset($atmsbranch)){
						//echo "consulta 2 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE city='$city'
						AND address LIKE '%$text%'
						AND type='$branch'
						";
					}else if( !isset($atms) and !isset($branch) and isset($atmsbranch)){
						//echo "consulta 3 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE city='$city'
						AND address LIKE '%$text%'
						AND type='$atmsbranch'
						";
					}else if( isset($atms) and isset($branch) and isset($atmsbranch)){
						//echo "consulta 4 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE city='$city'
						AND address LIKE '%$text%'
						";
					}else if( !isset($atms) and !isset($branch) and !isset($atmsbranch)){
						//echo $consulta."<br>";
						//echo "consulta 5 = ".$city;
						echo "<div class='col-md-12 bg-primary'>
							<h3 class='text-center bg-primary'>
								Please select what kind of direction you need, example: ATMS, BRANCH or BRANCH & ATMS
							</h3>
						</div>
						";
					}
				}else {
					if ( isset($atms) and !isset($branch) and !isset($atmsbranch)){
						//echo "consulta 11 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE address LIKE '%$text%'
						AND type='$atms'
						";
					}else if( !isset($atms) and isset($branch) and !isset($atmsbranch)){
						//echo "consulta 22 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE address LIKE '%$text%'
						AND type='$branch'
						";
					}else if( !isset($atms) and !isset($branch) and isset($atmsbranch)){
						//echo "consulta 33 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE address LIKE '%$text%'
						AND type='$atmsbranch'
						";
					}else if( isset($atms) and isset($branch) and isset($atmsbranch)){
						//echo "consulta 44 = ".$city;
						$consulta = "SELECT * FROM atms
						WHERE address LIKE '%$text%'
						";
					}else if( !isset($atms) and !isset($branch) and !isset($atmsbranch)){
						//echo $consulta."<br>";
						//echo "consulta 55 = ".$city;
						echo "<div class='col-md-12 bg-primary'>
							<h3 class='text-center bg-primary'>
								Please select what kind of direction you need, example: ATMS, BRANCH or BRANCH & ATMS
							</h3>
						</div>
						";
					}
				}

				$resultado = mysql_query($consulta,$conexion);
				while ($r = mysql_fetch_array($resultado)) {
					$nom = $r['nom'];
					$address = $r['address'];
					$type = $r['type'];
					?>
					<div class="atmsBox col-md-4">
						<div class="atms">
							<p>
								<?php
								echo "<h4>".$nom."</h4>";
								echo "Address: ".$address;
								echo "<br>Type: <span class='etiqueta'>".$type."</span>";
								?>
							</p>
						</div>
					</div>
					<?php
				}
				?>

			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
		$(document).ready(function () {
			$('#sect1').addClass('active');
		});
		$("#E-Banking").html('<span class="icon-user"></span> Personal E-Banking');
	</script>
</body>
</html>
