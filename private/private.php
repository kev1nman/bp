<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
<div class="container">
	<div class="col-md-9">
		<h1>Private wealth management</h1>
		<p>
			BP Wealth Management™ is an innovative team-based approach to wealth management that addresses the entirety of your life–your family, your business, your future–one facet at a time. Together with your relationship manager, our BP Wealth Management specialists bring their skills and expertise to the consideration of what you’ve accumulated–and how best to administer it through life’s changes. From financial counsel on managing your wealth to careful contemplation of how to transfer it to future generations, it’s your thinking, combined with our thinking, to create Enriched Thinking™.
		</p><br><br>
	</div>
</div>
<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	<h3></h3>
</div>
<div class="sections col-md-12">
	<div class="sect col-lg-4 col-md-4 col-sm-12">
		<div class="section1">
			<div class="imgSections">
				<a href="<?php host();?>/private/investmentManagement.php"><img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" /></a>
			</div>
			<h3>Investment management</h3>
			<legend></legend>
			<p>
				Everything you’ve seen and experienced in your life has come together to make your situation absolutely unique. Your needs and those of your family, your business and your future are unlike anyone else’s and that’s the ideal way to approach your investments.
			</p>
			<a class="btn btn-info" href="<?php host();?>/private/investmentManagement.php">More info</a>
		</div>
	</div>
	<div class="sect col-lg-4 col-md-4 col-sm-12">
		<div class="section1">
			<div class="imgSections">
				<a href="<?php host();?>/private/privateBanking.php"><img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" /></a>
			</div>
			<h3>Private banking</h3>
			<legend></legend>
			<p>
				Because no one ever said "I want to bank more", here's banking that's designed to be swift, satisfying and personalized to your needs. As a Private Banking client you'll experience our personalized, attentive and discreet service, designed to cater to your banking and borrowing needs accurately and efficiently, saving you time and effort.
			</p>
			<a class="btn btn-info" href="<?php host();?>/private/privateBanking.php">More info</a>
		</div>
	</div>
	<div class="sect col-lg-4 col-md-4 col-sm-12">
		<div class="section1">
			<div class="imgSections">
				<a href="<?php host();?>/private/wealthStructuringAndTrust.php"><img class="img-responsive img-circle center-block" width="300" src="<?php host();?>/rs/img/investment.jpg" alt="" /></a>
			</div>
			<h3>Wealth structuring and trust</h3>
			<legend></legend>
			<p>
				For over 15 years, BP Bank has helped families and individuals plan for the preservation and transfer of their wealth. Together with your relationship manager, we offer specialized services tailored to meet your personal objectives and designed to evolve across a variety of life stages and financial events.
			</p>
			<a class="btn btn-info" href="<?php host();?>/private/wealthStructuringAndTrust.php">More info</a>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect3').addClass('active');
	});
	$("#E-Banking").html('Private E-Banking');
</script>
</body>
</html>
