<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Private banking</h1>
			<div class="col-md-9 col-md-offset-2">
				<h3>The personalized, attentive and discreet service you've earned.</h3>
					<p>
						Because no one ever said "I want to bank more", here's banking that's designed to be swift, satisfying and personalized to your needs. As a Private Banking client you'll experience our personalized, attentive and discreet service, designed to cater to your banking and borrowing needs accurately and efficiently, saving you time and effort.
					</p>
					<p>
						The customary banking services – chequing and savings accounts, loans and lines of credit – are all provided. What sets us apart is our ability to help you on an entirely different level.

					</p>
					<p>
						Need Yuan in Grand Cayman because you're leaving for China tomorrow morning? We can help.

					</p>
					<p>
						Been travelling and returned home to discover the utility company will cut off your vacation home electricity if a past-due bill isn't paid immediately? We can help you get it done.

					</p>
					<p>
						Through Bp Bank Wealth Management, you will also enjoy the safety, security and peace of mind that comes from working alongside a premier financial institution.

					</p>
				<h3>Preferential Services</h3>
					<p>
						We recognize that time is your most valuable asset – the ultimate finite resource. Our suite of comprehensive, preferential banking services is designed specifically to save you time. Your suite of services will be customized based on your circumstances and in consultation with your relationship manager and your dedicated Private Banker.
						At your request, we will work closely with your lawyers, accountants and tax advisors to create a truly personalized solution.
					</p>
					<p>
						Private Banking services include:
					</p>
					<ul>
						<li>Up to four chequing or savings accounts</li>
						<li>Personalized cheques</li>
						<li>Customized credit solutions</li>
						<li>Preferred rates on Certificates of Deposit</li>
						<li>Online Banking</li>
						<li>Bill payment services</li>
						<li>Cashiers Cheques/ Bank drafts</li>
						<li>Cheque certification and stop payments</li>
						<li>Use of BP Bank® banking machines, including other ABMs displaying the Plus symbol+</li>
						<li>Interest and Fee letters at year end</li>
						<li>Letters of introduction</li>
					</ul>

				<h3>Customized lending</h3>
					<p>
						It's likely you've wanted to act on an investment opportunity but, with equity wrapped up in your home, investments and insurance, accessing your money was complex, time-consuming, even frustrating. One of the key differentiators of our Private Banking offering is the ability to provide you with customized solutions designed to facilitate building wealth.
						Our customized credit solutions are developed through a clear understanding of the opportunity drawing on a combination of personal and commercial credit structures.
					</p>

				<h3>Credit that Fits Your Lifestyle</h3>
					<p>
						You may be considering the purchase of a new vacation property, sailboat or family vehicle. You may have discovered an exceptional investment property and time is of the essence. Or perhaps you're looking to finance major home renovations or your children's post-secondary education. Whatever goals you have and outlays you need to make, we're here to try to help you make them, unhindered by inopportune timing related to investment maturities or compensation. Our Private Banking products and services are designed to help you obtain the credit and liquidity you need, when you need it, to fit your lifestyle.
						Working with a team of credit specialists and your external tax professionals, your relationship manager will work to facilitate your borrowing needs to provide tailored lending solutions designed to meet the demanding requirements of complex structures. Whether those include leveraging multiple asset classes or availing of loans in multiple currencies to achieve the preservation of foreign exchange strategies, we strive to deliver innovative borrowing solutions.
					</p>

				<h3>Find the Right Investments For Your Goals</h3>
					<p>
						Meet with a professional Wealth Advisor to develop an investment portfolio designed to manage risk and meet your financial goals
					</p><br><br>
			</div>
		</div>
	</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect3').addClass('active');
	});
	$("#E-Banking").html('Private E-Banking');
</script>
</body>
</html>
