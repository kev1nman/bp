<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Wealth structuring and trust</h1>
			<div class="col-md-9 col-md-offset-2">
				<h3>Enriched Thinking™ means you can plan for what’s to come.</h3>
					<p>
						For over 15 years, BP Bank has helped families and individuals plan for the preservation and transfer of their wealth. Together with your relationship manager, we offer specialized services tailored to meet your personal objectives and designed to evolve across a variety of life stages and financial events.
					</p>
					<p>
						We work with you to define your goals and evaluate your current financial plans, identifying gaps that may require attention and creating strategies to address new opportunities for future success - as you define it. Together, we will develop a tailored plan with the goal of preserving your hard-earned wealth across future generations.
					</p>
					<p>
						We have extensive experience in administering customized solutions for institutions, individuals, and the entities they control. Our dedicated teams are committed to providing wealth structuring and trust expertise and insight across a range of financial disciplines.
					</p>
				<h3>Wealth Structuring</h3>
					<p>
						With so many fiduciary options available and different facets to be taken into consideration, working with a Scotia Wealth Management relationship manager with access to international and local expertise can be an effective way to address your unique needs. We specialize in complex structures which may include multi-faceted touch points and financial and non-financial assets, providing you with a financial plan that takes your world-wide assets into consideration.
						Using tailored asset holding vehicles and associated services, you, your relationship manager and a team of relevant wealth management specialists work in partnership with your external advisors to design and implement your preferred structure. Your BPtrust specialist also consults with you about integrating governance oversight that is designed to take into account decision making, family values, dispute resolution as well as the allocation and management of assets.
						Once the appropriate structure(s) has been determined, a dedicated team will provide you with ongoing support and service.
					</p>
				<h3>Estate Administration & Trust Management</h3>
					<p>
						By appointing BPtrust as Executor and/or Trustee of your estate, you are helping to ensure that the desires expressed in your Will and trusts are carried out with sensitivity, professionalism and objectivity. Experienced professionals at BPtrust administer and manage estates and trusts of varying complexity and value in a timely manner and are sensitive to the needs and concerns of family members and other beneficiaries.
					</p>
					<p>
						BPtrust may be one of your best choices for Executor if you have a complex family situation, own complex assets, wish to establish ongoing trusts or ease the burden on loved ones at a difficult time. Where appointed as your executor, BPtrust may also be a good choice as attorney for property.
					</p>
					<p>
						Should you be named as an Executor, trustee or Attorney for Property, BPtrust can help. We provide invaluable mentoring and education services to executors who are uncertain about what the role entails, face time or distance challenges, are concerned about personal liability or the potential for beneficiary issues. A thoughtful estate plan will also include a plan for potential future incapacity.
					</p>
				<h3>Philanthropic Advisory</h3>
					<p>
						Establishing a private charitable foundation is an effective way to engage in philanthropy and create a personal legacy. Together with your relationship manager and your external advisors, we can assist you in integrating and balancing your philanthropy with your financial and estate plan.
						Our knowledge and understanding of the region helps to ensure that you and your relationship manager arrive at a solution designed to meet your individual needs while protecting your assets. Once a foundation that serves both you and your beneficiaries is established, BPtrust can act on your behalf in many respects upon your request - from acting as secretary and providing directors to administering the foundation.
					</p>
				<h3>Custody Services</h3>
					<p>
						Our custody services are backed with a world class support team. Together we work to help investors consolidate and safeguard their assets.
						Known for service, security and privacy, we administer financial assets including cash, money market instruments, equities, government and corporate bonds, other debt instruments, rights, warrants and mutual fund units.
						Our goal is to help you keep your financial assets safe and efficiently manage the administration of your investments.
					</p>
				<h3>Develop a Plan for All Your Wishes</h3>
					<p>
						Meet with a BPtrust specialist today to learn how a customized strategy can help ensure your wealth is preserved and properly transferred to your loved ones.
					</p><br><br>
			</div>
		</div>
	</div>

<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect3').addClass('active');
	});
	$("#E-Banking").html('Private E-Banking');
</script>
</body>
</html>
