<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
<div class="container">
	<div class="col-md-12">
		<h1>Investment management</h1>
		<div class="col-md-9 col-md-offset-2">
			<h3>Achieving your financial goals begins with understanding them</h3>
				<p>
					Everything you’ve seen and experienced in your life has come together to make your situation absolutely unique. Your needs and those of your family, your business and your future are unlike anyone else’s and that’s the ideal way to approach your investments.
					<br><br>
					Your BP Bank Wealth Management relationship manager will engage you in a highly consultative process to determine your wishes and plans for what’s next in your life and to uncover your definition of success. Are you looking to increase your wealth? Safeguard it? Share it with others? Do you want to create a legacy that will last? In terms of your risk appetite, are you conservative? More growth-oriented?
					<br><br>
					Working with you, our team of investment professionals will develop, execute and monitor an investment portfolio created exclusively for you, designed with a view to effectively managing risk and helping you achieve your unique financial goals.
				</p>
			<h3>Solutions to Match Your Objectives</h3>
				<p>
					Depending on where you are in your life and what you’re trying to achieve with your wealth, we can discuss the merits of a wide variety of investment accounts and plans. These include Pledged accounts for loans with Private Banking, Personal and Corporate accounts, multi-currency accounts, margin accounts as well as online account access.
					With offices in New Zealand, we understand the local landscape and deliver wealth management with privacy and discretion, including online account access and paperless statement options.
				</p>
			<h3>A Portfolio Created Exclusively for You</h3>
				<p>
					You will benefit from our expertise and commitment to your financial success, providing you with a portfolio designed to achieve your goals and provide you with peace of mind. As part of our commitment, your relationship manager will work with you to determine what we believe are the right strategies to help you invest your hard-earned wealth for today and for the future. Starting by learning all they can about your financial situation - including your current profile, needs, concerns, risk tolerance, time horizon, liquidity and return objectives - your advisor can help you develop a personalized portfolio which includes a variety of international products and services and market strategies should you so choose.
				</p>
			<h3>Solutions to Match Your Preferred Level of Participation</h3>
				<p>
					If you prefer to play a more active role in the day-to-day management of your investments, you may choose to participate in one of our fee-based programs while receiving research, insights and ongoing advice from your advisor. These give you access to seasoned money managers while providing benefits usually enjoyed only by large institutional investors and pension funds.
					There are two types of fee-based programs to discuss with your advisor based on your financial complexity and portfolio size:
				</p>
				<ul>
					<li>Moderate Involvement Programs let you delegate everyday decisions</li>
					<li>Increased Involvement Programs give you more active participation in the portfolio management process</li>
				</ul>
				<p>
					In either case, you will work with your Wealth Advisor to develop an investment strategy that will help determine how your goals will be reflected. Your investment plan forms part of your total wealth plan, designed to cover every facet of your life-what you’ve accumulated and how best to administer it.
				</p><br><br>
		</div>
	</div>
</div>

<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect3').addClass('active');
	});
	$("#E-Banking").html('Private E-Banking');
</script>
</body>
</html>
