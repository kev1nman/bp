$(document).ready(function(){
	//Boton ir arriba
	$('.ir-arriba').click(function(){
		$('html, body').animate({
			//scrollTop: '0px'
			scrollTop: $("#ir-arriba").offset().top
		}, 200);
	});

	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
	//Menu vertical
	$(window).scroll(function(){
		if( $(this).scrollTop() > 1250 ){
			$('.menuVertical').slideDown(300);
		} else {
			$('.menuVertical').slideUp(300);
		}
	});
	//Menu vertical TOGGLE
	$(window).scroll(function(){
		if( $(this).scrollTop() > 1650 ){
			$('.menuVerticalToggle').slideDown(300);
		} else {
			$('.menuVerticalToggle').slideUp(300);
		}
	});

});
