<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Personal banking</h1>
			<p>
				We have the products that best meet your needs, <em>you decide how to handle them.</em>
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
		<h3>We have the products that best meet your needs, <em>you decide how to handle them.</em></h3>
	</div>
	<div class="sections col-md-12">
			<div class="sect col-md-offset-1 col-md-2 col-sm-12">
				<div class="section1">
					<div class="">
						<a href="<?php host();?>/personal/accounts/accounts.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" width="300"/></a>
					</div>
					<h4>Accounts</h4>
					<legend></legend>
					<a class="btn btn-info" href="<?php host();?>/personal/accounts/accounts.php">More info</a>
				</div>
			</div>
			<div class="sect col-lg-2 col-md-2 col-sm-12">
				<div class="section2">
					<div class="imgSections">
						<a href="<?php host();?>/personal/creditCard/creditCard.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/creditCard.jpg" alt="" /></a>
					</div>
					<h4>Credit Card</h4>
					<legend></legend>
					<a class="btn btn-info" href="<?php host();?>/personal/creditCard/creditCard.php">More info</a>
				</div>
			</div>
			<div class="sect col-lg-2 col-md-2 col-sm-12">
				<div class="section1">
					<div class="imgSections">
						<a href="<?php host();?>/personal/homeLoans/homeLoans.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" /></a>
					</div>
					<h4>Home loans</h4>
					<legend></legend>
					<a class="btn btn-info" href="<?php host();?>/personal/homeLoans/homeLoans.php">More info</a>
				</div>
			</div>
			<div class="sect col-lg-2 col-md-2 col-sm-12">
				<div class="section1">
					<div class="imgSections">
						<a href="<?php host();?>/personal/personalLoans/personalLoans.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/savingsPlus.jpg" alt="" /></a>
					</div>
					<h4>Personal loans</h4>
					<legend></legend>
					<a class="btn btn-info" href="<?php host();?>/personal/personalLoans/personalLoans.php">More info</a>
				</div>
			</div>
			<div class="sect col-lg-2 col-md-2 col-sm-12">
				<div class="section2">
					<div class="imgSections">
						<a href="<?php host();?>/personal/investment/investment.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/investment.jpg" alt="" /></a>
					</div>
					<h4>Investments</h4>
					<legend></legend>
					<a class="btn btn-info" href="<?php host();?>/personal/investment/investment.php">More info</a>
				</div>
			</div>
	</div>
	<?php
 		pie();
 	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
