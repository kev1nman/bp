	<!DOCTYPE html>
	<html lang="es">
	<head>
		<?php
			include '../../structure.php';
			cabecera();
		?>
	</head>
	<body>
		<?php
			menu();
		?>
		<div id="banner">
			<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
		</div>

			<div class="content">
				<div class="container">
					<h1>Personal loan</h1>
					<p>
						A loan you could use for a new car, travel or a studies. Whatever you need, an BP Bank Personal Loan could be an option.
					</p><br><br>
				</div>
			</div>

			<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
				<h3 class="text-center">Personal loan uses</h3>
				<h4 class="text-center"><em>Use this loan for almost anything</em></h4>
			</div>
			<div class="sections col-md-12">
				<div class="sect col-lg-4 col-md-4 col-sm-12 col-md-offset-2">
					<div class="section1">
						<div class="imgSections">
							<a href="<?php host();?>/personal/personalLoans/carLoans.php">
								<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" />
							</a>
						</div>
						<h3>Car loan</h3>
						<legend></legend>
						<p>
							Got your eye on some new wheels? Whether you are upgrading, or buying your first car, consider an BP Bank Car Loan.
						</p>
						<a class="btn btn-info" href="<?php host();?>/personal/personalLoans/carLoans.php">More info</a>
					</div>
				</div>
				<div class="sect col-lg-4 col-md-4 col-sm-12">
					<div class="section2">
						<div class="imgSections">
							<a href="<?php host();?>/personal/personalLoans/debtConsolidation.php">
								<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" />
							</a>
						</div>
						<h3>Debt Consolidation</h3>
						<p>
							Roll all your debts into easy to manage BP Bank Debt Consolidation loan and get your finances organised.
						</p>
						<a class="btn btn-info" href="<?php host();?>/personal/personalLoans/debtConsolidation.php">More info</a>
					</div>
				</div>
			</div>



			<div class="container col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<h2 class="text-center">Why Choose Us</h2>
					<h4 class="text-center"><em>Great reasons to choose our personal loan</em></h4>
					<ul>
						<li>Answers usually within 24 hours</li>
						<li>Make extra repayments at any time with no charges and no fees</li>
						<li>Borrow for just about anything</li>
						<li>Top up your loan for something new</li>
					</ul>
				</div>

				<div class="col-md-12">
					<h2 class="text-center">Get started</h2>
					<h4 class="text-center"><em>It's easy, get started today</em></h4>
						<div class="col-md-6">
							<p>
								<div class="col-md-12">
									<h4><b>01.</b> Calculate</h4>
									Work out how much you would like to borrow to reach your goal. Calculate your repayments over different timeframes for different amounts you could borrow.
								</div>
								<div class="col-md-12">
									<h4><b>03.</b> Get approved</h4>
									We’ll usually give you an answer within 24 hours and often we can give you an indication straight away.
								</div>
							</p>
						</div>
						<div class="col-md-6">
							<p>
								<div class="col-md-12">
									<h4><b>02.</b> Apply</h4>
									Ready?  Simply complete your application online.   If you are a Fast saver account online banking customer, apply here or call 0800 0000 000.
								</div>
								<div class="col-md-12">
									<h4><b>04.</b> Get your money</h4>
									For unsecured personal loans, once your loan has been approved and you’ve signed the loan documents, usually we’ll put money in your account on the same day. You can also apply to top up or extend your loan if you need more at a later time.
								</div>
							</p>
						</div>
				</div>
				<div class="col-md-12">
					<h2 class="text-center">Rates</h2>
						<h4 class="text-center"><em>Use this loan for almost anything</em></h4>
						<div class="col-md-4">
							<h3>12.95%p.a Fixed</h3>
							<h4>Secured personal loan</h4>
							<h5>(minimum amount $20,000)</h5>
						</div>
						<div class="col-md-4">
							<h3>17.95%p.a Fixed</h3>
							<h4>Unsecured personal loan</h4>
							<h5>(minimum amount $2,000)</h5>
						</div>
						<div class="col-md-4">
							<h3>15.95%p.a Fixed</h3>
							<h4>Debt consolidation loand</h4>
							<h5>(minimum amount $2,000)</h5>
						</div>
						<div class="col-md-12">
							<legend>Loan processing fee of $250.00 may apply. A PPSR (security registration) fee of $25 will apply to all secured personal loans.</legend>
							<br><br>
						</div>
				</div>
			</div>

		<?php
			pie();
		?>
		<script>
		    $(document).ready(function () {
		        $('#sect1').addClass('active');
		    });
			$("#E-Banking").html('Personal E-Banking');
		</script>
	</body>
	</html>
