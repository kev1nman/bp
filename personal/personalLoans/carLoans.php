<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="">
		<div class="content">
			<div class="container">
				<h1>Car loans</h1>
				<p>
					Got your eye on some new wheels? Whether you are upgrading, or buying your first car, consider an BP Bank Car Loan.
				</p><br><br>
			</div>
		</div>

		<div class="col-md-12 nopadding">
			<div class="sectLoand1 gris col-md-6">
				<div class="">
					<h1 class="text-center">Car loans</h1>
					<legend></legend>
					<h3>
						An BP Bank Car Loan puts you in the driver’s seat. Borrow for up to 5 years, make extra payments and pay off early with no penalty.
					</h3>
				</div>
			</div>
			<div class="sectLoand2 col-md-6">
				<div class="">
					<h3 class="text-center"><em>Great reasons to choose us</em></h3>
					<ul>
						<li>Answers usually within 24 hours</li>
						<li>Make extra repayments at any time with no charges and no fees</li>
						<li>Finance for new and used cars</li>
						<li>Maximum 5-year term</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-12 nopadding gris">
			<div class="sectLoand1 col-md-6">
				<div class="">
					<h3 class="text-center">Get started</h3>
					<legend></legend>
					<h4 class="text-center"><em>It's easy, get started today</em></h4>
					<div class="col-md-6">
						<p>
							<div class="col-md-12">
								<h4><b>01.</b> Calculate</h4>
								Check our car buyers’ guide for some pre-purchase tips, things you might want to consider when you buy, and work out how much you might need. Calculate your repayments over different timeframes and for different amounts to see how you can budget for them.
							</div>
							<div class="col-md-12">
								<h4><b>03.</b> Get approved</h4>
								We’ll usually give you an answer within 24 hours and often we can give you an indication straight away.
							</div>
						</p>
					</div>
					<div class="col-md-6">
						<p>
							<div class="col-md-12">
								<h4><b>02.</b> Apply</h4>
								Ready?  Simply complete your application online.   If you are a FastNet Classic online banking customer, apply here or call 0800 000 000. Check here for helpful information on your loan application.
							</div>
							<div class="col-md-12">
								<h4><b>04.</b> Get your money</h4>
								For unsecured car loans, once your loan has been approved and you’ve signed the loan documents, usually we’ll put money in your account on the same day.  Secured car loans work a little differently.
							</div>
						</p>
					</div>
				</div>
			</div>
			<div class="sectLoand2 col-md-6">
				<div class="">
					<h3 class="text-center">Rates</h3>
					<legend></legend>
					<h4 class="text-center"><em>Use this loan for almost anything</em></h4>
					<div class="col-md-6">
						<h3>12.95%p.a. Fixed</h3>
						<h4>Fixed rate secured personal loan</h4>
						<h5>(minimum amount $20,000)</h5>
					</div>
					<div class="col-md-6">
						<h3>17.95%p.a Fixed</h3>
						<h4>Unsecured personal loan</h4>
						<h5>(minimum amount $2,000)</h5>
					</div>
					<div class="col-md-12">
						<legend></legend>
						<h4>Loan processing fee of $250.00 may apply. A PPSR (security registration) fee of $25 will apply to all secured personal loans. You’ll find.</h4>
					</div>
				</div>
			</div>
		</div>

	</div>

	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
