<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>

	<div class="container">

		<div class="col-md-9">
			<h1>Buying your first home</h1>
			<p>
				Starting with the basics for first home buyers, we’ll talk you through home loans and every step of the process, so you can move forward with confidence.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="col-md-2 col-md-offset-1">
			<div class="section1">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" />
				</div>
				<h4>Getting started</h4>
				<legend></legend>
				<p>
					Begin with the basics like your mortgage deposit options, conditional approval and tips for first-home buyers.			</p>
				<a class="btn btn-info" href="#GettingStarted">More info…</a>
			</div>
		</div>
		<div class="sect col-md-2">
			<div class="section2">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" />
				</div>
				<h4>Do the maths</h4>
				<legend></legend>
				<p>
					We investigate the important question: "Can I afford it?"</p>
				<a class="btn btn-info" href="#DoTheMaths">More info…</a>
			</div>
		</div>
		<div class="sect col-md-2">
			<div class="section1">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/savingsPlus.jpg' alt="" />
				</div>
				<h4>House hunting</h4>
				<legend></legend>
				<p>
					What are you looking for in a home, and how might that affect you financially?</p>
				<a class="btn btn-info" href="#HouseHunting">More info…</a>
			</div>
		</div>
		<div class="sect col-md-2">
			<div class="section1">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/creditCard.jpg' alt="" />
				</div>
				<h4>Buying process</h4>
				<legend></legend>
				<p>
					A look at how making an offer, bidding at auction or submitting a tender works.</p>
				<a class="btn btn-info" href="#TheBuyingProcess">More info…</a>
			</div>
		</div>
		<div class="sect col-md-2">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img width="300" class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/investment.jpg' alt="" />
				</div>
				<h4>Settlement</h4>
				<legend></legend>
				<p>
					What happens at settlement and once you’ve moved in.</p>
				<a class="btn btn-info" href="#Settlement">More info…</a>
			</div>
		</div>
	</div>
	<div class="contenido col-md-12">

		<!-- MENU VERTICAL -->
		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1>
				<legend></legend>
			</div>

			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							Buying your first home</a>
						</h4>
					</div>

					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<ul>
								<li><a href="#BuyingYourFirstHome">Buying your first home</a>
									<ul>
										<li><a href="#GettingStarted">Getting started</a></li>
										<li><a href="#DoTheMaths">Do the maths</a></li>
										<li><a href="#HouseHunting">House hunting</a></li>
										<li><a href="#TheBuyingProcess">The buying process</a></li>
										<li><a href="#Settlement">Settlement</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
			<!--Fast saver account-------------------------------------->
			<h2 id="BuyingYourFirstHome">Buying your first home</h2>
				<p>
					Starting with the basics for first home buyers, we’ll talk you through home loans and every step of the process, so you can move forward with confidence.
				</p>
			<h3 id="GettingStarted">Getting started</h3>
				<p>
					If you’re looking to buy your first home, a great place to start is with your deposit for a home loan. Most people will need 20%* of the property price as a deposit, but there are other options available to help you get into your first home quicker. Check out our deposit options for first home buyers, or read on for a handy overview of the home buying process.
				</p>
				<h4>
					The Home Loan journey
				</h4><p>
					You're on a mission to buy your first home, and like most New Zealanders, you need a home loan to help you get there. Saving your deposit is usually the first step in the journey, or you might like to see whether your family could help you out - take a look at your deposit options to explore how they could do so with things like Family Springboard.
					<br> You'll also want to review your budget, and calculate what you could afford. We've got the home loan calculators to help you out. In the meantime, check out our simple guide to the home loan process below.
				</p>
				<h4>
					Get conditionally approved for a Home Loan
				</h4><p>
					With a conditional approval** on a home loan you can house hunt with confidence, knowing exactly how much you’ll be able to afford.
				</p>
				<h4>
					Should I apply for conditional approval?
				</h4><p>
					That’s completely up to you, although it’s a common practice here in New Zealand and can be helpful for first home buyers who may not know how much money they can borrow. However some people will wait until they’ve found a home they want to make an offer on, or have made an offer already. 
				</p>
				<h4>
					What does conditional approval mean?
				</h4><p>
					Conditional approval means you’ve been approved as a borrower providing nothing changes for you, and the home you choose meets the loan provider’s lending conditions. This conditional approval tells sellers you’re a serious buyer – and if you want to bid at auction, conditional approval is a must-have, so please talk to us first. 
					<br>A conditional approval from Westpac is valid for 6 months, so you have plenty of time to house hunt.  Renewing your conditional approval is easy too – just contact us when the expiry date is approaching.
					<br>With conditional approval on a loan you'll know what you can afford, giving you the ability to quickly make a decision once you've found a great property. 
	 				<b>Note:</b> If you’re buying at auction and want to buy an apartment, Auckland investment property or a lifestyle block, talk to us first as conditional approval may not cover some circumstances, and we may need some additional information from you.
				</p>
				<h4>
					Apply for conditional approval, or get some more help
				</h4><p>
					If you’re ready to apply, great! You can apply online and could be conditionally approved within 60 seconds (we’ll still need to see the required supporting documents before you can get on with renovating or making a bid on a house). You can also get one of our Mobile Mortgage Managers to come and see you, or visit a branch.
					Or if you’re not ready to apply but want to talk to us about how we can help, simply click on here to request a call back. We’ll get back to you quickly.
				</p>
				<h4>
					Apply for conditional approval, or get some more help
				</h4><p>
					If you’re ready to apply, great! You can apply online and could be conditionally approved within 60 seconds (we’ll still need to see the required supporting documents before you can get on with renovating or making a bid on a house). You can also get one of our Mobile Mortgage Managers to come and see you, or visit a branch.
					Or if you’re not ready to apply but want to talk to us about how we can help, simply click on here to request a call back. We’ll get back to you quickly.
				</p>
				<h4>
					Looking for a low deposit home loan?
				</h4><p>
					Check out deposit options to see how you may be able to get help from a Welcome Home Loan, Family Springboard or more.
				</p>
				<h4>
					What types of ownership are there?
				</h4><p>
					<b>Freehold:</b> this is the most common type of ownership. It means you own the land and house with virtually no restrictions on your ownership rights.
					<br>
					<b>Leasehold:</b> with this type of ownership you lease the land and pay rent to the landowner. You own the house but your use of the land may be restricted, and the rent can go up. You can sell the lease if you want to move, but you need to get the landowner’s consent first.
					<br>
					<b>Cross-lease:</b> this is where there are several homes on a piece of land and all the owners own the land together. Each owner leases the land their home is on from the others for a small cost.
					<br>
					<b>Unit title:</b> you own or lease your unit but common areas (like stairways and parking) are managed by the body corporate.
					<br>
					<b>Company title:</b> if you buy a flat with a company title, you buy ‘shares’ that give you the right to live there. The company administers and maintains the block of flats.
					<br>
					<b>Licence to occupy:</b> with this type of ownership you don’t actually own the land or buildings, but you could have a right to live there for life. This is the most common type of ownership for retirement villages.
				</p>
				<h4>
					Are you buying with someone else?
				</h4><p>
					There are two main ways of sharing the ownership of a home.
					<b>With a joint tenancy,</b> you own the home together and if one person dies the others take over the ownership – this is the way most couples own a home together.
					<b>With a tenancy in common,</b> you each own a share and can leave your share to anyone you wish in your will – this is more common when there are several owners.
					Another option is a property sharing agreement, but your lawyer can advise you about this and on the way to best set things up for your situation.
				</p>
				<h4>
					Home buying How-To Guides
				</h4><p>
					We’ve got the How-To list to help with everything home buying – from when you first apply for a loan and start house hunting, right through to settlement and the moving in process.
				</p>
				<h4>
					When applying for a loan
				</h4><p>
					Talk to the bank early on and find out what their lending requirements are
					find a lender you feel comfortable dealing with and keep in touch with them
					create a good savings history – even three to six months of regular saving can help
					work out a budget before you see the bank to show you are well organised financially
					pay off as many debts as you can before you apply for a loan (having other debts can reduce the amount you can borrow)
					reduce the limits on your credit cards (lenders will take into account the maximum you could owe on credit cards)
					try to gather up the information the bank needs before you apply (it’ll save you time and means you’ll get your answer faster)
				</p>
				<h4>
					Here's a list of things to have ready when you apply
				</h4><p><b>When searching for a property</b><br>
					Decide what areas you’re interested in
					make a list of everything you’d like in a home (in priority order)
					do a budget and gather up your financial information
					talk to your lender – and ask about a loan conditional approval
					do your research – talk to agents, read the papers and go online
					find a good lawyer – ring several for quotes
					look at as many homes as you can – and ‘score’ them.
				</p>
				<h4>
					Download the 'How does that home score?' checklist
				</h4><p><b>Before making your offer</b><br>
					Register your interest if the property is being sold by auction or tender
					check the property out – use our checklist and talk with the local council
					estimate the value – ask the agent about recent sales and price trends
					check local values with Quotable Value – QV.co.nz
					talk with your lawyer about your offer (or the auction or tender documents)
					get a LIM report unless your offer is conditional on the LIM report
					ask your bank what finance conditions you need in your offer
					check with your lawyer each time before you sign or countersign any papers.
				</p>
				<h4>
					Once your offer is accepted
				</h4><p>
					Pay the real estate deposit to the real estate agent
					give a copy of the sale and purchase agreement to your lawyer and lender
					decide what sort of home loan you’d like
					arrange for a valuation report if one is needed*
					organise builder’s and engineer’s reports if they’re needed*
					if you haven't already, get a LIM report and check the district plan* (your lawyer usually does this)
					arrange insurance for the new home
					think about making a new Will
					make a time with your lawyer and bank to sign papers
					set up any new bank accounts and automatic payments needed.
				</p>
				<h4>
					Read more about when your offer’s accepted
				</h4><p><b>At settlement time</b><br>
					Do a pre-settlement check of the property
					make sure you have your share of the money ready to pay to the lawyer
					on settlement day check with your lawyer that everything is going ahead
					after settlement check the statement and papers your lawyer sends you
					make sure you get a copy of the title showing you are the new owner
					check your loan and insurance payments are going out as expected.
				</p>
				<h4>
					Read more about settlement
				</h4><p><b>Arranging the move</b><br>
					If you’re renting, give notice and apply for your bond back
					get quotes from several moving companies
					check your home and contents insurance covers the move
					arrange a moving time with the other owners
					contact power, gas, TV, phone and Internet companies
					redirect your mail and send change of address cards.<br><br>
					You may want to do these things before you make your offer but most people do them afterwards because they don’t want to spend the money until they know their offer will be accepted. If you want to buy at auction or by unconditional tender you’ll need to do all your checks beforehand.
				</p>
			<h3 id="DoTheMaths">Do the maths</h3>
				<h4>
					Can you afford to buy property?
				</h4>
				<p>
					The information below will give you a good head-start to answering this important question, and includes a guide to what things cost. Make sure you try out our handy calculators too.
					<br>
					Once you’re ready, talking to one of our Mobile Mortgage Managers is a great way to determine your financial situation, so give us a call and we’ll send one out to visit you.
				</p>
				<h4>
					How much can I borrow?
				</h4><p>
					How much you can borrow depends on a few factors, like:<br><br>
					The value of the home you want to buy
					how much equity or deposit you have to contribute
					how much you can afford to pay towards your mortgage.
				</p>
				<h4>
					Try our calculators to see how much you could afford
				</h4><p><b>How much will I need for a deposit?</b><br>
					Every lender has different lending guidelines, but you may be able to borrow up to:<br><br>
					90% of the house’s market value (or the price you pay, whichever is less) depending on your situation. That means saving at least a deposit of 10% – but don’t stress, there are ways you could get help with your deposit.
					<br><br>
					There are some situations where you may need a greater deposit, including:<br><br>
					<br>65% for a small or investment apartment, or up to 85% for a larger owner-occupied apartment.
					<br>80% of the land’s market value for a section, depending on the area and services such as water and power.
					<br>70% for an investment property in Auckland.
					<br>90% if you’re planning to have a home built from scratch.<br><br>
					Generally, most lenders say your total loan payments (for all debts) shouldn’t be more than about a third of your income before tax – but they’ll also take your other expenses into account. You still need to have life after buying your home!
					<br><b>Look at your deposit options</b>
				</p>
				<h4>
					A few costs related to buying a home
				</h4><p>
					When you’re working out if you’re in the financial position to buy a home, don’t forget to take into consideration all those extra expenses during the process, like legal fees and valuation reports. Let’s take a look at some costs to be prepared for.
					<br><br><b>Costs from your lawyer</b>
				</p><p>
					<b>To buy a home</b><br><br>
					$700-$1,800 for legal fees<br>
					$200-$300 for costs such as land transfer fees.
				</p><p>
					<b>For conveyancing – transferring ownership of the property into your name</b><br><br>
					Between $600 and $2000<br>
				</p><p>
					<b>Costs from your real estate agent/s</b><br><br>
					If you’re buying, there’s no fee.<br>
					If you’re selling you can usually expect to pay:<br>
					A base fee of around $500, although not all agents charge this
					a commission based on the amount the home sells for – usually up to 4% for a certain sale price, then a lower percent for the rest, or
					you may be able to negotiate a fixed fee
					there may also be advertising costs.
				</p><p>
					<b>Costs from a registered valuer</b><br><br>
					For a full registered valuation report: $500–$800 <br>
					QV E-Valuer report: $40
				</p><p>
					<b>Cost for building and engineer’s reports</b><br><br>
					For a building surveyor’s report: $400–$1,150 <br>
					For an engineer’s report: $1,500–$4,000 <br><br>
					Your lender may require a valuation, building or engineer’s report as part of your conditional approval or loan agreement. You may also want to get these reports for your own benefit, so you have more information about the property.
				</p>
			<h3 id="HouseHunting">House hunting</h3>
				<p>
					Why is location so important when looking for your home? How can you know whether the property you purchase will be a good investment? And what are the implications of buying, say, an old house versus a newer one?
					We cover some of the things to take note of while you’re house hunting. 
				<h4>
					Where to start
				</h4>
				The key to house hunting is being methodical and organised with your approach, so here are some tips for starting your property search:
				<br><br>
				Make a list of the characteristics you’re looking for in your home, from location to property type
				use our calculators to get a sense of what you can afford
				you might also like to get conditional approval, so you know exactly how much you could borrow
				spend some time getting a feel for the market and researching properties. You can drive around, or try online in places like trademe.co.nz or realestate.co.nz
				the Realestate app is an amazing tool to have at your fingertips while you’re out house hunting, so download it before you go.
				</p>
				<h4>
					Location and your finances
				</h4><p>
					<b>Why buy the worst house in the best street?</b><br><br>
					Being in a good area rubs off on the value of your house. Then, you may make a further gain by improving your property to match others in the area. Before you go ahead though, have a good think about whether you’ll have the time or money to renovate.
					<br><b>Explore Building or Renovating</b>
				</p><p>
					<b>Tips for finding a good area</b><br><br>
					While you're house hunting:<br><br>
					Talk to family and friends about the areas they live in
					ask a real estate agent (or valuer) about recent sales and price trends – look for areas where houses are selling well and prices are rising
					look for an area with good facilities, such as transport, shops, schools, cafes, sporting venues and entertainment
					also look for areas that are attractive – with views, established gardens, lots of trees, or attractive homes for instance
					in older areas, look for locations where you can see places are being renovated and facilities look cared for
					in newer areas, look for locations where there’s a variety of home designs – and effort going into planting and landscaping
					remember, most people like sun, shelter, privacy, views and flat land – areas that offer all this usually sell well
					check the zoning of the area with your local authority, and if there are any changes planned – if the area is zoned commercial you could find your home surrounded by businesses in the future.
				</p><p>
					<b>Do your due diligence before you buy</b><br><br>
					Some checks are really important to do before you purchase the home you’re looking at – and if they're not done, can even affect your ability to get a home loan. Here are some essential things to do:<br><br>
					<ul>
						<li>1. Contact the council</li>
							Ask your local and regional councils for information about the area and any future plans, including zoning and other development rules.
						<li>2. Apply for a LIM report</li>
							A Land Information Memorandum (LIM) from the local authority gives you all sorts of valuable information about drainage, roads, flooding, erosion, consents etc.
						<li>3. Get a builder’s report</li>
							Get a report on the property from a licensed building practitioner. Ask them what their report will and won’t cover, and also ask them for an idea of what it might cost to fix any problems they find. If there could be any problems with the land or large structures you should also get a report from an engineer.
							You may also want to check with the Weathertightness service to see if there’s been a leaky home claim for the property.
						<li>4. Check the title to the property</li>
							This will tell you if there are any restrictions that could affect your ownership or use of the property. The agent should have a copy of the title. Also talk to your lawyer about the title and any other checks they think you should do.
							You might also want to ask your lawyer about title insurance. It could help protect you if you find later on that the boundaries are wrong or there’s been illegal work done on the property.
					</ul>
				</p>
				<h4>
					Costs for certain types of homes
				</h4>
				<p>
					When you're buying a house or apartment, its age, quality of build and other factors could affect the costs you may face in the future. So here are some things to keep in mind while you're out house hunting.
				</p>
				<p>
					<b>Older homes</b><br><br>
						Older homes can be hard to heat as they often have no insulation – that can mean higher power bills
						it can be hard to know what’s behind the walls, so alterations can be expensive
						sometimes even if you only want to make small changes, you’ll end up having to do other work to get building consent
						some older homes may have asbestos products or paints containing lead, which require a specialist contractor to remove.
					<br><br>
						If you’ve fallen in love with an older home and want to buy it, get someone qualified in to check the walls, insulation and any of the other considerations above first. A building inspection report is a good idea before going unconditional on an offer.
				</p><p>
					<b>New homes</b><br><br>
						Don’t forget to consider potential extra costs of landscaping or buying curtains and carpets.
						check the quality of the building materials and finish – building jobs that have been rushed or had shortcuts when it comes to the materials can end up costing you money in the future.
				</p><p>
					<b>Apartments</b><br><br>
					Most apartment complexes have a body corporate that all the owners belong to, and you’ll pay a levy to cover building running costs and maintenance. Every body corporate is different, so make sure you ask about your levy, the rules of the complex and what you can do with your apartment – as this could affect the value of your investment.
					<br><br>Older apartment buildings:<br><br>
					May need expensive maintenance
					because many earlier conversions were poorly done, this could make finance and insurance harder to get.
					<br><br>Newer apartments:<br><br>
					check the building quality and soundproofing, because again, maintenance could end up being expensive.
					<br><b>Download the ‘What to look out for’ checklist</b>
				</p>
			<h3 id="TheBuyingProcess">The buying process</h3>
				<p>
					There are three main ways to buy a home:
				</p>
				<ul>
					<li>1	By bidding at auction</li>
					<li>2	By offer and negotiation</li>
					<li>3	By submitting an offer by written tender (by tender) or similarly, by set date of sale</li>
				</ul>
				<h4>
					Buying by auction
				</h4><p>
					At an auction, everyone interested in the property bids against each other until only one bidder is left. If you buy at auction it’s unconditional (legally binding), so you need to arrange your finance and do all the legal and other checks beforehand.
				</p><p><b>How does the auction work?</b><br><br>
					The auctioneer runs the auction and tells you what amounts they will accept. They’ll try to start high but towards the end they may accept bids of $1,000, or even $500 or less
					the seller usually sets a reserve price and tells the auctioneer what it is. If the final bid is over the reserve price, the home is sold and the buyer pays a deposit, usually 10%, to the auctioneer. Settlement (the day you get ownership) is usually set for 20 days later, but can often be negotiated
					if the reserve isn’t reached, the home is ‘passed in’, meaning it didn’t sell at auction. Often it sells by negotiation straight after the auction. If you're the highest bidder you have the first chance to negotiate and can add conditions to the contract at this stage if you need to.
				</p><p><b>Before you buy at auction</b><br><br>
					Go and watch a couple of auctions to get a feel for how they work
					register your interest with the agent
					get a copy of the auction contract
					do your due diligence with your lawyer (property titles), the council (LIM) and get other reports done (valuation, building inspection etc)
					arrange your finance with your lender and have your money ready to pay a deposit
					A conditional approval* is important here as it means you've been approved as a borrower providing nothing changes for you, and the home you choose meets the loan provider's lending conditions.
					decide on your top price
					the seller may consider offers before the auction. If an offer's deemed to be potentially acceptable by the seller, the real estate agent may contact all potential buyers who have registered their interest and invite them to also put forward their offers for consideration. Or the auction date may be brought forward, with the initial offer becoming the opening bid.
				</p>p><b>We may be able to help with your deposit</b><br><br>
					If the money you need to give the agent or auctioneer as your deposit is tied up – perhaps as equity in your current home or in an investment you can’t break yet – talk with us. We may be able to help by lending you the money you need for a short time, or by guaranteeing your deposit.
				</p>
				<h4>
					Making an offer
				</h4><p>
					Many homes are sold through offer and negotiation, although auctions and tenders are often used in sought-after areas, in Auckland or if the home has special features (where it can be challenging for sellers to set what they feel is an accurate price). When you make an offer through your real estate agent, the seller may accept your offer straight away or there may be a negotiation process until both you and the seller are happy with the price and the conditions.
				</p><p><b>What’s good about this way to buy?</b><br><br>
					The big plus about buying by offer is that you can take time to think – and you can put in conditions that let you check the place out before you’re fully committed.
				</p><p><b>Common conditions added to an offer</b><br><br>
					<b>Finance –</b> this gives you time to arrange your loan. Make sure it says “finance on terms satisfactory to you” or you could be forced to borrow on terms you don’t like
					<b>Title search –</b> so your lawyer can check there are no problems with the title, or restrictions, covenants or easements you need to know about
					<b>Valuation report –</b> so you can check the market price. Your lender may also require a valuation report as part of the loan agreement
					<b>LIM report –</b> so you can check what the council knows about the property and make sure there are no problems with things like consents or flooding
					<b>Building inspection report –</b> so you can check the building is sound and find out about any problems that might cost money
					<b>Engineer’s report –</b> so you can check any structural or land issues
					<b>Sale of another home –</b> if you need to sell one home to buy another.
				</p><p>
					You might also want to add other conditions covering things like repairs the seller has said they’ll fix or extra items they’ve agreed to leave. Your conditions need to state that the report, finance or repairs must be satisfactory to you. Otherwise you’ll still have to go ahead even if you’re not happy with the results.
				</p><p><b>About your sale and purchase agreement</b><br><br>
					The sale and purchase agreement sets out in writing all the agreed terms and conditions of the purchase. It can vary, but most transactions will use a standard legal contract created by the Real Estate Institute and the Auckland District Law Society. It will include the agreed price, list of chattels, type of title, list of conditions the buyer and seller want fulfilled, date the agreement will become unconditional, settlement date and deposit the buyer must pay. It also covers other things like responsibilities under various laws and what happens if settlement is late – and lets you set out obligations you want the seller to abide by (for example access to the property).
					You’ll need to have it checked by your lawyer before you sign it – and each time any of the conditions change during negotiation. The agreement becomes binding once both you and the seller have signed it and initialled all the changes. You can stop negotiating at any time up until then.
					Your lender will also need to see the sale and purchase agreement after the deal is done, but talk to them beforehand to check if they have any specific clauses they want added.
				</p><p><b>What if my offer is unconditional?</b><br><br>
					If you make an unconditional offer you need to sort out your loan and everything else beforehand because once the offer is accepted, you have to go through with the sale. If you break the contract you can be sued, so think carefully before going unconditional.
				</p><p><b>How the money is paid</b><br><br>
					Once everything’s agreed, you pay a deposit of 5-10% of the sale price to the agent. The agent pays the money to the seller when your offer becomes unconditional. Your lender pays the rest of the money to your lawyer on settlement day.
				</p>
				<h4>
					Buying by tender or set date of sale
				</h4><p>
					With a tender or set date of sale method, you make a written bid for the property. It needs to be your best offer, as usually everyone who’s interested in the property puts in their offers at the same time – and you're unlikely to get a chance to negotiate. You can put conditions in your offer if you want, but it’s better if you check things out before hand instead because the more conditions your offer has, the less attractive it will be to the seller.
					<br><br>
					The seller may accept the highest offer – or decide to negotiate with the person whose offer they like best. Or they could reject all the offers. You don’t get the chance to find out what the other offers are.
				</p><p><b>How do you go about it?</b><br><br>
					Register your interest with the agent
					get a copy of the tender or set date of sale document – it tells you how offers must be made, and gives details like the settlement date
					discuss the tender or set date of sale document with your lawyer and prepare a written offer
					get a valuation and other reports, like a LIM, first so you know the market value
					when you put your offer in you may have to include a deposit – this is returned/refunded if your bid is not successful (this is usually the case with tenders, but not always with set date of sale)
					if your offer is accepted you're committed to buying the property, and have a set amount of time to meet all the sale conditions
					often the seller is prepared to look at offers before the tender or end of the set date of sale, so if you get in quick you may be able to make an offer before other buyers are ready. 
				</p><p><b>Closed and open tenders, set date of sale – what’s the difference?</b><br><br>
					Tenders or set date of sale are usually arranged through real estate agents. If the tender is ‘closed’ it means offers have to be in by a certain date and won’t be considered before then. An ‘open’ tender means there is no time limit. With a set date of sale, the seller can sell the property before the specified date.
				</p>
			<h3 id="Settlement">Settlement</h3>
				<p>
					Your offer’s been accepted and you can breathe a little easier – or go celebrate! This is the time when your lawyer will take over the process of settling the purchase, transferring the property into your name, and get you to sign a few papers.
					Not long now until you can move in to your new home.
				</p>
				<h4>
					When your offer is accepted – conveyancing
				</h4><p>
					Made your offer, perhaps negotiated with the seller on conditions, and now your offer has been accepted? Great! It’s now time for your lawyer to make sure the conditions in your agreement are met, and start doing the legal work to transfer the property to your name. This legal process – including checking and registering documents to transfer the ownership over – is called conveyancing.
				</p><p><b>Are you insured?</b><br><br>
					Your lawyer will also check the property is insured – this is usually a condition of your home loan. If you’re applying for a home loan with Westpac, we can help you arrange all your insurances (like home, contents and personal protection insurance etc) when you apply. You can also check out what you may need in the Insurance section.
				</p>
				<h4>
					What happens on settlement day
				</h4><p>
					Once you’ve made an offer, it’s been accepted and any conditions are met, along comes settlement day. Your lawyer works to settle the deal and does the transfer of ownership, and there’s nothing for you to do but check in with your lawyer to make sure everything’s been settled.
				</p><p>
					Your lawyer’s work includes:<br><br>
					Doing a guaranteed title search
					liaising with the seller’s lawyer to make sure you receive a clear title
					paying the money to the seller’s lawyer
					ensuring the seller’s lawyer does their side of the electronic dealing
					completing the transfer using Landonline
					final details such as where you get the keys and when you can move in!<br><br>
					The money paid on settlement day takes into account the deposit you’ve already paid on the home to the real estate agent.
				</p>
				<h4>
					What’s next after settlement?
				</h4><p>
					Legal stuff
				</p><p>
					After settlement the lawyer will:<br><br>
					Provide you with a statement showing all the purchase details
					send a copy of the title, mortgage and certificate of insurance to your lender
					give you a copy of the title showing you registered as the new owner.
				</p><p><b>Your first loan repayment</b><br><br>
					We’ll let you know when your first repayment is due, so please check that you have sufficient funds on that day. It’s always a good idea to set your repayments for the day after your salary or wages have been paid into your account.
					You’ll also get regular loan statements so you can keep an eye on your loan repayments and balance (you can check it online or at your nearest branch anytime).
				</p><p><b>Keep an eye on your loan</b><br><br>
					If your circumstances change, ask us how we can help to adapt your loan. You can speed up your loan payments to save money on interest, or if things get a bit tight, we could look at ways to help lower your repayments.
				</p><p><b>Look into managing your loan</b></p>
				<p>
					<b>When your fixed term is ending</b><br><br>
					If you have a fixed or capped rate loan we’ll be in touch before the fixed term is due to end, to remind you to consider what you’d like to do next. We can move it straight into another fixed or capped rate – or it may be a good time to restructure your loan.
				</p>
		</div>

	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
