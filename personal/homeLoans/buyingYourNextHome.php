<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">

		<div class="col-md-9">
			<h1>Buying your next home</h1>
			<p>
				Buying your next home often means selling your existing one – which means there’s a lot to consider. Let’s take a look at the process of buying and selling, and how it could work with your home loan.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="sect col-md-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" />
				</div>
				<h3>Getting started</h3>
				<legend></legend>
				<p>
					Know where you stand, what you want, and how to get there.
				</p>
				<a class="btn btn-info" href="#GettingStarted">More info…</a>
			</div>
		</div>
		<div class="sect col-md-4">
			<div class="section2 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/savingsPlus.jpg' alt="" />
				</div>
				<h3>Ways you can sell your home</h3>
				<legend></legend>
				<p>
					A look at the process of selling by auction, offer, tender and set date of sale
				</p>
				<a class="btn btn-info" href="#WaysYouCanSellYourHome">More info…</a>
			</div>
		</div>
		<div class="sect col-md-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" />
				</div>
				<h3>Financial considerations when you sell your home</h3>
				<legend></legend>
				<p>
					How your Choices Home Loan adapts to buying and selling property; bridging finance and more.
				</p>
				<a class="btn btn-info" href="#FinancialConsiderationsWhenYouSellYourHome">More info…</a>
			</div>
		</div>
	</div>
	<div class="contenido col-md-12">

		<!-- MENU VERTICAL -->
		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1>
			</div>

			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							Buying your next home</a>
						</h4>
					</div>

					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<ul>
								<li><a href="#BuyingYourNextHome">Buying your next home</a>
									<ul>
										<li><a href="#GettingStarted">Getting started</a></li>
										<li><a href="#WaysYouCanSellYourHome">Ways you can sell your home</a></li>
										<li><a href="#FinancialConsiderationsWhenYouSellYourHome">Financial considerations when you sell your home</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
			<!--Fast saver account-------------------------------------->
			<h2 id="BuyingYourNextHome">Buying your first home</h2>
				<p>
					Buying your next home often means selling your existing one – which means there’s a lot to consider. Let’s take a look at the process of buying and selling, and how it could work with your home loan.
				</p>
			<h3 id="GettingStarted">Getting started</h3>
				<p>
					Like any big decision, it pays to sit down and make a plan before you buy another property. Whether you're looking to upgrade because you’ve built up equity in your home, you’re relocating for work or your family is expanding and you want to be in the right school zone for your kids, take the time to ask yourself lots of questions.
					Don’t forget, we have Mobile Mortgage Managers who can come to you and talk through your finances and options, so give them a call. 
				</p>

				<h4>Make a plan</h4>
					<p>
						Getting your plans sorted is always a great first step, so when you’re gearing up to buy your next home think about the process of selling your current one too. Or, whether it might be worth keeping as a rental property. Here are three strategies to consider.

					</p>
					<b>1.  Selling your home before you buy the next</b><br>
					<p>
						Selling before you buy means you can avoid having to cover two mortgages at once.
						If your current home loan is with us, you have the option of portability. This means you may be able to ‘take the loan with you’ when you move, saving you the cost and inconvenience of establishing a new loan, and you’ll retain the existing account number.
						Have a talk with us to find out what will suit your home loan best, if you’re thinking of selling your home before buying again.
					</p>
					<b>2.  Buying a new home before you’ve sold your current one</b><br>
					<p>
						If there’s likely to be a gap between buying your new home and getting the money from the sale of your current home, we may be able to increase your Choices home loan for a short time, or help with bridging finance. Bridging finance is an interest-only loan for a short term to help ‘bridge’ the gap when you have mortgages over two homes.
						Paying mortgages for two homes at the same time may not be easy. So if you’re thinking of buying a second home before selling your curent home, please talk to us well in advance (before you sign anything) to see how we can help.
					</p>
					<b>3.  Keeping your home as an investment, and buying another</b><br>
					<p>
						If you do the maths you might be surprised to find that keeping your current home as an investment property can be within your reach, and you could potentially use the equity in your home as the deposit for the home loan for your new one.
						Hop over to the ‘Options besides selling’ tab on this page, or get more detailed information about equity and owning a rental property in the Invest in Property section.
					</p>
					<b>Get a full Home Loan health check</b><br>
					<p>
						Before you list your home for sale, talk to one of our Mobile Mortgage Managers or pop into branch to get a full health check of your home loan as it stands. They may also be able to give you a conditionally approved limit for buying your next home, and talk through all your options including bridging finance if you need to buy a new home before your current one has sold.
					</p>

				<h4>Could you renovate, subdivide or make it a rental?</h4>
					<p>
						Before you decide to sell, here are a couple of things to think about first: could you get the home you want by renovating instead? Or would keeping your current home as a rental be a good investment? Moving home does cost money, so have a think about all your options first.
						<a href="#">Review the costs of moving</a>
					</p>
					<b>The good and the not-so-good of renovating</b><br>
					<b>Advantages</b><br>
						<p>
							Renovating can make your home feel brand new, and give you just the home you want
							moving your home can be a huge job, taking up your time and energy with everything from house hunting to packing up (and unpacking at the other end)
							save money. Moving could cost thousands of dollars, while estate agents’ fees could be 4% or more of the price you sell your home for.
						</p>
					<b>Things to consider</b><br>
						<p>
							You could overcapitalise, spending more money on your home than it’s worth. If your home ends up being the nicest house on the street, you may not get all your money back when you sell
							renovating isn’t for everyone, and it can be hard living in a home during renovations
							if you have to move out of your home during renovations, it could add quite a bit of cost to the project budget.
							<a href="#">Learn more about building and renovating</a>
						</p>
					<b>Could it be subdivided?</b><br>
						<p>
							The value of your property is likely to increase if your section can be subdivided, as another home could be built on the land and sold. Subdividing is likely to attract consent fees, so check it out with your local authority first.
						</p>
					<b>Owning a rental property</b><br>
						<p>
							Have you thought about keeping your current home as an investment property? It might be more affordable than you think. You’ll want to start by working out how much you could get in rent for your home – the Department of Building & Housing is a great resource for this – and whether that rent would cover your costs, as well as things like how much equity you currently have in your home.
							Check out the Investing in property section, or get one of our <a href="#">Investment Property Lending Specialists</a> to come to you to work out where you stand.
							<a href="#">Read about investing in property</a>
						</p>
					<b>Would your home make a good rental?</b><br>
					<p>
						A good place to start is by asking yourself some key questions:<br>
						<ul>
							<li>Is it in a good, safe area?</li>
							<li>Are there facilities nearby such as shops, a medical centre, or sports grounds?</li>
							<li>Is it close to public transport?</li>
							<li>Is it in good condition?</li>
							<li>Is it a low maintenance property?</li>
							<li>Are the living areas a reasonable size?</li>
							<li>Does it have 2-3 bedrooms?</li>
							<li>Is there a garage or off-street parking?</li>
							<li>How much rent could I expect to get? (Our Property Investor Report could help you here.)</li>
							<li>Would the rent cover the loan, rates, insurance and upkeep for the property? (Try our Property Investment calculator).</li>
						</ul>
						If you’ve answered mainly ‘yes’, the chances are your home has reasonable rental potential. Research is key, and you can learn more in our Invest in Property section.
					</p>


			<h3 id="WaysYouCanSellYourHome">Ways you can sell your home</h3>
				<p>
					You may have bought your current home by making an offer and negotiating from there; or at auction, by submitting a tender or by set date of sale.
					Now that you’re on the other side and looking to sell your home, what do you need to consider from a seller's point of view?
				</p>
				<h4>The pros and cons of each way to sell</h4>
				<table class="table">
					<tr class="info">
						<td>Selling by</td>
						<td>Advantages</td>
						<td>Disadvantages</td>
					</tr>
					<tr>
						<td class="info" colspan="3">
							Offer and negotiation
						</td>
					</tr>
					<tr>
						<td>Can be done through a sole or general agency, or privately</td>
						<td>There’s less pressure on the seller
							you can take your time to consider offers and wait for the right price
							you can negotiate until you get a deal that suits you
							buyers have a price range to guide them
							many buyers prefer this method because they don't have to compete in a high pressure environment
						</td>
						<td>You need to be sure of your asking price
							buyers may try to negotiate the price down
							offers may have conditions included
						</td>
					</tr>
					<tr>
						<td class="info" colspan="3">
							Auction
						</td>
					</tr>
					<tr>
						<td>Can only be done through a sole agency</td>
						<td>You don't have to set an asking price (but you do set a reserve, which is private)
							you have a set day for the auction
							a keen buyer may pay a top price to buy the property before it goes to auction
							competition between buyers on auction day may push the price up
							a sale at the auction is unconditional
							if the home doesn't sell you can negotiate with the bidder/s
						</td>
						<td>It's a very public process
							only cash buyers can bid, which can mean fewer potential buyers
							you may have advertising costs to pay even if you don't sell, on top of real estate fees
						</td>
					</tr>
					<tr>
						<td class="info" colspan="3">
							Tender, or set date of sale
						</td>
					</tr>
					<tr>
						<td>Can only be done through a sole agency, or privately</td>
						<td>You don't have to set an asking price
							it's a private way to sell
							buyers put in their best offers - there's an element of competition between buyers
							offers are usually received by a set date
							you don't have to accept the offers/sell
							you can sell early if you want
							you can negotiate with some or all those who put in offers
						</td>
						<td>Some potential buyers are put off by the 'closed' nature of tenders
							you may have advertising costs to pay even if you don't sell
						</td>
					</tr>
				</table>

				<h4>Private sale vs. selling through an agent</h4>
				<table class="table">
					<tr class="info">
						<td>Selling by</td>
						<td>Advantages</td>
						<td>Disadvantages</td>
					</tr>
					<tr>
						<td class="info" colspan="3">
							Private sale
						</td>
					</tr>
					<tr>
						<td></td>
						<td>You won't need to pay a real estate agent's fees
							you're in control of the whole process
							there are companies that can help you manage your private sale (like providing you with an advertising toolkit). These elements usually cost less than using a real estate agent
						</td>
						<td>You'll need to get your home valued and set an asking price or tender date yourself
							doing it yourself doesn't mean it's ‘free' – there are still costs like your lawyer's fees and potentially advertising costs
							you'll need to wear many hats to manage everything from advertising to being a salesperson, and be available for property viewings whenever needed
						</td>
					</tr>
					<tr>
						<td class="info" colspan="3">
							Real estate agent
						</td>
					</tr>
					<tr>
						<td>
							Can be done through a sole or general agency. Fees vary between commissions, commissions and a base rate, or fixed fees.
						</td>
						<td>
							Your real estate agent/s will take care of organising advertising, promotion and open home days
							the agent will deal with potential buyers' offers, and showing them the property
						</td>
						<td>
							Fees vary a lot between agencies, but could include a base fee of around $500, and a commission starting at 4% of the sale price
						</td>
					</tr>
				</table>




			<h3 id="FinancialConsiderationsWhenYouSellYourHome">Financial considerations when you sell your home</h3>
				<p>
					Considering the financial aspects of buying your next home is about more than comparing your sale price with the cost of your new home. Take a holistic approach and factor in other moving costs and fees, as well as checking that your home loan can adapt to the change.
				</p>
				<h4>What's your home worth?</h4>
					<p>
						Go to a number of different sources to get a well-rounded idea of what your home could sell for – here are a few ideas of where to start.
						<br><b>Find out your rateable value:</b><br>
						The rateable value (RV) provided by your local authority is used to work out your rates, but may not necessarily be a good guide to what your home might sell for. RV doesn’t include chattels, such as carpet, drapes, light fittings, appliances and built in items that can add to the value of your home.
						<br><b>Talk to your real estate agent:</b><br>
						Most agents are happy to do a free appraisal. While an experienced agent will have a good idea of the current market, it's still only their opinion and different agents could have quite different views. Some may tend to give a higher possible sale price to encourage you to sign up with them, while others may give a lower price because it makes the home easier to sell. So you may want to get a second (or third) opinion.
						<br><b>Look at other recent house sales:</b><br>
						Try to find out what other homes in the area have sold for in recent months. You can buy this information from <a href="#">QV (Quotable Value)</a>.
						Your real estate agent can provide similar information from the <a href="#">REINZ (Real Estate Institute)</a> database. And you could ask the agent to show you similar homes for sale in your area, so you get a feel for the ‘competition’.
						<br><b>Registered valuation:</b><br>
						Getting a valuation from a registered valuer may cost $500-$800, but it can be a good guide to how much your home is likely to sell for. 
					</p>
				<h4>What happens to your Choices Home Loan when you move?</h4>
					<p>
						Whatever your next move is, there's a way we can help when it comes to your home loan. Here's a look at some scenarios that we can give you information on – just get in touch and we'll talk it through with you.
						<br><b>Take your home loan with you</b><br>
						When you buy a new home you can usually take your Westpac Choices home loan with you, keeping the same rate and term without paying any extra loan fees. You might also be able to use Choices for an investment home.
						It's a good idea to come and have a chat with us as soon as you decide you might want to sell and move, so we can tell you what you need to do and how much extra you might be able to borrow.
						<br><a href="#">Have a play with our online calculators</a><br>
						<br><b>When there's a gap between when you sell and buy</b><br>
						Whether you're going to buy before you sell and may need bridging finance, or want to sell first before buying again, we can give you the right advice. Just give our <a href="#">Mobile Mortgage Managers</a> a call or visit your <a href="#">nearest branch</a>, or read more about these strategies on the <a href="#">Getting started</a> page.
						<br><b>Not planning to buy again?</b><br>
						If you're not buying another home, your existing home loan will need to be repaid with the money you get when your home sells. If you have a fixed interest rate there may be a break cost to pay off the loan early.
						We'll work with your lawyer to arrange the repayment of your loan and the discharge of your mortgage. And of course we can recommend a suitable investment for the money you get from the sale, so that it continues to work for you.
					</p>

				<h4>What will it cost to move?</h4>
					<p>
						Usually the main costs you’ll have when you sell are the real estate fees and your moving costs. But you also need to be prepared to pay some one-off expenses for your new home, for instance if you need to make repairs or buy new appliances and furniture.
						<br><b>Real estate fees</b><br>
						The seller usually pays the real estate agent when the house sale becomes unconditional. Fees and costs can vary quite a bit from company to company – some companies charge a base fee plus a commission based on how much your home sells for. Others charge just a commission, and some charge a fixed fee no matter what your home sells for.
						Base fees are usually around $500 plus GST. Commissions usually start at about 3.75-4% (plus GST) of the sale price, but may work out less for more expensive homes – or you may be able to negotiate a fixed fee.
						You may also have other costs such as advertising (which you pay even if the home doesn’t sell).
						You may be able to get a better deal, especially if you sign up with just one agency, so be prepared to talk with several agents and negotiate the fee.
						<br><b>Moving costs and insurance</b><br>
						Moving costs vary considerably. You could expect to pay up to $3,000 for professional movers to help you move within the same town or city. It also depends on how much of the packing you’ll do yourself, and you may need to consider putting things into storage too.
						Most contents insurance policies don’t automatically cover your belongings during a move, so you’ll probably need to ask your insurer to give you extra cover for the day. Or the moving company may be able to provide the cover. If you’re planning to do any of the packing or moving yourself, it would pay to check what the insurance will cover.
						<br><b>Fees from your lawyer</b><br>
						You could pay around:<br>
						<ul>
							<li>
								$600-$1,200 for legal fees
							</li>
							<li>
								$100-$200 to discharge the mortgage.
							</li>
						</ul>
						So if you’re buying and selling at the same time you can expect to pay around:<br>
						<ul>
							<li>
								$1,300-$3,000 for legal fees, plus
							</li>
							<li>
								$300-$500 for other costs.
							</li>
						</ul>
					</p>
		</div>

	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
