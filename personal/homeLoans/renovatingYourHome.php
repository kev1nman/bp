<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">

		<div class="col-md-9">
			<h1>Renovating Your Home</h1>
			<p>
				There's no doubt about it, doing renovations on your home or even building a whole new home from scratch is an exciting project - but it pays to get organized early. We'll help to make sure you understand the process, including home loans, construction loans, and other financing for building.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="sect col-md-3">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img width="300" class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" />
				</div>
				<h3>Getting started</h3>
				<legend></legend>
				<p>
					What are the advantages of building and renovating, and what do you need to watch out for? </p><a class="btn btn-info" href="#GettingStarted">More info…</a>
			</div>
		</div>
		<div class="sect col-md-3">
			<div class="section2 altoCuadrosSavings">
				<div class="imgSections">
					<img width="300" class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/investment.jpg' alt="" />
				</div>
				<h3>Financing the project</h3>
				<legend></legend>
				<p>
					A look at construction loans, and options for financing your renovation – plus some of the costs that come hand in hand with building</p> <a class="btn btn-info" href="#FinancingTheProject">More info…</a>
			</div>
		</div>
		<div class="sect col-md-3">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img width="300" class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/savingsPlus.jpg' alt="" />
				</div>
				<h3>Finance your renovations</h3>
				<legend></legend>
				<p>
					When it comes to renovating you have several options, depending on the size of your project</p> <a class="btn btn-info" href="#FinanceYourRenovations">More info…</a>
			</div>
		</div>
		<div class="sect col-md-3">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img width="300" class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" />
				</div>
				<h3>Get covered</h3>
				<legend></legend>
				<p>
					Are you sure you’re covered during your build? Take a look at insurance, legal and record-keeping considerations.</p><a class="btn btn-info" href="#GetCovered">More info…</a>
			</div>
		</div>
	</div>
	<div class="contenido col-md-12">

		<!-- MENU VERTICAL -->
		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1>
			</div>

			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							Building or renovating</a>
						</h4>
					</div>

					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<ul>
								<li><a href="#BuildingOrRenovating">Building or renovating</a>
									<ul>
										<li><a href="#GettingStarted">Getting started</a></li>
										<li><a href="#FinancingTheProject">Financing the project</a></li>
										<li><a href="#FinanceYourRenovations">Finance your renovations</a></li>
										<li><a href="#GetCovered">Get covered</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
			<!--Fast saver account-------------------------------------->
			<h2 id="BuildingOrRenovating">Building or renovating</h2>
				<p>
					There's no doubt about it, doing renovations on your home or even building a whole new home from scratch is an exciting project - but it pays to get organized early. We'll help to make sure you understand the process, including home loans, construction loans, and other financing for building.
				</p>
			<h3 id="GettingStarted">Getting started</h3>
				<p>
					There's no one place to begin when you're renovating or building - but we reckon having a well-rounded idea of what the pros and cons could be is a good place to start.
				</p>
				<h4>The good and the not-so-good about building a new home</h4>
					<p>
						<h5>Advantages</h5>
								you can create the home you want on a section you like
								everything will be new
								you shouldn’t have to worry about maintenance for some years
								new houses are usually a lot more energy efficient.
						<h5>Things to consider</h5>
								it’s a big job and will require your time even if someone else is managing the project for you – as well as time to complete the build
								what the resale value would be after the building work is complete (consider building in a manner that’s in keeping with the area)
								if there are no neighbours yet, could they take your view, sun or privacy when they build?
						<h5>If you’re renovating, does it add value?</h5>
						When you’re renovating, consider first whether the alterations will add real value to the home or not, for when you might sell the home down the track. Here’s some help.
						<h5>Some renovations that can add value:</h5>
								redecorating that makes a home feel lighter, more spacious and cleaner
								work that cuts down on frequent maintenance
								improving kitchens and bathrooms
								extra living space and indoor-outdoor flow
								easy-care, attractive gardens
								better lighting and skylights.
						<h5>Some renovations that may not:</h5>
								renovations that are out of character with the home and neighbourhood
								anything that takes something away, such as turning 3 bedrooms into 2, or making a garage into a games room
								adding unusual features or things most people don’t want
								turning your home into the most expensive home in the neighbourhood.
					</p>


			<h3 id="FinancingTheProject">Financing the project</h3>
				<p>
					Financing your renovation or build can be achieved in a number of different ways, depending on your current financial situation and the size of your project. So let’s take a look at how a construction loan works, consider how much you could borrow and all those other costs you may not have thought of yet.
				</p>
				<p>
					<h4>Construction loan or home loan?</h4>
						A construction loan is a common way to finance the build of your new home, or for major renovations. But if you already own a home and have plenty of equity, you may find that a regular <a href="#">home loan</a> suits you better.
						To find out what loan type would suit your needs, contact one of our Mobile Mortgage Managers to talk through your position.
						<h5>Benefits of a construction loan</h5>
						<ul>
							<li>12 months conditional approval, so you have plenty of time to find the right section and plan your build</li>
							<li>Interest-only during the construction period, to help reduce your outgoings during the build</li>
							<li>Up to 12 months repayment holiday* to help manage your cash flow during the build – especially handy if you’re trying to build and pay rent or an existing mortgage at the same time</li>
							<li>No annual account fees for two years on any new credit card with hotpoints® to help with those extra purchase</li>
							<li>As little as 10% deposit is required for fully managed turn key new builds**.</li>
						</ul>
						See how much you could borrow with a construction loan on the ‘How much can I borrow?’ tab.
						<h5>Types of construction loans</h5>
						<h5>Turn key</h5>
						This is a single fixed-price contract with the builder that specifies a completed property or renovation. The property is ready to live in, including:<br>
						<ul>
							<li>Major landscaping (fences, retaining walls)</li>
							<li>Drives and pathways</li>
							<li>Decorating (painting, floor coverings).</li>
						</ul>
						There is no more money to spend, no more work to do. <br>
						Those contracts may need to include a Master Builders Guarantee or a Certified Builders Guarantee.<br>
						For a new build, the contract sometimes includes the land purchase.<br>
						The minimum deposit required for turn key contract is usually 10%.<br>
						<h5>Build only</h5>
						This is a single fixed-price contract with a builder that specifies a completed property or renovation to a completely liveable and compliant condition.
						The owner is responsible for completing some finishing work themselves or may have other contracts in place.
						The minimum deposit required for build only contract is usually 20%.
						<h5>Partial contract</h5>
						A partial contract involves a range of sub-contracts managed by the customer or a project manager, and/or a labour only arrangement with contractor(s). This type typically includes relocated and kitset homes. In the case of kitset and relocated homes, the lending is generally limited to the land value only, until the buildings are permanently attached.
						The minimum deposit required for partial contract is usually 35%.
						<h5>Here’s how a construction loan works</h5>
							1	First you'll need to get a valuation done to estimate how much the home will be worth when it’s completed.
							2	We'll review the valuation and let you know how much you can borrow, and once you’ve set up a contract with your builder, will make each payment directly (with your deposit being used first).
							3	As the building begins, the construction loan is paid in agreed stages. Your contract with the builder may set out how much is paid at each stage, and the building will need to be inspected and certified at each stage to say the work has been done (and therefore has a certain value at that stage). If you’re borrowing quite a lot of money you may even need to get interim valuations done by a registered valuer.
							4	During the project you only pay interest on the money already drawn down, and you don’t start repaying the loan itself until the project is finished. A construction loan is usually on a floating interest rate.
						<h5>Talk to us early on before you build</h5>
						If you think you’ll need to borrow money, come and talk to us early on so we can let you know how much you might be able to borrow, and the best way to go about it.
						<a href="#">Talk to a Mobile Mortgage Manager</a>
				</p>


			<h3 id="FinanceYourRenovations">Finance your renovations</h3>
				<p>
					When it comes to renovating you have several options, depending on the size of your project. You could:<br>
						<h5>1. Apply for a home loan – for medium to large projects</h5>
						A home loan can be set to the 'interest only' repayment option, to help keep costs down while the work is going on. You can change the repayment option once the project is completed.
						<h5>2. Use your Westpac Choices Home Loan buffer or get a top up – for small to medium projects</h5>
						If you have a Westpac Choices Floating or Offset home loan and are under your loan limit (because you've been paying more than the minimum repayment amount), you can draw the extra money out any time without having to reapply. If you have a Choices Everyday home loan, you can use this facility at any time up to the agreed limit. Or if you need more and meet our lending criteria, you can ask us for a top up - and if approved - we can usually arrange it on the spot.
						Read more about topping up and redrawing
						<h5>3. Use a credit card for small projects, or buying materials and appliances</h5>
						You could get the limit on your card reviewed or apply for a new one just for your project, so you can pay suppliers, buy materials on sale and get up to 55 days free credit.<br><br>
						The amount you can borrow for your build or renovation depends on the current or projected value of your home, your project and your ability to repay the money – factoring in your income and the repayments.<br>
						To get an exact answer, talk to one of our <a href="#">Mobile Mortgage Managers</a> or visit your <a href="#">nearest branch</a>. In the meantime here are some general guidelines for how much you may be able to borrow:<br>
							<ul>
								<li>If you’re topping up your loan for some simple renovations then – up to 80% of your home’s value    </li>
								<li>For major building work – up to 90% of your home's projected value for fully managed turn key contracts, up to 80% for a partial or build only contract, or up to 65% for labour-only contracts</li>
								<li>If you’re buying a residential section – up to 80% of the land value and up to 90% of the projected completed value when you build on it if you use a fully managed turn key contract.</li>
							</ul>
						Depending on the amount you want to borrow, you may need to get valuations at different stages of the project. And a caution: cost overruns are common during building work, so keep track of your budget as the project goes on. That way you can make adjustments as you go rather than find out later you can’t afford to finish!
						<a href="#">Learn about types of construction loans</a>
						<h5>What do I need when I apply to finance my build or renovation?</h5>
						This varies depending on the finance option you choose, but details about your income, assets and outgoings are essential.
						For larger projects and construction loans you could also be asked for your building contract, a valuation report, and the sale and purchase agreement for your section or home (if this applies).
				</p>


			<h3 id="GetCovered">Get covered</h3>

			<h4>Cost guides for the building process</h4>
				From architects to builders, valuers’ reports to consents, here are some handy tips and a rough guide to how much you might expect to pay for elements of the building process.
			<h4>For PIM and engineer’s reports</h4>
				It's a good idea to get a PIM (Project Information Memorandum) report about your piece of land before you buy, which you can get from your local authority, and if you have any questions about the suitability of the site, get an engineer’s report too.
				<h5>Cost guide:</h5>
				<ul>
					<li>a PIM could cost you $150-$1,000, or more, depending on the scale of your project</li>
					<li>an engineer's report could cost $1,500-$4,000 or more.</li>
				</ul>
				<h5>Handy tip: </h5> 
				To get a PIM you may need a design sketch, but a designer may want to see the PIM first – which is confusing! So be prepared that it could work either way around.
			<h4>For design and documentation</h4>
				If you want something designed especially for you, you'll need to employ an architect, designer or draughtsperson. Some builders do supply design services, and some companies include the design as part of a package.
				<h5>Cost guide:</h5>
				For a new home, the design and documentation ranges from around 6-15% of the building cost.
				<h5>Handy tip:  </h5>
				Make sure you get a written estimate or quote, including GST and any set costs, and ask about prices if you want to make changes.
			<h4>For a builder</h4>
				If you're using an architect they can manage this process for you. If you're finding your own builder, ask people you know for recommendations and get several quotes.
				<h5>Cost guide:</h5>
				Building work costs vary from region to region and depending on the nature of the job. It’s important to include a contingency in your budget of approximately 20%, for the unexpected such as council amends, changes to plans and other unforeseen circumstances.
				<h5>Handy tip:  </h5>
				If you use a registered master builder or a Licensed Building Practitioner you should be able to expect quality work that carries a guarantee on materials and labour. Licensed building practitioners (LBPs) have undergone an application process that includes a written application, verbal testing by proficient assessors, and referees’ confirmation of their work. They keep up with the changing industry and undergo continuous skills maintenance.
				Find out more on who LBPs are and when you need to use one here.
				 
			<h4>For a valuation</h4>
				A registered valuer can give you a valuation based on your plans. You'll need this for your loan, but it can also help ensure you don't pay too much, or spend too much on renovating an older home.
				Another professional who can help you check that you're paying the right price for your build is a registered quantity surveyor (some suppliers also do this job for free). They work out the quantities of materials required for a job.
			<h4>For plans and consents</h4>
				Depending on the nature of your project, you may need to get resource or building consents from your local authority, based off your plans. Your architect or builder may arrange these as part of their contract with you.
				If your project requires building consent and relates to the primary structure of your home, or affects its weathertightness, it's considered ‘restricted building work’ and must be done by or under the supervision of a Licensed Building Practitioner (LBP). You’ll need to include the name of your LBP in your building consent application to the council.
				<a href="#">More information on when an LBP is required</a>
				 
				<h5>Cost guide:</h5>  
				Consents can cost anywhere from between several hundred, to several thousand dollars.
				<h5>Handy tip:</h5>
				Getting consents can take a lot of time, and can be costly if you have to alter and resubmit your plans several times. So be prepared, and have patience.
		</div>

	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
