<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Home loans</h1>
			<p>
				We know you'd rather talk about getting into your new home than your mortgage. So we're making it easier for you to make the right decisions, and help you get in there quicker. Whatever your needs, we can help.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/personal/homeLoans/buyingYourFirstHome.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" />
					</a>
				</div>
				<h3>Buying your first home</h3>
				<legend></legend>
				<p>
					Starting with the basics for first home buyers, we’ll talk you through home loans and every step of the process, so you can move forward with confidence.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/homeLoans/buyingYourFirstHome.php">More info</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/personal/homeLoans/buyingYourNextHome.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" />
					</a>
				</div>
				<h3>Buying your next home</h3>
				<legend></legend>
				<p>
					Buying your next home often means selling your existing one – which means there’s a lot to consider. Let’s take a look at the process of buying and selling, and how it could work with your home loan.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/homeLoans/buyingYourNextHome.php">More info</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/personal/homeLoans/renovatingYourHome.php">
						<img width="300" class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/investment.jpg" alt="" />
					</a>
				</div>
				<h3>Building or renovating</h3>
				<legend></legend>
				<p>
					There's no doubt about it, doing renovations on your home or even building a whole new home from scratch is an exciting project - but it pays to get organized early. We'll help to make sure you understand the process, including home loans, construction loans, and other financing for building.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/homeLoans/renovatingYourHome.php">More info</a>
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
