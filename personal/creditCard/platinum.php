<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Credit Card | <b><i>Platinum</i></b></h1>
			<p>
				Platinum Credit Card is a premium international payment card which will give you top-class privileges and security while traveling, as well as being a great companion in your everyday life.
			</p>
			<ul>
				<li><b>Travel insurance</b> for the entire family</li>
				<li><b>Priority Pass</b> card</li>
				<li><b>Purchase insurance</b></li>
				<li><b>Interest-free period</b> of up to 50 days</li>
			</ul>
			<p>
				Borrow responsibly by fairly assessing your loan repayment capabilities.
			</p>
		</div>
		<div class="imgCreditCard col-md-3">
			<img class="creditCard" src="<?php host();?>/rs/img/PLATINUMCardMastercard.png" alt="" />
		</div>
		<div class="contenido col-md-12">
			<div class="col-md-6">
				<h3>Credit card opportunities</h3>
				<ul>
					<li>Business-class lounges at over 600 airports worldwide.</li>
					<li>Comprehensive <u>travel insurance</u> for the whole family which also provides cover for your personal belongings, trip interruption and insurance for rental cars.</li>
					<li>6-month <u>purchase insurance</u> against damage and theft.</li>
					<li><a href="#">The privileges of Premium</a> cardholders in exclusive hotels worldwide.</li>
					<li><a href="#">Great deals</a> for BP bank Platinum credit card holders in the world.</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3>Credit card requirements</h3>
				<ul>
					<li>Regular income of at least <b>USD</b> 670 per month after taxes.</li>
					<li>The minimum age of 18.</li>
					<li>A positive credit record if loans have been taken previously.</li>
					<li>The total amount of credit payments does not exceed 30% of the monthly income.</li>
					<li>If at the moment of receiving the credit card there is no income into an account with BP bank, it should be provided within 3 months.</li>
				</ul>
			</div>
			<div class="col-md-12">
				Insurance will help discharging credit card liabilities in case you suddenly lose your job or fall ill. You will be charged for insurance only if the credit card limit is used.
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
