<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>

	<div class="container">
		<div class="col-md-9">
			<h1>Credit Card | <b><i>Gold</i></b></h1>
			<p>
				The Gold Revolving Credit Card allows combining personal growth with relaxing and enjoying life to the fullest.Gold Revolving Credit Card gives access to a loan with any strict repayment deadline. Interest-free period during which you don't pay interest on purchases made before 10th day of the next month.
			</p>
			<ul>
				<li>Travel insurance for the entire family</li>
				<li>Interest-free period</li>
				<li>Purchase insurance</li>
			</ul>
			<p>
				Borrow responsibly by fairly assessing your loan repayment capabilities.
			</p>
		</div>
		<div class="imgCreditCard col-md-3">
			<img class="creditCard" src='<?php host();?>/rs/img/GOLDCardMastercard.png' alt="" />
		</div>
		<div class="contenido col-md-12">
			<div class="col-md-6">
				<h3>Credit card opportunities</h3>
				<ul>
					<li><b>Interest-free period</b> - no interest will be charged on purchases made till the 10th day of the following month.</li>
					<li><b>Travel insurance</b> will let you and your family feel secure and enjoy the travel abroad.</li>
					<li>All purchases made with the credit card are insured against damages and theft for 6 months from the purchase day.</li>
					<li>If there is a need – <b>do transactions</b> or <b>payments</b> directly from credit card account.</li>
					<li>If the card is lost abroad, cash can be received through Western Union.</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3>Credit card requirements</h3>
				<ul>
					<li>Regular income of at least <b>USD</b> 252 per month after taxes.</li>
					<li>The minimum age of 18.</li>
					<li>A positive credit record if loans have been taken previously.</li>
					<li>The total amount of credit payments does not exceed 30% of the monthly income.</li>
					<li>If at the moment of receiving the credit card there is no income into an account with BP bank, it should be provided within 3 months.</li>
				</ul>
			</div>
			<div class="col-md-12">
				Insurance will help discharging credit card liabilities in case you suddenly lose your job or fall ill. You will be charged for insurance only if the credit card limit is used.
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
