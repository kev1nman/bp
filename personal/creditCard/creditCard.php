<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Credit cards</h1>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		</div>
		<div class="imgCreditCard col-md-3">
			<img class="creditCard" src="<?php host();?>/rs/img/BLACKCardMastercard.png" alt="" />
		</div>
		<div class="contenido col-md-12">
			<h3>Credit Cards compared</h3>
			<div class="infoContenido col-md-12">
				<div class="table-responsive">
					<table class="table tableCreditCard table-bordered">
						<thead>
							<tr class="warning">
								<th><h4>Credit card</h4></th>
								<th class="columnaRate">
									<img class="imgCreditCardTable" src="<?php host();?>/rs/img/CLASSICCardMasterCard.png" alt="" />
									Classic <br> <a href="<?php host();?>/personal/creditCard/classic.php">Find out more</a>
								</th>
								<th class="columnaRate">
									<img class="imgCreditCardTable" src="<?php host();?>/rs/img/GOLDCardMastercard.png" alt="" />
									Gold <br> <a href="<?php host();?>/personal/creditCard/gold.php">Find out more</a>
								</th>
								<th class="columnaRate">
									<img class="imgCreditCardTable" src="<?php host();?>/rs/img/PLATINUMCardMastercard.png" alt="" />
									Platinum <br> <a href="<?php host();?>/personal/creditCard/platinum.php">Find out more</a>
								</th>
								<th class="columnaRate">
									<img class="imgCreditCardTable" src="<?php host();?>/rs/img/BLACKCardMastercard.png" alt="" />
									Black <br> <a href="<?php host();?>/personal/creditCard/black.php">Find out more</a>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="colTittle"><h4>Support for traveling</h4></td>
							</tr>
							<tr>
								<td>Travel insurance you and your family</td>
								<td></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Medical assistance</td>
								<td></td>
								<td>500 000 <b>USD</b></td>
								<td>500 000 <b>USD</b></span></td>
								<td>500 000 <b>USD</b></span></td>
							</tr>
							<tr>
								<td>Personal belongings</td>
								<td></td>
								<td>1000 <b>USD</b><br>(deductible 30 <b>USD</b>)</td>
								<td>1000 <b>USD</b><br>(deductible 30 <b>USD</b>)</td>
								<td>2500 <b>USD</b></td>
							</tr>
							<tr>
								<td>Travel or transport disruption</td>
								<td></td>
								<td>2000 <b>USD</b><br>(deductible 30 <b>USD</b>)</td>
								<td>2000 <b>USD</b><br>(deductible 30 <b>USD</b>)</td>
								<td>3500 <b>USD</b></td>
							</tr>
							<tr>
								<td>Rental car CASCO</td>
								<td></td>
								<td></td>
								<td></td>
								<td>40 000 <b>USD</b><br>(deductible 250 <b>USD</b>)</td>
							</tr>
							<tr>
								<td>Purchase insurance</td>
								<td></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Priority Pass</td>
								<td></td>
								<td></td>
								<td></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Visa Platinum credit card privileges</td>
								<td></td>
								<td></td>
								<td></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Possibility to book hotel and rent a car</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Option to deposit and use your own funds</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Access to money in case of losing the card abroad</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<tr>
								<td colspan="5" class="colTittle"><h4>Extra funds</h4></td>
							</tr>
							<tr>
								<td>Credit limit of up to the equivalent of 3 salaries</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Interest-free period for card purchases (days);does not apply to cash withdrawal in ATMs or transfers from credit card account</td>
								<td></td>
								<td>Until 30th date of next month</td>
								<td>Until 10th date of next month</td>
								<td>Until 20th date of next month</td>
							</tr>
							<tr>
								<td>Repayment</td>
								<td>Fixed</td>
								<td>Flexible</td>
								<td>Flexible</td>
								<td>Deferred / Flexible</td>
							</tr>
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<tr>
								<td colspan="5" class="colTittle"><h4>Cards Features</h4></td>
							</tr>
							<tr>
								<td>Payment for purchases worldwide</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Opportunity to transfer money from credit card account</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Access to cash worldwide</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Discounts and offers</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<tr>
								<td>Option to receive the credit card by post,deposit and use own funds</td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
								<td class="success"><span class="icon-checkmark"></span></td>
							</tr>
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<!--/////////////////////////////////////////////////////////////////////////////////////////-->
							<tr>
								<td colspan="5" class="colTittle"><h4>Price list and terms</h4></td>
							</tr>
							<tr>
								<td>Credit limit of up to the equivalentof 3 salaries Minimum income</td>
								<td>252 <b>USD</b></td>
								<td>252 <b>USD</b></td>
								<td>252 <b>USD</b></td>
								<td>670 <b>USD</b></td>
							</tr>
							<tr>
								<td>Issuance fee(if applied for via Internet Banking)</td>
								<td>2,13 <b>USD</b></td>
								<td>2,13 <b>USD</b></td>
								<td>2,85 <b>USD</b></td>
								<td>7,11 <b>USD</b></td>
							</tr>
							<tr>
								<td>Monthly card fee</td>
								<td>0 <b>USD</b></td>
								<td>1,42 <b>USD</b></td>
								<td>3,91 <b>USD</b></td>
								<td>16,67 <b>USD</b></td>
							</tr>
							<tr>
								<td>Minimum limit</td>
								<td>300 <b>USD</b></td>
								<td>300 <b>USD</b></td>
								<td>600 <b>USD</b></td>
								<td>2000 <b>USD</b></td>
							</tr>
							<tr>
								<td>Maximum limit</td>
								<td>3000 <b>USD</b></td>
								<td>3000 <b>USD</b></td>
								<td>12000 <b>USD</b></td>
								<td>12000 <b>USD</b></td>
							</tr>
						</tbody>
					</table>
				</div>
				<ul>
					<li>Full travel insurance risk cover <a href="#">here</a>.</li>
					<li>In order for travel insurance to be in force for the Gold Revolving Credit Card holders and their family, the expenses of accommodation or buying the trip or transport tickets or fuel must be paid for:
						<ul>
							<li>With the Gold Revolving Credit Card</li>
							<li>By a money transfer from BP bank account</li>
							<li>With debit cards linked to BP bank accounts</li>
						</ul>
					</li>
				</ul>
				<p>
					<b>Travel costs:</b> accommodation, trip purchase, tickets, fuelBorrow responsibly by fairly assessing your loan repayment capabilities!.
				</p>
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
