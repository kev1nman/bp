<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Credit Card | <b><i>Classic</i></b></h1>
			<p>
				BP Credit Card will be a great companion in your day-to-day life as the interest-free period and discounts will let you save money, while purchase insurance will help in unexpected situations. <br><br>The amount spent can be paid back whenever you wish - until that time you will only have to pay interest on a date of your choice.
			</p>
			<ul>
				<li>Interest-free period</li>
				<li>Purchase insurance</li>
			</ul>
			<p>
				Borrow responsibly by fairly assessing your loan repayment capabilities.
			</p>
		</div>
		<div class="imgCreditCard col-md-3">
			<img class="creditCard" src='<?php host();?>/rs/img/CLASSICCardMasterCard.png' alt="" />
		</div>
		<div class="contenido col-md-12">
			<div class="col-md-6">
				<h3>Credit card opportunities</h3>
				<ul>
					<li><b>Interest-free period</b> - if the spent amount is repaid till the end of month, no interest has to be paid.</li>
					<li>Payments at points of sale and on the Internet throughout the world <b>without a fee</b>.</li>
					<li>All purchases made with the credit card are insured against damages and theft for 6 months from the purchase day.</li>
					<li>If there is a need – <b>do transactions</b> or <b>payments</b> directly from credit card account.</li>
					<li>If the card is lost abroad, cash can be received through Western Union.</li>
				</ul>
			</div>
			<div class="col-md-6">
				<h3>Credit card requirements</h3>
				<ul>
					<li>Regular income of at least <b>USD</b> 252 per month after taxes.</li>
					<li>The minimum age of 18.</li>
					<li>A positive credit record if loans have been taken previously.</li>
					<li>The total amount of credit payments does not exceed 30% of the monthly income.</li>
					<li>If at the moment of receiving the credit card there is no income into an account with BP bank, it should be provided within 3 months.</li>
				</ul>
			</div>
			<div class="col-md-12">
				Insurance will help discharging credit card liabilities in case you suddenly lose your job or fall ill. You will be charged for insurance only if the credit card limit is used.
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
