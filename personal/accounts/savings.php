<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Savings account</h1>
			<p>
				Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
			</p>
		</div>
	</div>

	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
		<h3>Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.</h3>
	</div>
	<div class="sections col-md-12">
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" />
				</div>
				<h3>Fast Savings</h3>
				<legend></legend>
				<h4><b>Interest rate</b> - 0.75% p.a.</h4>
				<p>
					Earn bonus interest if your balance increases each month (on top of any interest earned) and you make no more than one withdrawal per month.  Your bonus interest is not affected by any Rapid Save fees.
				</p>
				<a class="btn btn-info" href="#fastSaverAccount">More about fast savings</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section2 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" />
				</div>
				<h3>Saver On Call</h3>
				<legend></legend>
				<h4><b>Interest rate</b> - 2.75% p.a. Total rate (0.10% base + 2.65% bonus)</h4>
				<p>
					Earn competitive interest on every dollar in your account and be able to access your funds at any time without affecting your interest rate. A good option if you need to dip into your savings from time to time.
				</p>
				<a class="btn btn-info" href="#saverOnCall">More about  saver on call</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/savingsPlus.jpg" alt="" />
				</div>
				<h3>Savings Plus</h3>
				<legend></legend>
				<h4><b>Interest rate</b> - 0.75% p.a Total rate 3.00% p.a</h4>
				<p>
					A savings account that rewards you for not making withdrawals – great for those who are saving for the long term.
				</p>
				<a href="#savingPLUS" class="btn btn-info">More about savings plus</a>
			</div>
		</div>
	</div>


	<div class="contenido col-md-12">

		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1><legend>Savings accounts</legend>
			</div>
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
								Fast saver account</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li><a href="#fastSaverAccount">Fast saver account</a>
										<ul>
											<li><a href="#benefitsAtaGlance">Benefits at a glance</a></li>
											<li><a href="#isItRightForMe">Is it right for me?</a></li>
											<li><a href="#interestRates&Fees">Interest rates & fees</a></li>
											<li><a href="#getMoreWithBP">Get more with BP</a></li>
											<li><a href="#openAnBPFastSaverAccount">Open an BP FastSaver account</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									Saver on call</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li><a href="#saverOnCall">Saver on call</a>
											<ul>
												<li><a href="#benefitsAtaGlance-2">Benefits at a glance</a></li>
												<li><a href="#isItRightForMe-2">Is it right for me?</a></li>
												<li><a href="#interestRates&Fees-2">Interest rates & fees</a></li>
												<li><a href="#getMoreWithBP-2">Get more with BP</a></li>
												<li><a href="#openAnBPFastSaverAccount-2">Open an BP FastSaver account</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
										Saving PLUS</a>
									</h4>
								</div>
								<div id="collapse3" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#savingPLUS">Saving PLUS</a>
												<ul>
													<li><a href="#benefitsAtaGlance-3">Benefits at a glance</a></li>
													<li><a href="#isItRightForMe-3">Is it right for me?</a></li>
													<li><a href="#interestRates&Fees-3">Interest rates & fees</a></li>
													<li><a href="#getMoreWithBP-3">Get more with BP</a></li>
													<li><a href="#openAnBPFastSaverAccount-3">Open an BP FastSaver account</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>






			<!--MENU ORIGINAL
			<div  id="ir-arriba" class="contenidoTitle col-md-offset-1 col-md-11">
				<h1>More about</h1><legend>Savings accounts</legend>
			</div>
			<div class="menuContenido col-md-offset-1 col-md-3">
				<ul>
					<li><a href="#fastSaverAccount">Fast saver account</a>
						<ul>
							<li><a href="#benefitsAtaGlance">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe">Is it right for me?</a></li>
							<li><a href="#interestRates&Fees">Interest rates & fees</a></li>
							<li><a href="#getMoreWithBP">Get more with BP</a></li>
							<li><a href="#openAnBPFastSaverAccount">Open an BP FastSaver account</a></li>
						</ul>
					</li>
					<li><a href="#saverOnCall">Saver on call</a>
						<ul>
							<li><a href="#benefitsAtaGlance-2">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe-2">Is it right for me?</a></li>
							<li><a href="#interestRates&Fees-2">Interest rates & fees</a></li>
							<li><a href="#getMoreWithBP-2">Get more with BP</a></li>
							<li><a href="#openAnBPFastSaverAccount-2">Open an BP FastSaver account</a></li>
						</ul>
					</li>
					<li><a href="#savingPLUS">Saving PLUS</a>
						<ul>
							<li><a href="#benefitsAtaGlance-3">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe-3">Is it right for me?</a></li>
							<li><a href="#interestRates&Fees-3">Interest rates & fees</a></li>
							<li><a href="#getMoreWithBP-3">Get more with BP</a></li>
							<li><a href="#openAnBPFastSaverAccount-3">Open an BP FastSaver account</a></li>
						</ul>
					</li>
				</ul>
			</div>-->
			<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
				<!--Fast saver account-------------------------------------->
				<h2 id="fastSaverAccount">Fast saver account</h2>
				<p>Every dollar in your account earns the same interest rate, so even a small balance is working hard for you.</p>
				<h3 id="benefitsAtaGlance">Benefits at a glance</h3>
				<p>
					Great for smaller savings balances that you might need to occasionally access.<br>
					Every dollar in your account earns the same interest rate.<br>
					Access your savings whenever you like.<br>
					No monthly base fee, or electronic transfer fees to transfer between your own BP accounts.<br>
				</p>
				<h3 id="isItRightForMe">Is it right for me?</h3>
				<p>
					FastSaver could be a great option if you have a smaller savings balance, because every dollar in your FastSaver earns the same interest rate. There’s no minimum or maximum balance required, and you can access your money whenever you like. You can move money between your BP accounts as often as you like with no fees.
				</p>
				<h3 id="interestRates&Fees">Interest rates & fees</h3>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="warning">
							<tr>
								<th>Feed</th>
								<th class="columnaRate">Rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>All balances</td>
								<td>0.75% p.a.</td>
							</tr>
							<tr>
								<td>Monthly base fee</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									<b>Transfers and deposits</b>
									Via online banking, Fastphone, ATM, TXT banking (to your own BP accounts), FastDeposit boxes or branch deposits.
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									<b>Payments and withdrawals</b>
									Bill payments, one-off payments, IRD payments and withdrawals. Also includes manual transfers via branch, contact centre, business banking and BP Virtual Branch.
								</td>
								<td>$1</td>
							</tr>
							<tr class="warning">
								<td>
									Direct debit and automatic payments are not available on FastSaver.
									Cash deposits may incur a cash handling fee.
									First chargeable <a href="#">manual</a> or <a href="#">electronic</a> transaction each month is free.
									<a href="#">Service charges apply</a>. <a href="#">See all our rates and fees</a>.</td>
									<td> </td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3 id="getMoreWithBP">Get more with BP</h3>
					<p>
						<h4>Bank when it suits you</h4>
						Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
						<h4>FastNet Classic online banking</h4>
						Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
						<h4>Bank on the go</h4>
						We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.
						<h4>Put away money as you spend</h4>
						Achieve your savings goal sooner with Save the Change. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.
						<h4>Tips to help you save</h4>
						Getting off to a good start can make all the difference when saving. Discover a world of money tips, from saving for a goal to teaching kids the difference between needs and wants.
					</p>
					<h3 id="openAnBPFastSaverAccount">Open an BP FastSaver account</h3>
					<p>
						<h4>Open online</h4>
						Open online and we’ll get you up and running as soon as possible.
						commented for change.     (BOTON) Open online
						<h4>Call us</h4>
						We’re here to take your call 24 hours a day.
						(92)80-6230
						<h4>Visit a branch</h4>
						Visit us at one of our many branches across New Zealand.
						<a href="#">Find a branch</a>.
					</p>

				</div>
				<div class="infoContenido col-md-offset-4 col-md-7">
					<!--Saver on call-------------------------------------->
					<h2 id="saverOnCall">Saver on call<a class="up" href="#ir-arriba">Up <span class="icon-arrow-up2"></span></a></h2>
					<p></p>
					<h3 id="benefitsAtaGlance-2">Benefits at a glance</h3>
					<p>
						Great if you want to transfer your savings between BP accounts
						You can access your savings whenever you like
						No monthly base fee, or electronic transfer fees to your own BP accounts
						Higher account balances earn higher interest rates
					</p>
					<h3 id="isItRightForMe-2">Is it right for me?</h3>
					<p>
						Savings On Call could be a great option if you have a larger account balance. The higher your balance, the higher your interest rate. We calculate interest daily and pay it monthly.
						Your money is not locked in so you can move it between your BP accounts as often as you like with no fees. You may have to pay a transaction fee for some withdrawals, but as long as your balance stays in the same tier, you’ll earn the same interest rate.
					</p>
					<h3 id="interestRates&Fees-2">Interest rates & fees</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Balance</th>
									<th class="columnaRate">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr class="warning">
									<td>< $10,000</td>
									<td>0.10% p.a.</td>
								</tr>
								<tr>
									<td>$10,000 - $24,999.99</td>
									<td>1.05% p.a.</td>
								</tr>
								<tr class="warning">
									<td>$25,000 - $49,999.99</td>
									<td>1.50% p.a.</td>
								</tr>
								<tr>
									<td>$50,000 - $99,999.99</td>
									<td>2.15% p.a.</td>
								</tr>
								<tr class="warning">
									<td>$100,000+</td>
									<td>2.65% p.a.</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Balance</th>
									<th class="columnaRate">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr class="warning">
									<td>Monthly base fee</td>
									<td>Free</td>
								</tr>
								<tr>
									<td>
										<b>Transfers and deposits</b>
										Via online banking, FastPhone, ATM, TXT banking (to your own BP accounts), FastDeposit boxes or branch deposits.
									</td>
									<td>Free</td>
								</tr>
								<tr class="warning">
									<td>
										<b>Payments and withdrawals</b>
										Bill payments, one-off payments, IRD payments and withdrawals.
									</td>
									<td>$1</td>
								</tr>
								<tr>
									<td>Manual transfers via branch, contact centre, business banking and ASB Virtual Branch.</td>
									<td>$5</td>
								</tr>
								<tr class="warning">
									<td>
										EFTPOS, PayHere, direct debit and automatic payments are not available on Savings On Call.
										Cash deposits may incur a cash handling fee.
										<a href="#">Service charges apply</a>.
										<a href="#">See all our rates and fees</a>.
									</td>
									<td> </td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3 id="getMoreWithBP-2">Get more with BP</h3>
					<p>
						<h4>Bank when it suits you</h4>
						Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
						<h4>FastNet Classic online banking</h4>
						Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
						<h4>Bank on the go</h4>
						We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.
						<h4>Put away money as you spend</h4>
						Achieve your savings goal sooner with <a href="#">Save the Change</a>. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.
						<h4>Tips to help you save</h4>
						Getting off to a good start can make all the difference when saving. Discover a world of <a href="#">money tips</a>, from <a href="#">saving for a goal</a> to <a href="#">teaching kids the difference between needs and wants</a>.
					</p>
					<h3 id="openAnBPFastSaverAccount-2">Open an BP FastSaver account</h3>
					<p>
						Open online
						Open online and we’ll get you up and running as soon as possible.
						commented for change.     (BOTON) Open online

						Call us
						We’re here to take your call 24 hours a day.
						(92)80-6230

						Visit a branch
						Visit us at one of our many branches across New Zealand.
						<a href="#">Find a branch</a>
					</p>
				</div>
				<div class="infoContenido col-md-offset-4 col-md-7">
					<!--Saving PLUS-------------------------------------->
					<h2 id="savingPLUS">Saving PLUS<a class="up" href="#ir-arriba">Up <span class="icon-arrow-up2"></span></a></h2>
					<p>Get rewarded for not dipping into your savings. Our Savings Plus account earns reward interest if you don’t make withdrawals.</p>
					<h3 id="benefitsAtaGlance-3">Benefits at a glance</h3>
					<p>
						No minimum balance requirements
						Reward interest rates encourage you to not make withdrawals and stay on track with your savings
						No matter how many withdrawals you make, you still earn the base interest rate
						No monthly base fee, or electronic transfer fees to transfer between your own BP accounts
					</p>
					<h3 id="isItRightForMe-3">Is it right for me?</h3>
					<p>
						Savings Plus could be a great option if you have a longer-term savings goal.
						With Savings Plus, the fewer withdrawals you make, the better off you are. Your savings earn a base rate of interest and then you have the opportunity to earn reward interest on top. So as long as you don’t make a withdrawal during a calendar quarter (e.g. from 1 January to 31 March), you earn full reward interest. Make one withdrawal and you’ll earn a partial reward interest, or make more than one withdrawal and you’ll earn base interest. A withdrawal at anytime during the calendar quarter (including the first day and last day) will impact your reward interest, so if you know you'll be dipping into your savings think about choosing another BP savings account.
						You can earn full or partial reward interest rates on amounts up to $2 million.
						Base interest and any reward interest (after tax) can be automatically transferred to another BP bank account immediately after it's paid to your Savings Plus account. The transfer of interest will not be counted as a withdrawal for reward interest purposes.
					</p>
					<h3 id="interestRates&Fees-3">Interest rates & fees</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Balance</th>
									<th class="columnaRate">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr class="warning">
									<td>
										<b>Base interest</b>
										Calculated daily and paid monthly regardless of transactional activity or balance
									</td>
									<td>0.75% p.a.</td>
								</tr>
								<tr>
									<td>
										<b>Full reward interest</b>
										Calculated daily and paid quarterly (on balances up to and including $2,000,000) if you make no withdrawals during a calendar quarter.
									</td>
									<td>2.25% p.a</td>
								</tr>
								<tr class="warning">
									<td>
										<b>Combined base and full reward interest</b>
										If you make no withdrawals during a calendar quarter.
									</td>
									<td>3.00% p.a</td>
								</tr>
								<tr>
									<td>
										<b>Partial reward interest</b>
										Calculated daily and paid quarterly (on balances up to and including $2,000,000) if you make one withdrawal during a calendar quarter.
									</td>
									<td>1.00% p.a.</td>
								</tr>
								<tr class="warning">
									<td>
										<b>Combined base and partial reward interest</b>
										If you make one withdrawal during a calendar quarter.
									</td>
									<td>1.50% p.a.</td>
								</tr>
								<tr>
									<td>
										Balances above $2 million earn base interest only
									</td>
									<td>Free</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Balance</th>
									<th class="columnaRate">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr class="warning">
									<td>Monthly base fee</td>
									<td>Free</td>
								</tr>
								<tr>
									<td>
										<b>Transfers and deposits</b>
										Via online banking, Fastphone, ATM, TXT banking (to your own BP accounts), FastDeposit boxes or branch deposits.
									</td>
									<td>Free</td>
								</tr>
								<tr class="warning">
									<td>
										<b>Payments and withdrawals</b>
										Bill payments, one-off payments, IRD payments and withdrawals. Also includes manual transfers via branch, contact centre, business banking and BP Virtual Branch.
									</td>
									<td>$1</td>
								</tr>
								<tr class="warning">
									<td>
										EFTPOS, PayHere, direct debit and automatic payments are not available on Savings Plus.
										Cash deposits may incur a cash handling fee.
										<a href="#">Service charges apply</a>.
										<a href="#">See all our rates and fees</a>.
									</td>
									<td> </td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3 id="getMoreWithBP-3">Get more with BP</h3>
					<p>
						<h4>Bank when it suits you</h4>
						Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.

						<h4>FastNet Classic online banking</h4>
						Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.

						<h4>Bank on the go</h4>
						We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.

						<h4>Put away money as you spend</h4>
						Achieve your savings goal sooner with <a href="#">Save the Change</a>. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.

						<h4>Tips to help you save</h4>
						Getting off to a good start can make all the difference when saving. Discover a world of <a href="#">money tips</a>, from <a href="#">saving for a goal</a> to <a href="#">teaching kids the difference between needs and wants</a>.
					</p>
					<h3 id="openAnBPFastSaverAccount-3">Open an BP FastSaver account</h3>
					<p>
						Open online
						Open online and we’ll get you up and running as soon as possible.
						commented for change.     (BOTON) Open online

						Call us
						We’re here to take your call 24 hours a day.
						(92)80-6230

						Visit a branch
						Visit us at one of our many branches across New Zealand.
						<a href="#">Find a branch</a>
					</p>
				</div>
			</div>
	<?php
 		pie();
 	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
