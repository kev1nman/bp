<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Everyday account</h1>
			<p>
				In economics, a country's current account is one of the two components of its balance of payments, the other being the capital account (sometimes called the financial account). The current account consists of the balance of trade, net primary income or factor income (earnings on foreign investments minus payments made to foreign investors) and net cash transfers, that have taken place over a given period of time. The current account balance is one of two major measures of a country's foreign trade (the other being the net capital outflow). A current account surplus indicates that the value of a country's net foreign assets (i.e. assets less liabilities) grew over the period in question, and a current account deficit indicates that it shrank. Both government and private payments are included in the calculation. It is called the current account because goods and services are generally consumed in the current period.		</p>
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="sect col-lg-4 col-md-4 col-sm-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" />
				</div>
				<h3>Electronic checking</h3>
				<legend></legend>
				<p>
					Streaming online is a bank account with no fees for electronic transactions. It’s great if you like to manage things on the go.
				</p>
				<a class="btn btn-info" href="#electronicChecking">More info…</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-4">
			<div class="section2 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" />
				</div>
				<h3>Fast checking account</h3>
				<legend></legend>
				<p>
					An everyday account for those who enjoy the flexibility to bank how they like – electronically or at a branch.
				</p>
				<a class="btn btn-info" href="#fastCheckingAccount">More info…</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/savingsPlus.jpg' alt="" />
				</div>
				<h3>Checking plus account</h3>
				<legend></legend>
				<p>
					Open an everyday account that pays interest on credit balances, with a simple monthly fee for unlimited transactions.
				</p>
				<a class="btn btn-info" href="#checkingPlusAccount">More info…</a>
			</div>
		</div>
	</div>
	<div class="contenido col-md-12">

		<!-- MENU VERTICAL -->
		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1><legend>Everyday accounts</legend>
			</div>
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
								Electronic checking</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
							<div class="panel-body">
								<ul>
									<li><a href="#electronicChecking">Electronic checking</a>
										<ul>
											<li><a href="#benefitsAtaGlance">Benefits at a glance</a></li>
											<li><a href="#isItRightForMe">Is it right for me?</a></li>
											<li><a href="#feedAndRates">Feed and Rates</a></li>
											<li><a href="#getMoreWithBP">Get more with BP</a></li>
											<li><a href="#openAnBPElectronicChequingAccount">Open an BP Electronic Chequing account</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									Fast checking account</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li><a href="#fastCheckingAccount">Fast checking account</a>
											<ul>
												<li><a href="#benefitsAtaGlance-2">Benefits at a glance</a></li>
												<li><a href="#isItRightForMe-2">Is it right for me?</a></li>
												<li><a href="#feedAndRates-2">Feed and Rates</a></li>
												<li><a href="#getMoreWithBP-2">Get more with BP</a></li>
												<li><a href="#openAnBPFastChequingAccount-2">Open an BP fast chequing account</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
										Checking plus account</a>
									</h4>
								</div>
								<div id="collapse3" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#checkingPlusAccount">Checking plus account</a>
												<ul>
													<li><a href="#benefitsAtaGlance-3">Benefits at a glance</a></li>
													<li><a href="#isItRightForMe-3">Is it right for me?</a></li>
													<li><a href="#feedAndRates-3">Feed and Rates</a></li>
													<li><a href="#getMoreWithBP-3">Get more with BP</a></li>
													<li><a href="#openAnBPChequingPlusAccount-3">Open an BP Chequing Plus Account</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>



	<!--
			<div id="ir-arriba" class="contenidoTitle col-md-offset-1 col-md-11">
				<h1>More about</h1><legend>Everyday accounts</legend>
			</div>
			<div class="menuContenido col-md-offset-1 col-md-3">
				<ul>
					<li><a href="#electronicChecking">Electronic checking</a>
						<ul>
							<li><a href="#benefitsAtaGlance">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe">Is it right for me?</a></li>
							<li><a href="#feedAndRates">Feed and Rates</a></li>
							<li><a href="#getMoreWithBP">Get more with BP</a></li>
							<li><a href="#openAnBPElectronicChequingAccount">Open an BP Electronic Chequing account</a></li>
						</ul>
					</li>
					<li><a href="#fastCheckingAccount">Fast checking account</a>
						<ul>
							<li><a href="#benefitsAtaGlance-2">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe-2">Is it right for me?</a></li>
							<li><a href="#feedAndRates-2">Feed and Rates</a></li>
							<li><a href="#getMoreWithBP-2">Get more with BP</a></li>
							<li><a href="#openAnBPFastChequingAccount-2">Open an BP fast chequing account</a></li>
						</ul>
					</li>
					<li><a href="#checkingPlusAccount">Checking plus account</a>
						<ul>
							<li><a href="#benefitsAtaGlance-3">Benefits at a glance</a></li>
							<li><a href="#isItRightForMe-3">Is it right for me?</a></li>
							<li><a href="#feedAndRates-3">Feed and Rates</a></li>
							<li><a href="#getMoreWithBP-3">Get more with BP</a></li>
							<li><a href="#openAnBPChequingPlusAccount-3">Open an BP Chequing Plus Account</a></li>
						</ul>
					</li>
				</ul>
			</div> -->
			<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
				<!--Fast saver account-------------------------------------->
				<h2 id="electronicChecking">Electronic checking</h2>
				<p>Electronic Chequing Account is a bank account with no fees for electronic transactions. It’s great if you like to manage things on the go.</p>
				<h3 id="benefitsAtaGlance">Benefits at a glance</h3>
				<p>
					Great if you prefer to bank online or use your mobile
					Move money between BP accounts in real-time
					No fees on electronic transactions
					No monthly base fee if you stop paper statements
				</p>
				<h3 id="isItRightForMe">Is it right for me?</h3>
				<p>
					A Electronic Chequing bank account could be great for you if you like to bank online or on your phone, because when you do, there’s no transaction fees. You only pay a transaction fee for cheques or if you ask branch staff or the contact centre to help you withdraw or deposit money.
					<br>
					Also, if you choose to stop your paper statements, we'll waive your monthly base fee. So when you save a few trees, you're also saving on fees.
				</p>
				<h3 id="feedAndRates">Feed and Rates</h3>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>All Balances</th>
								<th class="columnaRate">Interest rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>All balances</td>
								<td>N/A</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="warning">
							<tr>
								<th>Feed</th>
								<th class="columnaRate">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>
									<b>Monthly base fee</b>
									(pay no monthly base fee if you stop paper statements through FastNet Classic)
								</td>
								<td>$3.50</td>
							</tr>
							<tr>
								<td>
									<b>Transfer between your BP accounts</b>
									(via online banking, FastPhone, BP ATM and TXT banking), Fast Deposit boxes
								</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									Bill payment, one-off payment, IRD payment, ATM withdrawal
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									EFTPOS withdrawal
								</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									Automatic payment, direct debit
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									Branch deposit
								</td>
								<td>$3</td>
							</tr>
							<tr class="warning">
								<td>
									<b>Withdrawal, transfer, IRD payment, one-off payment</b>
									(via branch, contact centre, business banking centres and BP Virtual Branch)
								</td>
								<td>$3</td>
							</tr>
							<tr>
								<td>
									Cheque
								</td>
								<td>$3</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h3 id="getMoreWithBP">Get more with BP</h3>
				<p>
					<h4>Bank when it suits you</h4>
					Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
					<h4>FastNet Classic online banking</h4>
					Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
					<h4>Bank on the go</h4>
					We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.
					<h4>Put away money as you spend</h4>
					Achieve your savings goal sooner with Save the Change. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.
					<h4>Tips to help you save</h4>
					Getting off to a good start can make all the difference when saving. Discover a world of money tips, from saving for a goal to teaching kids the difference between needs and wants.
				</p>
				<h3 id="openAnBPElectronicChequingAccount">Open an BP Electronic Chequing account</h3>
				<p>
					<h4>Open online</h4>
					Open online and we’ll get you up and running as soon as possible.
					commented for change. (BOTON) Open online
					<h4>Call us</h4>
					We’re here to take your call 24 hours a day.
					(92)80-6230
					<h4>Visit a branch</h4>
					Visit us at one of our many branches across New Zealand.
					<a href="#">Find a branch</a>.
				</p>

			</div>
			<div class="infoContenido col-md-offset-4 col-md-7">
				<!--Saver on call-------------------------------------->
				<h2 id="fastCheckingAccount">Fast checking account<a class="up" href="#ir-arriba">Up <span class="icon-arrow-up2"></span></a></h2>
				<p>An Fast Chequing Account makes life simple with just two transaction fee types and no monthly base fee.</p>
				<h3 id="benefitsAtaGlance-2">Benefits at a glance</h3>
				<p>
					Great if you bank both online and in branch, and want to keep things simple
					No monthly base fee
					Two simple transaction fee types
					You could get a fee exemption each month if you’re receiving Super or a military pension, or if your total relationship with BP is worth over $200,000.
				</p>
				<h3 id="isItRightForMe-2">Is it right for me?</h3>
				<p>
					An Omni account could be a great choice if you want to bank with us anywhere, because there is just one fee type for <a href="#">electronic transactions</a> and another for <a href="#">manual transactions</a>.<br>
					It could also be a great option if you receive the New Zealand Superannuation or a war, widows or army pension. If you direct credit this into your Omni account, or your <a href="#">total relationship</a> with us is worth more then $200,000, you could get an exemption of your transactions fees of up to $20 each month.
				</p>
				<h3 id="feedAndRates-2">Interest rates & fees</h3>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>All Balances</th>
								<th class="columnaRate">Interest rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>All balances</td>
								<td>N/A</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="warning">
							<tr>
								<th>Feed</th>
								<th class="columnaRate">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>
									<b>Monthly base fee</b>
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									<b>Transfer between your BP accounts</b>
									(via online banking, FastPhone, BP ATM and TXT banking), Fast Deposit boxes
								</td>
								<td>40c</td>
							</tr>
							<tr class="warning">
								<td>
									Bill payment, one-off payment, IRD payment, ATM withdrawal
								</td>
								<td>40c</td>
							</tr>
							<tr>
								<td>
									EFTPOS withdrawal
								</td>
								<td>40c</td>
							</tr>
							<tr class="warning">
								<td>
									Automatic payment, direct debit
								</td>
								<td>40c</td>
							</tr>
							<tr>
								<td>
									Branch deposit
								</td>
								<td>80c</td>
							</tr>
							<tr class="warning">
								<td>
									<b>Withdrawal, transfer, IRD payment, one-off payment</b>
									(via branch, contact centre, business banking centres and BP Virtual Branch)
								</td>
								<td>80c</td>
							</tr>
							<tr>
								<td>
									Cheque
								</td>
								<td>80c</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h3 id="getMoreWithBP-2">Get more with BP</h3>
				<p>
					<h4>Bank when it suits you</h4>
					Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
					<h4>FastNet Classic online banking</h4>
					Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.
					<h4>Bank on the go</h4>
					We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.
					<h4>Put away money as you spend</h4>
					Achieve your savings goal sooner with <a href="#">Save the Change</a>. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.
					<h4>Tips to help you save</h4>
					Getting off to a good start can make all the difference when saving. Discover a world of <a href="#">money tips</a>, from <a href="#">saving for a goal</a> to <a href="#">teaching kids the difference between needs and wants</a>.
				</p>
				<h3 id="openAnBPFastChequingAccount-2">Open an BP fast chequing account</h3>
				<p>
					Open online
					Open online and we’ll get you up and running as soon as possible.
					commented for change.     (BOTON) Open online

					Call us
					We’re here to take your call 24 hours a day.
					(92)80-6230

					Visit a branch
					Visit us at one of our many branches across New Zealand.
					<a href="#">Find a branch</a>
				</p>
			</div>
			<div class="infoContenido col-md-offset-4 col-md-7">
				<!--Saving PLUS-------------------------------------->
				<h2 id="checkingPlusAccount">Checking plus account<a class="up" href="#ir-arriba">Up <span class="icon-arrow-up2"></span></a></h2>
				<p>Make unlimited transactions and earn interest for one flat monthly base fee.</p>
				<h3 id="benefitsAtaGlance-3">Benefits at a glance</h3>
				<p>
					Great if you want to keep your banking simple with just one account
					Unlimited transactions for one monthly base fee
					Earn interest on credit balances
					Do your banking electronically or manually – it’s up to you
				</p>
				<h3 id="isItRightForMe-3">Is it right for me?</h3>
				<p>
					An Unlimited bank account could be a great option if you want to access your money often and in any way you choose.
					You pay one flat monthly base fee, which covers as many transactions as you like including in-branch transactions. You even earn interest on credit balances, which we calculate daily and pay monthly.
				</p>
				<h3 id="feedAndRates-3">Interest rates & fees</h3>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Balance</th>
								<th class="columnaRate">Interest rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>$0 - $99,999</td>
								<td>0.25% p.a.</td>
							</tr>
							<tr>
								<td>$100,000+</td>
								<td>0.75% p.a.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="warning">
							<tr>
								<th>All Balances</th>
								<th class="columnaRate">Interest Rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td>All balances</td>
								<td>N/A</td>
							</tr>
							<tr>
								<td>Feed</td>
								<td>Amount</td>
							</tr>
							<tr class="warning">
								<td>
									<b>Monthly base fee</b>
								</td>
								<td>$12.00</td>
							</tr>
							<tr>
								<td>
									<b>Transfer between your BP accounts</b>
									(via online banking, FastPhone, BP ATM and TXT banking), Fast Deposit boxes
								</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									Bill payment, one-off payment, IRD payment, ATM withdrawal
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									EFTPOS withdrawal
								</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									Automatic payment, direct debit
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									Branch deposit
								</td>
								<td>Free</td>
							</tr>
							<tr class="warning">
								<td>
									<b>Withdrawal, transfer, IRD payment, one-off payment</b>
									(via branch, contact centre, business banking centres and BP Virtual Branch)
								</td>
								<td>Free</td>
							</tr>
							<tr>
								<td>
									Cheque
								</td>
								<td>Free</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h3 id="getMoreWithBP-3">Get more with BP</h3>
				<p>
					<h4>Bank when it suits you</h4>
					Whether you’re a night owl or an early bird, you can do your everyday banking at any time with FastNet Classic online banking. Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.

					<h4>FastNet Classic online banking</h4>
					Sign in and you can pay your bills, set up automatic payments, order foreign exchange and more. You can even apply for a home loan after hours.

					<h4>Bank on the go</h4>
					We’re there wherever you need us with the BP Movile App. Check your balance while waiting for a bus, move money between your accounts while you’re shopping, or pay a friend for lunch using just their mobile number.

					<h4>Put away money as you spend</h4>
					Achieve your savings goal sooner with <a href="#">Save the Change</a>. You can round up electronic purchases you make to the nearest dollar and the change goes into another account. You'll be surprised how quickly it can add up.

					<h4>Tips to help you save</h4>
					Getting off to a good start can make all the difference when saving. Discover a world of <a href="#">money tips</a>, from <a href="#">saving for a goal</a> to <a href="#">teaching kids the difference between needs and wants</a>.
				</p>
				<h3 id="openAnBPChequingPlusAccount-3">Open an BP Chequing Plus Account</h3>
				<p>
					Open online
					Open online and we’ll get you up and running as soon as possible.
					commented for change.     (BOTON) Open online

					Call us
					We’re here to take your call 24 hours a day.
					(92)80-6230

					Visit a branch
					Visit us at one of our many branches across New Zealand.
					<a href="#">Find a branch</a>
				</p>
			</div>
		</div>
	<?php
 		pie();
 	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
