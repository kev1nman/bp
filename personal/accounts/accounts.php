<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-12">
			<h1>Accounts</h1>
			<p>
				Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
	</div>
	<div class="sections col-md-12">
		<div class="sect col-md-4 col-md-offset-2">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/personal/accounts/everyday.php">
						<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" /></a>
				</div>
				<h3>Everyday banking</h3>
				<legend></legend>
				<p>
					Managing your money the way you want to.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/accounts/everyday.php">More info</a>
			</div>
		</div>
		<div class="sect col-md-4">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/personal/accounts/savings.php">
						<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" /></a>
				</div>
				<h3>Savings accounts</h3>
				<legend></legend>
				<p>
					We're serious about helping you reach your saving goals.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/accounts/savings.php">More info</a>
			</div>
		</div>
	</div>
	<?php
 		pie();
 	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
