<!DOCTYPE html>
<html lang="es">
<head>
	<?php
	include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">

		<div class="col-md-12">
			<h1>Share trading</h1>
			<p>
				Value for money - open a directshares account before 31 December 2016 and we'll give you your first 10 trades free to get you going with a head start
				Investment tools - extensive range of investment tools, including independent research and timely stock tips
				Help and guidance - free, detailed 'how to trade' guidance on our Investor website.
			</p>
		</div>
	</div>
	<div class="sections col-md-12">
		<div class="col-md-8">
			<h3>Directshares</h3>
			<p>
				<b>Your first $50 brokerage waived on up to 10 trades*</b>
				Available for existing BP Bank customers.<br><br>
				<b>Start now</b>
				Start with 10 free trades<br><br>
				Open a directshares account before 31 December 2016 and we'll waive the first $50 of brokerage on up to 10 trades* for 30 days
				<br><br>
				<b>Trading education</b>
				There's a whole website dedicated to helping you get familiar with directshares. So you can buy, sell and manage risk.
				<br><br>
				Enjoy great-value trading
				<br><br>
				You can trade from just $19.95 per trade*
				<br><br>
				<b>Directshares platform</b>
				Trade through the bank you trust. With our training tools and on-call support, we're always here when you need us.
			</p>
		</div>
		<div class="col-md-4">
			<div class="col-md-12">
				<a href="#Overview">Overview</a>
			</div>
			<div class="col-md-12">
				<a href="#Products">Products</a>
			</div>
			<div class="col-md-12">
				<a href="#Trading">Trading</a>
			</div>
			<div class="col-md-12">
				<a href="#Newtotrading">New to trading</a>
			</div>
			<div class="col-md-12">
				<a href="#Rates&Feeds">Rates & Feeds</a>
			</div>
		</div>
	</div>


	<div class="contenido col-md-12">

		<div class="menuVertical col-md-offset-1 col-md-3">
			<div class="contenidoTitle">
				<h1>More about</h1><legend>Share trading</legend>
			</div>
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
								Share trading</a>
						</h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<ul>
								<li><a href="#Sharetrading">Share trading</a>
									<ul>
										<li><a href="#Overview">Overview</a></li>
										<li><a href="#Products">Products</a></li>
										<li><a href="#Trading">Trading</a></li>
										<li><a href="#Newtotrading">New to trading</a></li>
										<li><a href="#Rates&Feeds">Rates & Feeds</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

			<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
				<h2 id="Sharetrading">Share trading</h2>
					<p>
						Value for money - open a directshares account before 31 December 2016 and we'll give you your first 10 trades free to get you going with a head start
						Investment tools - extensive range of investment tools, including independent research and timely stock tips
						Help and guidance - free, detailed 'how to trade' guidance on our Investor website.
					</p>
				<h3 id="Overview">Overview</h3>
					<h4>Hassle-free banking</h4>
						<p>
							If you currently bank with St.George, BankSA or BP Bank, you can use your eligible deposit account1 to settle your trades. Alternatively, you can also apply for a St.George Margin Loan to maximise your trading opportunities.
						</p>

					<h4>Value for money</h4>
						<p>
							Pay only when you trade, with low, competitive online broking fees. Access independent research and account information any time from around the globe, with your portfolio information, favourite trading tools and the latest market news all at your fingertips.
						</p>

					<h4>Extensive range of investment tools, including independent research and timely stock tips</h4>
						<p>
							Make your own investment decisions backed by all the information you need:<br><br>

							easy-to-use Education Centre for those new to buying shares
							up-to-the-minute research compiled by a range of independent research houses
							directshares Power, one of the market’s most sophisticated trading tools, ideal for experienced, active share trading.
						</p>

					<h4>Proactive tax management</h4>
						<p>
							Receive a FREE summary report on your account’s income and realised CGT in a single downloadable PDF. Proactively manage your tax throughout the year with our Plus and Premium subscription services.
						</p>
				<h3 id="Products">Products</h3>
					<h4>Shares</h4>
						<p>
							There’s an extensive selection of Australian Shares to invest in, with over 2,200 companies currently listed on the ASX.
						</p>
					<h4>Options</h4>
						<p>
							Options are a versatile product that allows you to buy protection against a market fall, earn extra income by selling an Option, or generate leveraged returns if the market increases.
						</p>
					<h4>ETFs</h4>
						<p>
							Exchange Traded Funds are just like managed funds, except they’re listed on the sharemarket. You buy and sell units in the fund itself, so it can be another easy way to get started at minimal cost.
						</p>
					<h4>IPOs</h4>
						<p>
							Through IPOs you can gain exposure to companies when they are first introduced on the market.
						</p>
					<h4>Directshares SharePacks</h4>
						<p>
							Directshares SharePacks can be an easy and affordable way to purchase a diversified portfolio of 8 shares from different industries. They are chosen by a consensus of analyst recommendations and updated weekly. You can invest from as little as $5,500.
						</p>
					<h4>Managed Funds</h4>
						<p>
							Managed funds are professionally managed collective investments which pool investors’ money. You can access a wide range of managed funds across various asset classes, including fixed interest, property, Australian shares and international shares.
						</p>
					<h4>Self Managed Super Funds (SMSF or DIY Super)</h4>
						<p>
							Benefit from greater freedom through managing your own superannuation investment. Through a SMSF, you may also benefit from a greater diversity of investments options and potential cost savings.
						</p>
					<h4>Warrants</h4>
						<p>
							A warrant is a derivative that is issued over an underlying instrument such as a share, index, currency or commodity. They can be a good way to diversify your portfolio and can provide increased returns while reducing risk.
						</p>
				<h3 id="Trading">Trading</h3>
					<h4>Why switch to directshares?</h4>
						<p>
							Use your St.George, BankSA or Bank of Melbourne account <br><br>
							If you bank with St.George, BankSA or Bank of Melbourne; you can link your eligible deposit account to settle your trades.1
						</p>

					<h4>More affordable trading</h4>
						<p>
							Trade shares from as little as $19.95*. You only pay when you trade and it costs absolutely nothing to become a directshares customer.
							<br><br>
							Tools to enhance your trading
							<br><br>
							Stock Tips and Trading Tools
							<br><br>
							Take advantage of premium, cutting-edge tools designed to help you get ahead, such as stock filters, sector analysis tools, stock valuation tools and company comparisons.
						</p>

					<h4>Professional trader platform</h4>
					<ul>
						<li>Active Traders can benefit from our professional-standard platform, directshares Power. It boasts sophisticated tools that can deliver the market to your desktop in real time</li>
						<li>Personal service</li>
						<li>Tired of waiting on the phone? We’re here to help if you ever need a real person to answer your questions. Click here for our contact details</li>
						<li>Proactive tax management</li>
						<li>We take the headache out of tax from your share trading activity, with three levels of tax support available</li>
						<li>Free independent research.</li>
					</ul>

					<h4>Directshares customers can access a range of independent research from leading experts:</h4>
					<ul>
						<li>Aspect Huntley</li>
						<li>Morningstar</li>
						<li>AAP The Ferret</li>
						<li>FatProphets</li>
						<li>The Intelligent Investor</li>
						<li>Wise-Owl</li>
						<li>It’s simple to switch to directshares</li>
					</ul>

					<h4>If you have an existing account with another online broker or a stock broker, it’s easy to transfer your securities to directshares. To get access to the full benefit of directshares trading platform:</h4>
					<ul>
						<li>Complete the online application</li>
						<li>Arrange a transfer of your Broker and/or Issuer sponsored holdings</li>
						<li>Send in any required documentation</li>
					</ul>
				<h3 id="Newtotrading">New to trading</h3>
					<h4>About online share trading</h4>
						<p>
							Buying and selling shares can be an exciting and rewarding way to build your wealth. Whether you’re building a portfolio to pay for a holiday, a deposit on a home or support you in retirement, investing with directshares can help you on your way.
							Anyone over the age of 18 can trade stocks online via directshares as long as they have $500 (plus brokerage), a computer, an Internet connection and an eligible St.George, Bank of Melbourne or BankSA bank account.1
						</p>
					<b>How will directshares help me get started?</b><br><br>
					<b>Free independent research</b><br><br>
					<p>
						Directshares provides access to a range of independent stock tips and research from leading experts:
					</p>
					<p>
						Morningstar - Aspect Huntley research and financial information on the Top 200 ASX listed companies. Over 30 years experience
					</p>
					<p>
						AAP The Ferret - Real time news and information about Australia’s Equity markets and a collection of company announcements, daily features and broker research
					</p>
					<p>
						FatProphets - Value based recommendations combining fundamental and technical analysis for the medium to long term investor
					</p>
					<p>
						The Intelligent Investor - Independent, long-term, value investing analysis on the Top 200 ASX listed companies and other selected opportunities
					</p>
					<p>
						Wise-Owl - Quantitative, Fundamental and Technical Analysis coupled with risk management techniques, to select the opportunities we call 'stocks on the move' or the 'blue chips of tomorrow'.
					</p>
					<p>
						News
					</p>

					<p>
						We provide a wide range of financial news to keep you up to date with breaking developments, company announcements, share prices and all of the latest information:
					</p>
						<ul>
							<li>ASX Company Announcements</li>
							<li>Finance News Network videos</li>
							<li>Dow Jones Australian News</li>
							<li>Dow Jones International News</li>
							<li>AAP Australian News</li>
							<li>Most commonly traded stocks Lists</li>
						</ul>

					<p>
						Check out what other directshares customers are doing. We provide a list of the most bought, sold and held stocks amongst directshares clients.
					</p>


					<p>
						<b>Education</b><br><br>
						We make it easy for you to buy shares online and offer everything for the first time investor:
					</p>
					<p>
						Getting started in the sharemarket<br>
						Understanding the sharemarket<br>
						Weekly sharemarket basics articles from the Intelligent Investor<br>
						Guides to investing from the Intelligent Investor<br>
						Fundamental Analysis tools and how-tos<br>
						ASX resources<br>
						Education on our products and tools<br>
					</p>
				<h3 id="Rates&Feeds">Rates & Feeds</h3>
					<h4>Value for money</h4>
					<p>
						Enjoy our fully-featured platform, our great range of trading tools and up-to-date independent research. All from as little as $19.95* per trade.
						<br><br>
						Remember, you can access our tools for free, just by becoming a directshares customer. You only pay when you trade.
						<br><br>
						Pricing
						<br><br>
						Our standard brokerage rates (inc GST) for trading ASX-listed securities via the internet are:
					</p>
					<table>
						<tr>
							<td>
								Trades per calendar month
							</td>
							<td>
								Up to $5,000
							</td>
							<td>
								$5,001 - $10,000
							</td>
							<td>
								$10,001 - $28,000
							</td>
							<td>
								 over $28,000
							</td>
						</tr>
						<tr>
							<td>
								1st trade
							</td>
							<td>
								$19.95
							</td>
							<td>
								$24.95
							</td>
							<td>
								$29.95
							</td>
							<td>
								 0.11%
							</td>
						</tr>
						<tr>
							<td>
								2nd & subsequent
							</td>
							<td colspan="4">
								$19.95 for trade value up to or equal to
								$18,000 or 0.11% for trade value greater than
								$18,000
							</td>
						</tr>
					</table>

					<p>
						Click the following for a complete list of our rates and fees. For information on our brokerage rates please refer to the Financial Services Guide.
					</p>
			</div>
		</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
