<!DOCTYPE html>
<html lang="es">
<head>
	<?php
	include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Superannuation</h1>
			<p>
				Superannuation refers to the arrangements people make to accumulate funds to provide them with income in retirement.
			</p><br><br>
		</div>
	</div>
	<div class="sections col-md-12">
		<div class="sect col-md-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/saverOnCall.jpg' alt="" />
				</div>
				<h3>Overview</h3>
				<legend></legend>
				<p>
					What you get. Transparent fees. $0 commissions and contribution fees1.
				</p>
				<a class="btn btn-info" href="#Overview">More info…</a>
			</div>
		</div>
		<div class="sect col-md-4">
			<div class="section2 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/savingsPlus.jpg' alt="" />
				</div>
				<h3>Feeds</h3>
				<legend></legend>
				<p>
					BT Super for Life work hard to give your super the best chance to grow.
				</p>
				<a class="btn btn-info" href="#Feeds">More info…</a>
			</div>
		</div>
		<div class="sect col-md-4">
			<div class="section1 altoCuadrosSavings">
				<div class="imgSections">
					<img class="img-responsive img-circle center-block" src='<?php host();?>/rs/img/fastSavings.jpg' alt="" />
				</div>
				<h3>Investment options</h3>
				<legend></legend>
				<p>
					1. Opt for an age-based Lifestage fund, 2. Choose your own investment mix.
				</p>
				<a class="btn btn-info" href="#InvestmentOption">More info…</a>
			</div>
		</div>
	</div>
	<!-- MENU VERTICAL -->
	<div class="menuVertical col-md-offset-1 col-md-3">
		<div class="contenidoTitle">
			<h1>More about</h1>
		</div>

		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
						Superannuation</a>
					</h4>
				</div>

				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body">
						<ul>
							<li><a href="#Superannuation">Superannuation</a>
								<ul>
									<li><a href="#Overview">Overview</a></li>
									<li><a href="#Feeds">Feeds</a></li>
									<li><a href="#InvestmentOption">Investment options</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


<div class="contenido col-md-12">



		<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
			<h2 id="Superannuation">Superannuation</h2>
			<p>
				Superannuation refers to the arrangements people make to accumulate funds to provide them with income in retirement.
			</p>
			<h2 id="Overview">Overview</h2>
				<p>
					What you get<br><br>

					Transparent fees<br><br>

					$0 commissions and contribution fees1<br><br>

					Easy access<br><br>

					View and manage your super alongside your Internet Banking<br><br>

					Investment options<br><br>

					Flexible range of investment options<br><br>

					Insurance<br><br>

					Enjoy Pre-approved life insurance cover~<br><br>

					Exclusive offers - Benefits Now<br><br>

					BT Super for Life members can access exclusive offers and deals<br><br>

					Free lost super search<br><br>

					Track down and combine all your lost super<br><br>

					Available for existing Bank of Melbourne customers registered for Internet Banking.<br><br>

					More with BT Super for Life<br><br>

					Ask BT to complete a free Super Search to help you find all your super
					Have one of BT's specialists assist in combining your super into a BT account or do it online with BT's Easy Rollover Tool
					Make the most of clever investment options that are easily understood and a breeze to implement
					Extra peace of mind with life insurance that enables you to double or triple cover within 90 days3
					Preservation age<br><br>

					Generally, access to your super benefits is dependent on two things:<br><br>

					Having retired from work
					On reaching your preservation age, which is based on your date of birth and ranges from 55 to 60 years of age.
					Exclusive offers with Benefits Now<br><br>

					As a BT Super for Life member you can treat yourself with exclusive offers and deals through the Benefits Now program.
					Half price cinema tickets, discounted holidays and offers across a range of financial products are just some of the many benefits you can enjoy

				</p>

			<h2 id="Feeds">Feeds</h2>
				<h4>Transparent fees</h4>
					<p>
						BT Super for Life work hard to give your super the best chance to grow.<br><br>

						Commissions - $0<br>
						Investment switching fees - $0<br>
						Exit fees - $0<br>
						Withdrawal fees - $0<br>
						Contribution fees - $0<br>
						What fees apply?<br><br>

						Administration and management fees - $5 per month plus 0.45% p.a. of your account balance for all other investment options (this is Nil for the Super Cash option)
						Investment fee - 0.50% p.a. of account balance for all other investment options (this is Nil for the Super Cash option)
					</p>
			<h2 id="InvestmentOption">Investment option</h2>
			<ul>
				<li>1. Opt for an age-based Lifestage fund</li>
				<p>
					We can invest your super in a Lifestage Fund based on your age. Over time the fund gradually shifts to a more conservative asset mix by shifting its weighted allocation from growth to conservative assets.
				</p>
				<li>2. Choose your own investment mix</li>
				<p>
					Or if you prefer, select your own investment mix from a range of growth, moderate, conservative and super cash investment options.
				</p>
			</ul>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
