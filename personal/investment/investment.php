<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">

		<div class="col-md-12">
			<h1>Ivestment</h1>
			<p>
				You’ve spent a career establishing yourself. You want your investments to work as hard as you have. We can help make sure your wealth is working for you.
				You may be a first time investor, or saving for your retirement. You may have a small lump sum to invest, or you could be a high net-worth individual with an existing portfolio of shares. Whatever your stage in life or financial situation, we can make a real difference in helping you achieve your financial goals.
			</p>
			<br><br>
		</div>
	</div>

	<div class="sectionsTitle col-lg-12 col-md-12 col-sm-12">
		<h3 class="text-center">Personal loan uses</h3>
		<h4 class="text-center"><em>Use this loan for almost anything</em></h4>
	</div>
	<div class="sections col-md-12">
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/personal/investment/shareTrading.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" />
					</a>
				</div>
				<h3>Share trading</h3><legend>Trade through the bank you trust</legend>
				<p>
					Value for money - open a directshares account before 31 December 2016 and we'll give you your first 10 trades free to get you going with a head start
					Investment tools - extensive range of investment tools, including independent research and timely stock tips
					Help and guidance - free, detailed 'how to trade' guidance on our Investor website.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/investment/shareTrading.php">More info</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/personal/investment/superannuation.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" />
					</a>
				</div>
				<h3>Superannuation</h3><legend>BP Bank and BT have teamed up to give you access to your bank accounts and super together online</legend>
				<p>
					Super made super easy - manage your super together with your Internet Banking
					Low fees - no commissions, withdrawal or contribution fees
					Flexible investment options - easy to choose an investment option that suits you and your lifestyle
					Pre-approved life insurance cover with no forms or medicals to complete~
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/investment/superannuation.php">More info</a>
			</div>
		</div>
		<div class="sect col-lg-4 col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/personal/investment/termInvestments.php">
						<img class="img-responsive img-circle center-block" width="300" src="<?php host();?>/rs/img/investment.jpg" alt="" />
					</a>
				</div>
				<h3>Term investment</h3><legend>Enjoy the certainty of knowing your investment return</legend>
				<p>
					Earn a competitive interest rate with no setup or monthly fees
					Flexible maturity options from 1 month to 5 years
					Interest calculated daily and can be paid at maturity, annually, half-yearly or monthly
					Access and manage your funds via Online and Mobile Banking.
				</p>
				<a class="btn btn-info" href="<?php host();?>/personal/investment/termInvestments.php">More info</a>
			</div>
		</div>
	</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
