<!DOCTYPE html>
<html lang="es">
<head>
	<?php
	include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">

		<div class="col-md-9">
			<h1>Term investments</h1>
			<p>
				There's over $16.2 billion* in lost super in New Zealand. Some of it could be yours.
				Sometimes it is best to get the help of a professional, so we've partnered with BP to make super much simpler with BP Super for Life.
			</p>
		</div>
	</div>
	<div class="sections col-md-12">
			<div class="col-md-12">
				<p>
					<h2>Everyday benefits</h2>
					<ul>
						<li>Earn a competitive interest rate</li>
						<li>No setup or monthly fees</li>
						<li>Flexible interest payment options</li>
						<li>Suitable for DIY / Self Managed Super Funds</li>
						<li>What you get</li>
					</ul>
				</p>
			</div>
			<div class="col-md-6">
				<p>
					Earn a competitive interest rate <br>
					Earn 3.00% p.a. for 12 months on balances between $1,000 and $5,000,000 with interest paid at maturity
				</p>
			</div>
			<div class="col-md-6">
				<p>
					Interest is calculated daily <br>
					Can be paid at maturity, annually, half yearly or monthly
				</p>
			</div>
			<div class="col-md-6">
				<p>
					Flexible maturity options <br>
					1 month to 5 years
				</p>
			</div>
			<div class="col-md-6">
				<p>
					Access and manage your funds <br>
					via Online and Mobile Banking
				</p>
			</div>
	</div>


	<div class="contenido col-md-12">

			<div id="ir-arriba" class="infoContenido col-md-offset-4 col-md-7">
				<h2 id="InterestRates">Interest rates</h2>
					<h3>Interest paid at maturity. Interest rates are % per annum</h3>

					<table class="table">
						<tr>
							<td></td>
							<td>$1,000 to less than $5,000</td>
							<td>$5,000 to less than $20,000</td>
							<td>$20,000 to less than $50,000</td>
							<td>$50,000 to less than $100,000</td>
							<td>$100,000 to less than $250,000</td>
							<td>$250,000 to less than $5,000,000</td>
						</tr>
						<tr>
							<td>1 to less than 2 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>2 to less than 3 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>3 to less than 4 months</td>
							<td>2.40</td>
							<td>2.40</td>
							<td>2.40</td>
							<td>2.40</td>
							<td>2.40</td>
							<td>2.40</td>
						</tr>
						<tr>
							<td>4 to less than 5 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>5 to less than 6 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>6 to less than 7 months</td>
							<td>2.45</td>
							<td>2.45</td>
							<td>2.45</td>
							<td>2.45</td>
							<td>2.45</td>
							<td>2.45</td>
						</tr>
						<tr>
							<td>7 to less than 8 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>8 to less than 9 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>9 to less than 10 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>10 to less than 11 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>11 to less than 12 months</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
							<td>2.00</td>
						</tr>
						<tr>
							<td>12 months</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
						</tr>
					</table>

					<h3>Interest paid annually.  Interest rates are % per annum.</h3>

					<table class="table">
						<tr>
							<td></td>
							<td>$1,000 to less than $5,000</td>
							<td>$5,000 to less than $20,000</td>
							<td>$20,000 to less than $50,000</td>
							<td>$50,000 to less than $100,000</td>
							<td>$100,000 to less than $250,000</td>
							<td>$250,000 to less than $5,000,000</td>
						</tr>
						<tr>
							<td>24 to less than 36 months</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
						</tr>
						<tr>
							<td>36 to less than 48 months</td>
							<td>3.20</td>
							<td>3.20</td>
							<td>3.20</td>
							<td>3.20</td>
							<td>3.20</td>
							<td>3.20</td>
						</tr>
						<tr>
							<td>48 to less than 60 months</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
						</tr>
						<tr>
							<td>60 months</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
							<td>3.10</td>
						</tr>
					</table>

					<h3>Interest paid half yearly.  Interest rates are % per annum.</h3>

					<table class="table">
						<tr>
							<td></td>
							<td>$1,000 to less than $5,000</td>
							<td>$5,000 to less than $20,000</td>
							<td>$20,000 to less than $50,000</td>
							<td>$50,000 to less than $100,000</td>
							<td>$100,000 to less than $250,000</td>
							<td>$250,000 to less than $5,000,000</td>
						</tr>
						<tr>
							<td>12 to less than 24 months</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
						</tr>
						<tr>
							<td>24 to less than 36 months</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
						</tr>
						<tr>
							<td>36 to less than 48 months</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
						</tr>
						<tr>
							<td>48 to less than 60 months</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
						</tr>
						<tr>
							<td>60 months</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
						</tr>
					</table>

					<h3>Interest paid monthly.  Interest rates are % per annum.</h3>

					<table class="table">
						<tr>
							<td></td>
							<td>$1,000 to less than $5,000</td>
							<td>$5,000 to less than $20,000</td>
							<td>$20,000 to less than $50,000</td>
							<td>$50,000 to less than $100,000</td>
							<td>$100,000 to less than $250,000</td>
							<td>$250,000 to less than $5,000,000</td>
						</tr>
						<tr>
							<td>6 to less than 7 months</td>
							<td>2.25</td>
							<td>2.25</td>
							<td>2.25</td>
							<td>2.25</td>
							<td>2.25</td>
							<td>2.25</td>
						</tr>
						<tr>
							<td>7 to less than 8 months</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
						</tr>
						<tr>
							<td>8 to less than 9 months</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
						</tr>
						<tr>
							<td>9 to less than 10 months</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
						</tr>
						<tr>
							<td>10 to less than 11 months</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
						</tr>
						<tr>
							<td>11 to less than 12 months</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
							<td>1.80</td>
						</tr>
						<tr>
							<td>12 to less than 24 months</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
							<td>2.80</td>
						</tr>
						<tr>
							<td>24 to less than 36 months</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
						</tr>
						<tr>
							<td>36 to less than 48 months</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
							<td>3.00</td>
						</tr>
						<tr>
							<td>48 to less than 60 months</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
							<td>2.60</td>
						</tr>
						<tr>
							<td>60 months</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
							<td>2.90</td>
						</tr>
					</table>

			</div>
		</div>
	<?php
		pie();
	?>
	<script>
	    $(document).ready(function () {
	        $('#sect1').addClass('active');
	    });
		$("#E-Banking").html('Personal E-Banking');
	</script>
</body>
</html>
