<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src='<?php host();?>/rs/img/bann.jpg' id="img_banner">
	</div>
<div class="container">
	<div class="col-md-12">
		<h1>Your privacy and security</h1>
		<div class="col-md-9 col-md-offset-2">
			<h3>Report your security issues</h3>
                <p>
                    If you have any concerns about your BP BANK accounts or online security, or you suspect you have responded to a fraudulent email, please call our Customer Solutions Advisors on 0800 735 901 or +64 4 473 5901 from overseas (international toll charges apply).
                </p>

            <h3>How we use personal information you provide online or in store</h3>
                <p>
                    Wherever you provide personal information about yourself, whether it’s online or in store, we will do everything we can to protect it from being used wrongly.
                </p>
                <p>
                    We follow the privacy provisions as set out in the Privacy Act, which says we can collect information for a lawful purpose. We believe in being honest and upfront with our customers at all times. We’ll only disclose information in accordance with our Standard terms and conditions, or if we are required to by law.
                </p>
                <p>
                    Under the Privacy Act, you’re entitled to see and request the correction of any personal information about you held by us. As a new customer, you can choose not to supply this information but we may not be able to accept you as a customer.
                </p>

            <h3>How we keep your money and identity secure online</h3>
                <p>
                    We have taken steps to make sure that you and your computer are safe when visiting our website and/or using Internet Banking, Internet Banking for Business, and Mobile Banking.
                </p>
                <h4>A secure website</h4>
                    <p>
                        Our website has security certificates (issued by Entrust) which prove to you that our site is genuine, encrypted, and secure, giving you peace of mind that your personal and financial confidential information is protected.  You can check that our site (and any other website you visit) has been authenticated with a security certificate easily in any internet browser you are using.  Proof of the authentication appears in a variety of ways — the address bar or part of the URL may be green (or any other colour depending on your browser) or there will be a padlock symbol next to the address or in some other part of the address bar. Sometimes, you may have to click a similar padlock symbol at the bottom of the browser page to see the certificate.  If you ever go to our website, and you can’t see a sign that the site has a security certificate, please call us on 0800 800 468.
                    </p>
                <h4>Protecting your banking information with advanced technology</h4>
                    <ul>
                        <li>
                            To stop other people seeing or tampering with your internet or mobile banking information, we use encryption to scramble the information while it travels over the internet or mobile phone networks
                        </li>
                        <li>
                            We also use sophisticated firewall technology to protect our computer systems, and your information within them, against fraudulent access via the internet
                        </li>
                        <li>
                            If you are logged into one of our internet banking services, but you haven’t done anything for 20 minutes (or 5 minutes on Mobile Banking) ,we’ll automatically log you out to prevent anyone else continuing your session and accessing your accounts or information through that computer or smartphone
                        </li>
                    </ul>
                <h4>Online Banking and PC Business Banking</h4>
                    <p>
                        When you use our Internet Banking services, you are protected by NetGuard or Mobile NetGuard if you are using the Mobile Banking app, adding an additional layer of security to our other systems and procedures. PC Business Banking also comes with its own robust, customisable online security protection, including options to have multiple users authenticating each other’s transactions and the ability to control what individual users can do within system.
                    </p>
                <h4>Monitoring our security systems, equipment and suspicious activity</h4>
                    <ul>
                        <li>
                            We regularly audit and inspect our security systems and equipment
                        </li>
                        <li>
                            We continuously monitor online banking transactions and if there’s any suspicious activity on your accounts our fraud department will be notified immediately
                        </li>
                        <li>
                            We provide security alerts on www.banquepasche.nz whenever we become aware of suspicious activity online
                        </li>
                    </ul>
                <h4>Things we’ll never ask you to do</h4>
                    <p>
                        We will never change the login process for Internet Banking without telling you first, and we will never ask you to tell us or anyone your password.
                    </p>
            <h3>Tips on keeping your banking information secure</h3>
                <p>
                    Always type www.banquepasche.nz into your browser when you do your banking, or visit our site.
                    Think twice before clicking. Take a moment to look at it and if anything seems remotely suspicious, whether it’s an email or on a website, avoid it.
                    Make sure any virus protection you have on your computer is up-to date, including your operating system and your software. See below for more information.
                    Choose strong passwords. Avoid any significant names or numbers, like birthdays, and try to use a mix of numbers and letters in lower and upper case. Learn more about passwords.
                    Never share your login details or confidential banking and personal information with anyone, not even with family members. Don’t store your information where it can be read or accessed by anyone else.
                </p>
            <h3>Improve your online security with anti-virus software</h3>
                <p>
                    You can protect your computer’s hardware and software from internet threats like malware, which includes computer viruses, spyware, trojans, and rootkits, by installing and maintaining reliable and reputable anti-virus (AV) software. These Internet threats often infect computers through compromised websites, email attachments, and affected files from attached devices such as a USB sticks and then corrupt and delete data on your computer and/or use your email account to infect other computers by sending illegal spam emails. If your computer is running really slowly, it might be sign it is infected. To help keep your computer virus-free, make sure:
                </p>
                <ul>
                    <li>Your AV software is configured properly so it doesn’t slow your computer down</li>
                    <li>Your AV software is configured to automatically update everyday</li>
                    <li>You schedule a weekly AV scan of your computer</li>
                </ul><br><br><br>
		</div>
	</div>
</div>

<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#pie1').addClass('activefooter');
	});
	$("#E-Banking").html('Personal E-Banking');
</script>
</body>
</html>
