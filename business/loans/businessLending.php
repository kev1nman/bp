<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Business lending</h1>
			<p>
				Flexible options to help you reach your business goals.
			</p>
	</div>

	<div class="rows">
		<div class="col-md-9 col-md-offset-2">
		<h3>Why get a loan with us?</h3>
			<ul>
				<li>Fixed or variable interest rates.</li>
				<li>Flexible repayment options.</li>
				<li>Terms from six months to five years (and sometimes longer).</li>
				<li>Secured against business or personal assets.</li>
			</ul>

		<h3>How it works</h3>
			<p>
				One of our business lending experts will work with you to figure out the best option for your business. Once we've agreed on a loan amount and term, you'll have a couple of options to choose from:
			</p>

		<h3>Fixed rate loans</h3>
			<h4>Your interest rate will stay the same for the agreed term, giving you certainty about repayments</h4>
			<ul>
				<li>The rate will depend on the terms of your loan and the security you have to offer.</li>
				<li>You can repay your loan early, but fixed rate break costs may apply.</li>
			</ul>

		<h3>Variable rate loans</h3>
			<h4>Tailor repayments to suit your cashflow — pay off more when you can.</h4>
			<ul>
				<li>Make lump sum payments whenever you want — good if your income tends to be irregular.</li>
				<li>Fix all or part of your loan later if you want to.</li>
			</ul>
			<p>
				Most of our loans are table loans. This means your payments stay the same over the term of your loan. At the start of your loan, your payments will mostly cover interest — but you’ll pay back less interest and more principal as time goes on.
				<br><br>
				We also offer interest-only loans. These are a good short-term backstop to help ease cashflow as you get set up — your repayments only cover the interest you’re charged, so at the end of the agreed term, the loan will need to be repaid in full or renegotiated.
			</p><br><br>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
