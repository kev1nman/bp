<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Loans</h1>
			<p>
				We’ve got a range of ways to borrow — whether you’re expanding your business, just starting out, or need some funds to tide you over.
			</p><br><br>
	</div>
</div>

<div class="sectionsTitle col-md-12">
</div>

<div class="sections col-md-12">
		<div class="sect col-md-4 col-sm-12">
			<div class="section1">
				<div class="">
					<a href="<?php host();?>/business/loans/businessLending.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" width="300"/></a>
				</div>
				<h4>Business lending</h4>
				<legend></legend>
				<h4>Flexible options to help you grow or get going.</h4>
				<ul>
					<li>Fixed or variable interest rates.</li>
					<li>Flexible repayment options.</li>
					<li>Terms from six months to five years (and sometimes longer).</li>
					<li>Secured against business or personal assets.</li>
				</ul>
				<a class="btn btn-info" href="<?php host();?>/business/loans/businessLending.php">Read more</a>
			</div>
		</div>
		<div class="sect col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/business/loans/assetFinance.php">
						<img width="300" class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/creditCard.jpg" alt="" /></a>
				</div>
				<h4>Asset finance</h4>
				<legend></legend>
				<h4>Loans from BP Bank Asset Finance to help with the purchase of capital assets.</h4>
				<ul>
					<li>Loans to buy business assets: vehicles, equipment or tools.</li>
					<li>Borrow up to 100% of the purchase price.</li>
					<li>Immediate ownership and use of the asset.</li>
					<li>Secured against the assets themselves.</li>
				</ul>
				<a class="btn btn-info" href="<?php host();?>/business/loans/assetFinance.php">Read more</a>
			</div>
		</div>
		<div class="sect col-md-4 col-sm-12">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/business/loans/commercialBankingPropertyFinance.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" /></a>
				</div>
				<h4>Commercial Banking and Property Finance</h4>
				<legend></legend>
				<h4>Loans for medium or large businesses, property investors and developers.</h4>
				<ul>
					<li>Large scale loans for terms up to five years (or sometimes longer).</li>
					<li>Our specialist lenders will tailor a package to suit.</li>
					<li>New borrowing or refinancing.</li>
					<li>Secured against business and/or personal assets.</li>
				</ul>
				<a class="btn btn-info" href="<?php host();?>/business/loans/commercialBankingPropertyFinance.php">Read more</a>
			</div>
		</div>
</div>

<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
