<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Asset finance</h1>
		<p>
			BP Asset Finance was set up by BP Bank to provide term loans to help with the purchase of capital assets.
		</p>
	</div>
	<div class="rows">
		<div class="col-md-9 col-md-offset-2">
			<h3>Why BP Asset Finance?</h3>
				<p>
					BP Asset Finance is managed by experienced finance professionals. They’ve been providing asset finance for many years, so they understand the needs of BP businesses.
					<br>Capital assets are things like:
				</p>
				<ul>
					<li>Business motor vehicles</li>
					<li>Commercial trucks and trailers</li>
					<li>Earthmoving and contracting equipment</li>
					<li>Plant and machinery.</li>
				</ul>
			<h3>How it works</h3>
				<p>
					BP Asset Finance can lend you the money to buy the things you need to run your business. You’ll have ownership and use of the asset while you’re paying it off — the loan is secured by the asset, or against other existing assets if additional security is required.
				</p>
				<ul>
					<li>You can borrow up to 100% of the purchase price of the new asset (if additional security is provided).</li>
					<li>Borrow from $20,000 (consideration given to lesser amounts).</li>
					<li>Terms from six months to five years.</li>
					<li>Competitive interest rates.</li>
					<li>Structure your repayments to suit your situation.</li>
				</ul>
				<p>
					You don’t need to be a Kiwibank customer to get funding for capital assets.
				</p><br><br>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
