<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Commercial Banking and Property Finance</h1>
			<p>
				Loans for bigger businesses with bigger plans, property investors and developers.
			</p>
	</div>
	<div class="rows">
		<div class="col-md-9 col-md-offset-2">
			<h3>Why Commercial Banking and Property Finance?</h3>
			<p>
				If you’re a medium or large business looking for finance our specialist lenders can help — whether you’re:
			</p>
			<ul>
				<li>looking to refinance</li>
				<li>funding growth or a new venture</li>
				<li>purchasing your own premises</li>
				<li>subdividing land</li>
				<li>developing or investing in multi-unit housing or apartments</li>
				<li>developing or investing in commercial or industrial properties.</li>
			</ul>

			<h3>How it works</h3>
				<p>
					We offer large-scale loans for terms up to five years (and sometimes longer). Our specialist lenders will design a full suite of banking services for you and your business, with additional options including:
				</p>
				<ul>
					<li>bonds</li>
					<li>bank guarantees</li>
					<li>interest rate hedging</li>
					<li>foreign exchange services</li>
					<li>Importers & Exporters Letters of Credit</li>
					<li>Collections & Foreign Currency payment services.</li>
				</ul>
				<p>
					Your loans need to be secured by business and/or personal assets.
				</p><br><br>
		</div>
	</div>
</div>

<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
