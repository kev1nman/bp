<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Credit cards</h1>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
		</p>
	</div>
	<div class="imgCreditCard col-md-3">
		<img class="creditCard" src="<?php host();?>/rs/img/BLACKCardMastercard.png" alt="" />
	</div>
	<div class="contenido col-md-12">
		<div class="infoContenido col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr class="warning">
							<th><h4>Credit card</h4></th>
							<th class="columnaRate">
								<img class="imgCreditCardTable" src="<?php host();?>/rs/img/CLASSICCardMasterCard.png" alt="" />
								Classic <br> <a href="<?php host();?>/business/creditcard/classic.php">Find out more</a>
							</th>
							<th class="columnaRate">
								<img class="imgCreditCardTable" src="<?php host();?>/rs/img/GOLDCardMastercard.png" alt="" />
								Gold Revolving <br> <a href="<?php host();?>/business/creditcard/gold.php">Find out more</a>
							</th>
							<th class="columnaRate">
								<img class="imgCreditCardTable" src="<?php host();?>/rs/img/PLATINUMCardMastercard.png" alt="" />
								Platinum <br> <a href="<?php host();?>/business/creditcard/platinum.php">Find out more</a>
							</th>
							<th class="columnaRate">
								<img class="imgCreditCardTable" src="<?php host();?>/rs/img/BLACKCardMastercard.png" alt="" />
								Black <br> <a href="<?php host();?>/business/creditcard/black.php">Find out more</a>
							</th>
						</tr>
					</thead>
				</table>
			</div>
			<ul>
				<li>Full travel insurance risk cover <a href="#">here</a>.</li>
				<li>In order for travel insurance to be in force for the Gold Revolving Credit Card holders and their family, the expenses of accommodation or buying the trip or transport tickets or fuel must be paid for:
					<ul>
						<li>With the Gold Revolving Credit Card</li>
						<li>By a money transfer from BP bank account</li>
						<li>With debit cards linked to BP bank accounts</li>
					</ul>
				</li>
			</ul>
			<p>
				<b>Travel costs:</b> accommodation, trip purchase, tickets, fuelBorrow responsibly by fairly assessing your loan repayment capabilities!.
			</p>
		</div>
	</div>

</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
