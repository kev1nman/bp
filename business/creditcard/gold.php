<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Credit Card | <b><i>Gold</i></b></h1>
		<p>
			The Gold Revolving Credit Card allows combining business growth with relaxing and enjoying life to the fullest.Gold Revolving Credit Card gives access to a loan with any strict repayment deadline. Interest-free period during which you don't pay interest on purchases made before 10th day of the next month.
		</p>
	</div>
	<div class="imgCreditCard col-md-3">
		<img class="creditCard" src="<?php host();?>/rs/img/GOLDCardMastercard.png" alt="" />
	</div>
	<div class="col-md-12">
		<div class="col-md-6">
			<p>
				<h3>Offers low interest rate on purchases and interest free period to help manage your business cash flow</h3>
				<ul>
					<li>Interest rate: 9.99% p.a. (variable) on purchases</li>
					<li>Interest rate: 17.15% p.a. (variable) on cash advances.</li>
					<li>Up to 55 days interest free on purchases when you pay the closing balance (including any balance transfer amount) by the statement due date each month.</li>
					<li>Competitive annual fee of $55 per card</li>
				</ul>
				The Business Gold Credit Card has a variable purchase rate of 9.99% p.a. and up to 55 days
				interest-free on purchases.
			</p>
		</div>
		<div class="col-md-6">
			<h3>Everyday benefits</h3>
			<ul>
				<li>9.99% p.a.(variable) interest rate on purchases - new cards only</li>
				<li>Up to 55 days interest free on purchases</li>
				<li>Greater expense visibility</li>
				<li>Detailed reporting</li>
				<li>Secure online shopping</li>
			</ul>
		</div>
	</div>
	<div class="col-md-12">
		<h3>Features</h3>
		<div class="col-md-6">
			<p>
				<h4>Competitive interest rates</h4>
					9.99% p.a. (variable) interest rate on purchases.
					17.15% p.a. (variable) cash advance rate.

				<h4>Easier expense reporting</h4>
					Comprehensive statements to help monitor and manage your expenses and optional consolidated summaries by department and division.
					<br><br>
					24/7 Internet banking access to manage your account (subject to systems availability).

				<h4>Insurance</h4>
				Complimentary insurance against fraudulent use and unauthorised transactions.
			</p>
		</div>
		<div class="col-md-6">
			<p>
				<h4>Business credit limit</h4>
				Minimum Business credit limit on the credit card facility is $1,000. You can have an unlimited number of credit cards for the card facility.

				<h4>Full service</h4>
				24 hour Visa Emergency Assistance Centre available.<br>
				Register for GoBP for business tools and tips and special offers


				<h4>Contactless payments</h4>
				Speed and convenience – no need to sign or enter PIN for everyday purchases under $100 at participating merchants, who accept contactless transactions.
			</p>
		</div>
	</div>
	<div class="col-md-12">
		<h3>Fees</h3>
		<table class="table">
			<tr>
				<td>Low annual fee</td>
				<td>
					$55 annual fee per credit card (waived for up to three cards if eligible for BP Bank BPPack).
					Up to 55 days interest free on purchases when you pay the closing balance (including any balance transfer or promotional amount) by statement due date each month.
				</td>
			</tr>
			<tr>
				<td>Cash advance transfer</td>
				<td>
					If your credit card account is in credit (has a positive balance) after a cash advance transaction, a flat fee of $2.50 is payable.
					If your account is in debit after a cash advance transaction, you will be charged 2% of the cash advance amount, with a minimum fee of $2.50 and a maximum fee of $150.
					These fees will appear on your credit card statement directly below the relevant cash advance.
				</td>
			</tr>
			<tr>
				<td>Foreign transaction fee</td>
				<td>
					A 3% Foreign transaction fee is payable as a percentage of the New Zealand dollar value of any Foreign transaction.
					<br>
					Foreign transaction is any transaction made using the Card:
					<ul>
						<li>In a currency other than New Zealand dollars; or</li>
						<li>In American dollars or any other currency with a merchant located outside New Zealand; or</li>
						<li>In American dollars or any other currency that is processed by an entity located outside New Zealand.</li>
					</ul>
					Note: It may not always be clear to you that the Merchant or entity processing the transaction is located outside New Zealand.
				</td>
			</tr>
		</table>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
