<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Credit Card | <b><i>Classic</i></b></h1>
	</div>
	<div class="imgCreditCard col-md-3">
		<img class="creditCard" src="<?php host();?>/rs/img/CLASSICCardMasterCard.png" alt="" />
	</div>
	<div class="contenido col-md-6">
		<p>
			<h3>Earn uncapped points</h3>
			Earn uncapped points on business purchases including double points on international spending. There are two rewards programs to choose from.

			<h3>Complimentary insurance</h3>
			Enjoy a range of complimentary insurance covers - including Transit Accident Insurance, Business Inconvenience Insurance and Unauthorised Transaction Insurance.

			<h3>Contactless payments</h3>
			Speed and convenience – no need to sign or enter PIN for everyday purchases under $100 at participating merchants, who accept contactless transactions.

			<h3>Secure online shopping</h3>
			Additional layer of security provided to you by St.George free of charge when shopping with participating online retailers.
		</p>
	</div>
	<div class="contenido col-md-6">
		<p>
			<h3>Cards per account</h3>
			The Amplify Business credit card is a personal liability credit card with a maximum of 4 cards per account consisting of a single account owner and up to 3 additional cardholders.

			<h3>Simplify tax reporting</h3>
			Export expenses directly into MYOB and BAS. Transactions can also be downloaded into excel to make analysis of business transactions easy.

			<h3>Choose between two great rewards programs</h3>
			Points earned on purchases	Link your card to Amplify Rewards*	Link your card to Amplify Qantas
			NZ    purchases	            $1 = 1 Amplify Point	$1 = 0.5 Qantas Points
			Overseas purchases	$1 = 2 Amplify Points	$1 = 1 Qantas Point
			Government payments	$1 = 0.5 Amplify Points	$1 = 0.25 Qantas Points
			* Earn bonus Amplify Points with Business Clasic Credit Card Bonus Partners
		</p>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
