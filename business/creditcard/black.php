<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Credit Card | <b><i>Black</i></b></h1>
		<p>
			The Corporate Credit Card brings a new level of efficiency to corporate card use and reporting. Assisted by Internet technology, you are able to monitor card usage and expenditure.
			<br><br>
			When it comes to business purchasing, travel and entertainment, choosing the right card and expense management system, to control and manage your organisation's expenditure is essential. That is why the Institutional Black Credit Card has been developed to provide much more than simply the purchasing efficiency and global acceptance you would expect from a Institutional card.
			<br><br>
			By linking your Institutional Black Credit Card  Smart Data Online, a complete online expense management system, you can access up to 20 reports, giving you greater visibility of card use and expenditure. Take a look at our demonstration to see how Credit Card Smart Data Online can help make your credit card management easier.
		</p>
	</div>
	<div class="imgCreditCard col-md-3">
		<img class="creditCard" src="<?php host();?>/rs/img/BLACKCardMastercard.png" alt="" />
	</div>

	<div class="col-md-12">
		<h3>Features & Benefits</h3>
		<table class="table">
			<tr>
				<td><h4>Convenience</h4></td>
				<td>
					<ul>
						<li>MasterCard acceptance throughout the world</li>
						<li>Online MIS Reporting via MasterCard Smart Data Online</li>
						<li>One card convenience</li>
						<li>No limit to the number of cards</li>
						<li>Up to 35 days interest free on purchases</li>
						<li>Cash rate of 17.45% p.a.</li>
						<li>Automatic payment / direct debit from any bank account</li>
						<li>Individual card credit limits</li>
						<li>Additional deposits</li>
						<li>Interface with Financial Management system</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td><h4>Control</h4></td>
				<td>
					<ul>
						<li>Select individual credit limits</li>
						<li>Ability to set ATM cash withdrawal limits for individual cardholders</li>
						<li>Hierarchy management control</li>
						<li>Authorise transactions online</li>
						<li>Up to 20 different reporting functions available</li>
						<li>A foreign transaction fee on each foreign transaction</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td><h4>Security</h4></td>
				<td>
					<ul>
						<li>Secure access to Credit Card Smart Data Online</li>
						<li>Complimentary liability and travel accident insurance covers</li>
						<li>Monthly statements posted directly to cardholder</li>
						<li>PIN issued for cash access</li>
						<li>Shop online with Verified by Visa or Master Card knowing your internet purchases and payments are safer and more secure.</li>
					</ul>
				</td>
			</tr>
		</table>
	</div>
	<div class="col-md-6">
		<h3>Convenience</h3>
		<p>
			<h4>MasterCard acceptance</h4>
			To meet the needs of our Corporate & Business Bank customers, BP Bank Institutional Black Credit card gives you worldwide acceptance for purchases, cash advances and ATM withdrawals wherever MasterCard is accepted..

			<h4>MIS Reporting via institutional Black Credit Card Data Online</h4>
			Black Credit Card Smart Data Online can help make credit card management easier. This internet-based management system provides sophisticated reporting and management options featuring details of all card expenditure and transactions.
			<br>
			Online reporting makes expense reconciliation, monitoring and auditing easier than ever before. Up to 20 different reporting functions are available, delivering a new level of control over card use and management.
			<br>
			Institutional Black Credit Card helps beat the paper chase for you and your staff. Key functions and features include GST management capabilities, individual cardholder access for transaction authorization and reconciliation, and the ability to download data and interface with compatible financial systems.
			<br>
			Institutional Black Credit Card allows you to engage with travel agencies to provide specific travel data. This enhanced information enables you to negotiate supplier pricing and identify cost saving opportunities. An example of these travel partners include Qantas Business Travel, Amex Travel Management and Avis Car Rental.
			<br>
			Take a look at our demonstration to see how Institutional Black Credit Card can help make your credit card management easier.

			<h4>One card convenience</h4>
			Our one card strategy gives you the convenience of making a variety of purchases from general procurement and purchasing for travel and entertainment.

			<h4>No limit to the number of cards</h4>
			There is no limit to the number of cards available to be issued to employees within your organization provided the total of all individual card credit limits does not exceed the facility limit.

			<h4>Up to 35 days interest free on purchases</h4>
			Your suppliers enjoy almost instant payment while you can benefit from an interest free period of up to 35 days on purchases when you pay your closing balance in full by the statement due date.

			<h4>Automatic payment from any bank account</h4>
			Save time and effort on paperwork with the convenience of automatic monthly payments. At the end of every month BP Bank automatically debits the total debit balance of all cards to your nominated account.

			<h4>Purchases up to the cardholder available balance</h4>
			Cardholders will be able to obtain cash advances and make purchases up to the available balance on their card. We provide you with the flexibility to set for each card the credit limit and daily ATM cash withdrawal limit.

			<h4>Additional deposits</h4>
			Additional deposits to card accounts can be made via BPAY®, or over the counter at BP Bank or BankSA branches.

			<h4>Interface with Financial Management systems</h4>
			You won't need special software to integrate Institutional Black Credit Card into your existing accounting system. It lets you or your staff transfer data electronically, which could save hours of administrative time each month. If required, all transactions can be allocated a general ledger code and/or a project code. For maximum efficiency, reports and data can even be downloaded, if required, into your organization's own financial management system.
		</p>
	</div>
	<div class="col-md-6">
		<h3>Control</h3>
		<p>
			<h4>Individual card credit limits</h4>
			We provide you with the flexibility to allocate cards to selected employees and set their individual credit limits (between $500 and $250,000).

			<h4>ATM cash withdrawal limits</h4>
			ATM cash access is available throughout the world with each cardholder using their own Personal Identification Number (PIN). Daily ATM cash withdrawal limits can be specified for each cardholder between $0 and $2,000.

			<h4>Hierarchy management control</h4>
			Institutional Black Credit Card uses a hierarchical reporting structure. The hierarchy groups cardholders, departments, or divisions for the purpose of running reports, exporting transaction data and reviewing transactions. The administrator will have the highest level in the hierarchy and will have access to all levels, including supervisors and accounts.

			<h4>Control on cardholder spend</h4>
			You have the ability to control cardholder spend by monitoring cardholder expenditures, reviewing reports that summarize all card transactions, purchases at specific merchant types, and so on. Reports can be scheduled to run at whatever frequency you require - e.g. daily, monthly, weekly, quarterly etc.
			Card activity monitoring can be available up to 48 hours after the actual transaction.

			<h4>Authorize transactions online</h4>
			Cardholders can check and authorize transactions via Institutional Black Credit Card as frequently as required. They can split transactions online if they are for multiple purposes (e.g. lunch bill - part FBT and part not) or for general ledger allocation across multiple cost centres.

			<h4>A foreign transaction fee</h4>
			A 3% Foreign transaction fee is payable as a percentage of the American dollar value of any Foreign transaction.
			<br><br>
			Foreign transaction is any transaction made using a Card:
			<ul>
				<li>in a currency other than Australian dollars; or</li>
				<li>in Australian dollars or any other currency with a merchant located outside Australia; or</li>
				<li>in Australian dollars or any other currency that is processed by an entity located outside Australia.</li>
			</ul>
			Note: It may not always be clear to you that the Merchant or entity processing the transaction is located outside Australia.

			<h4>Security</h4>
			<h4>Secure access to MasterCard Smart Data Online</h4>
			You have the ability to create a secure access hierarchy, so that transaction and account data can be shared throughout your organization with appropriate levels of security access.

		</p>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
