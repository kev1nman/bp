<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Business Select Saving</h1>
		<p>
			Earn high interest while you’re saving for bigger-ticket items. Start with $2,000 and add money whenever you want — just give either 32 or 90 days’ notice to make a withdrawal.
		</p>
	</div>
	<div class="col-md-9 col-md-offset-2">
		<p>
			2.75%p.a.<br>
			90 days’ notice.<br>
			Minimum balance $2,000.<br>
			Maximum balance $1 million.<br>
			2.25%p.a.<br>
			32 days' notice.<br>
			Minimum balance $2,000.<br>
			Maximum balance $1 million.<br>
		</p>
	</div>

	<div class="rows">
		<div class="col-md-6">
			<h2>Why Business Select Saving?</h2>
				<ul>
					<li>$2,000 minimum balance to earn interest.</li>
					<li>Add money anytime.</li>
					<li>Just give 32 or 90 days’ notice to make a withdrawal — you choose the notice period when you set up your Notice Saver.</li>
					<li>No monthly account management fee or transaction fees.</li>
					<li>Choose to have your interest compounding or paid out monthly.</li>
				</ul>
		</div>
		<div class="col-md-6">
			<h2>How it works</h2>
			<ul>
				<li>You can’t link your account to a card or cheque book — give notice to withdraw some or all of your funds by making a transfer in internet or phone banking, and the money will be transferred into your nominated account after the notice period is up.</li>
				<li>Transfer money in from any other BP Bank account using internet or phone banking.</li>
				<li>You can change your notice period in internet banking.</li>
			</ul>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
