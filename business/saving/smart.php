<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Business Smart Saving</h1>
		<p>
			Up to <br>
			1.75%p.a.<br>
			Check out the interest you’ll receive on your balance
		</p>
	</div>
	<div class="col-md-9 col-md-offset-2">
		<div class="col-md-12">
			<h2>Business Smart Saving account</h2>
			<p>
				A good place to stash money for regular bills and expenses — you’ll earn interest on balances over $5k, and you can make free transfers to your other BP Bank accounts whenever you need to.
			</p>
		</div>
		<div class="col-md-12">
			<h2>Why Business Performer?</h2>
			<ul>
				<li>No minimum balance, and you’ll start earning interest once you reach $5k.</li>
				<li>Free transfers to your other BP Bank accounts (excluding credit cards).</li>
				<li>No monthly account fee.</li>
				<li>25 free electronic deposits and 25 free deposits at your local BP Bank every month.</li>
				<li>Interest calculated daily and paid monthly.</li>
			</ul>
		</div>
		<div class="col-md-12">
			<h2>How it works</h2>
			<ul>
				<li>Access your money whenever you need it.</li>
				<li>Transfer funds in and out of your other BP Bank accounts (excluding credit cards) for free.</li>
				<li>You can’t link your account to a card, but you can make payments to anyone through internet banking — it’s a good place to hold money for bills and expenses, so you always know you’ll have enough set aside.</li>
				<li>Start earning interest at $5k, and earn more the higher you get. </li>
			</ul><br><br>
		</div>
	</div>

</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
