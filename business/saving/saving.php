<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Business saving account</h1>
		<p>
			Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
		</p><br><br>
	</div>
</div>

<div class="sectionsTitle col-md-12">
</div>

<div class="sections col-md-12">
		<div class="sect col-md-4 col-sm-12">
			<div class="section1">
				<div class="">
					<a href="<?php host();?>/business/saving/smart.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" width="300"/></a>
				</div>
				<h4>Smart</h4>
				<legend></legend>
				<p>
					Up to <br>
					1.75%p.a.<br>
					Check out the interest you’ll receive on your balance
				</p>
				<a class="btn btn-info" href="<?php host();?>/business/saving/smart.php">More info</a>
			</div>
		</div>
		<div class="sect col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/business/saving/master.php">
						<img width="300" class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/creditCard.jpg" alt="" /></a>
				</div>
				<h4>Master</h4>
				<legend></legend>
				<p>
					A good place to stash tax payments, or money you’re setting aside for a short time. Earn bonus interest if you don’t make any withdrawals during the month.
				</p>
				<a class="btn btn-info" href="<?php host();?>/business/saving/master.php">More info</a>
			</div>
		</div>
		<div class="sect col-md-4 col-sm-12">
			<div class="section1">
				<div class="imgSections">
					<a href="<?php host();?>/business/saving/select.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" /></a>
				</div>
				<h4>Select</h4>
				<legend></legend>
				<p>
					Earn high interest while you’re saving for bigger-ticket items. Start with $2,000 and add money whenever you want — just give either 32 or 90 days’ notice to make a withdrawal.
				</p>
				<a class="btn btn-info" href="<?php host();?>/business/saving/select.php">More info</a>
			</div>
		</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
