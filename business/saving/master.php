<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Business Master Saving account</h1>
		<p>
			A good place to stash tax payments, or money you’re setting aside for a short time. Earn bonus interest if you don’t make any withdrawals during the month.
		</p>
	</div>
	<div class="col-md-9 col-md-offset-2">
		<div class="col-md-9 ">
			<p>
				1.75%p.a.<br>
				for balances above $2,000.<br>
				0.75%p.a.<br>
				For the calendar month if you make a withdrawal.
			</p>
		</div>
		<div class="rows">
			<div class="col-md-6">
				<h2>Why Business Master Saving?</h2>
					<ul>
						<li>$2,000 minimum balance to earn interest.</li>
						<li>No monthly account management fee.</li>
						<li>No fixed term — transfer money in and out whenever you need to.</li>
						<li>Earn bonus interest if you don’t make any withdrawals during the month.</li>
						<li>100 free deposits a month and no charge to transfer funds to your nominated account.</li>
						<li>PIE option available — ask us if it might be right for you.</li>
					</ul>
			</div>
			<div class="col-md-6">
				<h2>How it works</h2>
				<ul>
					<li>You can’t link your Business Online Call account to a card or cheque book. Instead, it’s linked to a nominated account — either with BP Bank or any other New Zealand registered bank.</li>
					<li>Make a withdrawal by transferring money to your nominated account.</li>
					<li>Transfer money in using internet or phone banking or at your local BP Bank.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
