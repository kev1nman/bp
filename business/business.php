<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
	<div id="banner">
		<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
	</div>
	<div class="container">
		<div class="col-md-9">
			<h1>Business banking</h1>
			<p>
				Our dedicated Business Banking specialists know what makes successful BP organizations tick. Along with products and services built specifically for the New Zealand market, we are here to help your business.
			</p><br><br>
		</div>
	</div>
	<div class="sectionsTitle col-md-12">
		<h3><b>What our customers have to say, </b><em>don’t take our word for it. We asked our business banking customers to share their view of how BP Bank has worked for them</em></h3>
	</div>

	<div class="sections col-md-12">
			<div class="sect col-md-3 col-sm-12">
				<div class="section1">
					<div class="">
						<a href="<?php host();?>/business/saving/saving.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" width="300"/></a>
					</div>
					<h4>Saving</h4>
					<legend></legend>
					<p>
						Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
					</p>
					<a class="btn btn-info" href="<?php host();?>/business/saving/saving.php">More info</a>
				</div>
			</div>
			<div class="sect col-md-3 col-sm-12">
				<div class="section2">
					<div class="imgSections">
						<a href="<?php host();?>/business/everyday/everyday.php">
							<img width="300" class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/creditCard.jpg" alt="" /></a>
					</div>
					<h4>Everyday</h4>
					<legend></legend>
					<p>
						Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
					</p>
					<a class="btn btn-info" href="<?php host();?>/business/everyday/everyday.php">More info</a>
				</div>
			</div>
			<div class="sect col-md-3 col-sm-12">
				<div class="section1">
					<div class="imgSections">
						<a href="<?php host();?>/business/creditcard/creditcard.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/saverOnCall.jpg" alt="" /></a>
					</div>
					<h4>Credit card</h4>
					<legend></legend>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</p>
					<a class="btn btn-info" href="<?php host();?>/business/creditcard/creditcard.php">More info</a>
				</div>
			</div>
			<div class="sect col-md-3 col-sm-12">
				<div class="section1">
					<div class="imgSections">
						<a href="<?php host();?>/business/loans/loans.php">
							<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/savingsPlus.jpg" alt="" /></a>
					</div>
					<h4>Loans</h4>
					<legend></legend>
					<p>
						We’ve got a range of ways to borrow — whether you’re expanding your business, just starting out, or need some funds to tide you over.
					</p>
					<a class="btn btn-info" href="<?php host();?>/business/loans/loans.php">More info</a>
				</div>
			</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-3 col-md-offset-3">
			<h2>TERM DEPOSIT SPECIAL </h2>
			<p>
				3.20% p.a.<br>
				for 120 days. Minimum investment $10,000. Interest at maturity
			</p>
		</div>
		<div class="col-md-3">
			<h2>BUSINESS MASTERCARD</h2>
			<p>
				16.90% p.a.<br>
				$0+<br>
				Standard interest rate.
			</p><br><br>
		</div>
	</div>
	<?php
 		pie();
 	?>
	<script>
	    $(document).ready(function () {
	        $('#sect2').addClass('active');
	    });
		$("#E-Banking").html('Business E-Banking');
	</script>
</body>
</html>
