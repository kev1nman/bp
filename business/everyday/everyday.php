<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-9">
		<h1>Business everyday account</h1>
		<p>
			Thought of saving, we have the best for you; personalized attention, extensive network of ATMs, access to your account via web or mobile.
		</p><br><br>
	</div>
</div>
<div class="sectionsTitle col-md-12">
</div>

<div class="sections col-md-12">
		<div class="sect col-md-4 col-sm-12 col-md-offset-2">
			<div class="section1">
				<div class="">
					<a href="<?php host();?>/business/everyday/smart.php">
						<img class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/fastSavings.jpg" alt="" width="300"/></a>
				</div>
				<h4>Smart</h4>
				<legend></legend>
				<p>
					Apply now for a Business Smart Account <br>
					Essential everyday transaction bank account with the flexibility and freedom your business needs. Ideal for starting a new business.
				</p>
				<a class="btn btn-info" href="<?php host();?>/business/everyday/smart.php">More info</a>
			</div>
		</div>
		<div class="sect col-md-4 col-sm-12">
			<div class="section2">
				<div class="imgSections">
					<a href="<?php host();?>/business/everyday/master.php">
						<img width="300" class="img-responsive img-circle center-block" src="<?php host();?>/rs/img/creditCard.jpg" alt="" /></a>
				</div>
				<h4>Master</h4>
				<legend></legend>
				<p>
					Complete everyday transaction bank account ideal for businesses that do lots of transactions each month
				</p>
				<a class="btn btn-info" href="<?php host();?>/business/everyday/master.php">More info</a>
			</div>
		</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
