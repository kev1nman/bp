<!DOCTYPE html>
<html lang="es">
<head>
	<?php
		include '../../structure.php';
		cabecera();
	?>
</head>
<body>
	<?php
		menu();
	?>
<div id="banner">
	<img src="<?php host();?>/rs/img/bann.jpg" id="img_banner">
</div>
<div class="container">
	<div class="col-md-12">
		<h1>Business Smart Account</h1>
		<p>
			Apply now for a Business Smart Account <br>
			Essential everyday transaction bank account with the flexibility and freedom your business needs. Ideal for starting a new business.
		</p>
	</div>
	<div class="rows">
		<div class="col-md-9 col-md-offset-2">
			<h2>Everyday benefits</h2>
			<ul>
				<li>Pay and get paid</li>
				<li>Make tax time easier</li>
				<li>Separate personal and business transactions</li>
				<li>Free online legal and taxation advice</li>
			</ul>
			<h2>What you get</h2>
				<ul>
					<li>Unlimited fee-free electronic transactions</li>
					<li>55 free over the counter deposits or withdrawals, 55 free cheque deposits or merchant envelopes monthly</li>
					<li>Access to your funds anytime, anywhere: Online, mobile & tablet, Branch, ATM, Cheque, EFTPOS, BPAY®</li>
					<li>SMS and email alerts to keep track of your account activity </li>
					<li>Link Business Visa Debit Card to your account</li>
					<li>Earn competitive interest (calculated daily, paid monthly)</li>
				</ul>
			<h2>Fees and interest</h2>
			<table class="table">
				<tr>
					<td>Account fee</td>
					<td>$10 monthly account keeping feed</td>
				</tr>
				<tr>
					<td>Transaction fees</td>
					<td>
						<ul>
							<li>Charged when monthly fee-free allowance is exceeded</li>
							<li>Read more about fees and charges and how to minimise them</li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php
	pie();
?>
<script>
	$(document).ready(function () {
		$('#sect2').addClass('active');
	});
	$("#E-Banking").html('Business E-Banking');
</script>
</body>
</html>
